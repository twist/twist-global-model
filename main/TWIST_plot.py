#!/usr/bin/env python

# ---------------------------------- #
# Core module of the TWIST 
# (Trade WIth STorage) model. 
#
# jacob.schewe@pik-potsdam.de
# ---------------------------------- #

import numpy as np
import pickle
import os, sys
from importlib import reload

import TWIST_functions
import TWIST_plotFunctions
import imp


def main(TWIST_inputfiles, TWIST_settings):
    """TWIST plot module.
       Loads input data as specified in TWIST_inputfiles.py;
       imports settings from TWIST_settings.py;
       imports model results from pickle file;
       plots results using TWIST_plotFunctions.py.
    """

    print('Hi, this is the plot module!')

    # -------------------- Get input data and parameters --------------------

    import TWIST_inputfiles as TWIST_inputfiles;
    imp.reload(TWIST_inputfiles)
    import TWIST_settings as settings;
    imp.reload(settings)

    dataPath = '{0}/{1}'.format(settings.localbasedir, 'input')
    producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, \
    grainEndStorage_file = TWIST_inputfiles.inputFiles(settings.commodity, dataPath)

    funs = TWIST_functions.Functions(settings.n_price_units)
    prodCountry_names, n_producers, \
    n_producers_per_country, month_of_harvest = funs.producer_country_data(producfileVar, iso3file, harvDatefile,
                                                                           settings.q_clip,
                                                                           settings.n_producers_smallest)
    consCountry_names, n_consumers = funs.consumer_country_data(consumfileVar, settings.q_clip)
    prodCountry_quantity_var, prodCountry_years_var = funs.read_harvest_timeseries(producfileVar, prodCountry_names,
                                                                                   settings.start_year,
                                                                                   settings.q_trade)
    consCountry_quantity_var, consCountry_years_var = funs.read_demand_timeseries(consumfileVar, consCountry_names,
                                                                                  settings.q_trade)

    # Lists of Producers and consumers
    producers = np.arange(0, n_producers)
    consumers = np.arange(0, n_consumers)

    # Get n_producers x 12 array of fraction of annual harvest per month
    harvest_distrYear = funs.harvestingPeriod(settings.t_per_year, month_of_harvest, settings.harvest_distribution,
                                              producers)

    if (settings.production_varying == 0):
        # For constant annual harvests:
        t_start = settings.constant_t_start
        t_end = settings.constant_t_end
        q_harvest = funs.harvest_timeseries_constant(t_start, t_end, settings.shock_year, settings.shock_month,
                                                     settings.shock_factor, prodCountry_quantity_var,
                                                     prodCountry_years_var, settings.input_year, settings.t_per_year)
    elif (settings.production_varying == 1):
        # For time-varying annual harvests:
        q_harvest, t_start, t_end = funs.harvest_timeseries_varying(prodCountry_quantity_var, settings.t_per_year,
                                                                    settings.repeatFirstColumnNTimes)
    if (settings.consumption_varying == 0):
        # For constant monthly demand:
        finalCons, p_max_cons, q_storage_max_cons = funs.demand_timeseries_constant(t_start, t_end,
                                                                                    consCountry_quantity_var,
                                                                                    consCountry_years_var,
                                                                                    settings.input_year,
                                                                                    settings.min_frac,
                                                                                    settings.p_max_cons_base,
                                                                                    settings.q_storage_max_cons_factor,
                                                                                    settings.t_per_year)
    elif (settings.consumption_varying == 1):
        # For time-varying monthly demand:
        q_harvest_total_init = np.sum(q_harvest[0, :])
        finalCons, p_max_cons, q_storage_max_cons, q_out_min_annual = funs.demand_timeseries_varying(
            consCountry_quantity_var, settings.filtersize, settings.t_per_year, settings.repeatFirstColumnNTimes,
            settings.min_frac, settings.p_max_cons_base, settings.match_q_spinup, q_harvest_total_init,
            settings.q_storage_max_cons_factor, settings.q_out_offset, settings.q_out_offset_type)


    # Further parameters defining agents' behaviour:
    p_storage, q_storage_prod = funs.producerInput(t_start, t_end, settings.q_trade, settings.q_storage_init_prod)
    q_storage_cons = funs.consumerInput(finalCons)
    countries_producers = funs.countries(prodCountry_names)

    # Create an *independent* array
    q_out_min = np.array([item for item in finalCons])

    # Load (prescribed) export restrictions and import strategies
    exportRestr = funs.exportRestrictions()
    #  importStrat = funs.importStrategies(settings.repeatFirstColumnNTimes, settings.start_year, consCountry_names, consCountry_years_var)
    importStrat = funs.importStrategies()
    # Global plot settings
    qty_multiplier = settings.qty_multiplier
    ticklsize = settings.ticklsize
    axlsize = settings.axlsize
    # Load plot functions
    plotFunctions = TWIST_plotFunctions.PlotFunctions(settings.outdir, settings.t_per_year, settings.start_year,
                                                      t_start, t_end, settings.repeatFirstColumnNTimes,
                                                      settings.consumption_varying, qty_multiplier, axlsize, ticklsize,
                                                      settings.endogFinalDemand, subplots=False)

    fp = open('TWIST_results.pkl', 'rb')
    Px, Q_out, Q_storage_prod, Q_storage_cons, P_min_prod, P_max_prod = pickle.load(fp)
    fp.close()

    #####  Plot  #####

    # Define time axes
    taxis = np.arange(t_start, t_end + 1)
    taxis_annual = np.arange(t_start, t_end + 1, settings.t_per_year)

    if not settings.projection_mode:
        # Read World Bank grain prices:
        grainPrices_list = funs.readCSVinput(grainPrices_file)
        grainPrices_array = np.array(grainPrices_list)
        grainPrices_year = [item.split('M')[0] for item in grainPrices_array[2:, 0]]
        grainPrices_month = [item.split('M')[1] for item in grainPrices_array[2:, 0]]
        grainPrices_legends = grainPrices_array[0, 1:]
        grainPrices_data_str = grainPrices_array[2:, 1:]
        grainPrices_data_str[grainPrices_data_str == 'NA'] = np.nan
        grainPrices_data = grainPrices_data_str.astype(np.float)
        # Correct for inflation:
        priceDeflator_dict = funs.read_priceDeflator(CPIfile)
        grainPrices_deflator = np.array([priceDeflator_dict[int(year)] for year in grainPrices_year if
                                         int(year) in list(priceDeflator_dict.keys())])
        grainPrices_data_cropped = np.array([row for i, row in enumerate(grainPrices_data) if
                                             int(grainPrices_year[i]) in list(priceDeflator_dict.keys())])
        grainPrices_year_cropped = np.array(
            [year for year in grainPrices_year if int(year) in list(priceDeflator_dict.keys())])
        grainPrices_deflated = np.divide(grainPrices_data_cropped, grainPrices_deflator[:, np.newaxis]) * 100.

    # Plot of world market price over time:
    figNum = 3
    # figName = 'price_Fix_Flex.pdf'
    figName = 'price_Fix_Flex_policies.pdf'
    # figName = 'price_Fix_Corn_paramsWheat.pdf'
    # figName = 'priceDiff_Fix_Corn_paramsWheat.pdf'
    # figName = 'price_Flex_Corn_paramsWheat.pdf'
    # figName = 'priceDiff_Flex_Corn_paramsWheat.pdf'
    # figName = 'priceDiff_Flex_Sugar_LPJmL_HadGEM_RCP2p6_RCP8p5.pdf'
    figSave = 1
    if settings.projection_mode:
        # For plotting just one model version:
        plotFunctions.plotPrice_NoObs(figNum, taxis, Px, prodCountry_years_var, figName, figSave, xlabels=True,
                                      yaxis_right=False, ndiff=0)
        # # For plotting two different model versions, first save the result of the first run...
        # with open('price_Fix.pkl','wb') as fp:
        # pickle.dump((Px), fp)
        # # ...and then load it again, and plot:
        # with open('price_Fix.pkl', 'rb') as fp:
        # Px_Fix = pickle.load(fp)
        # plotFunctions.plotPrice_NoObs(figNum, taxis, [Px, Px_Fix], prodCountry_years_var, figName, figSave, xlabels=True, yaxis_right=False, ndiff=1)
        # # ...and afterwards remove the pickle file:
        # os.remove('price_Fix.pkl')
    else:
        # For plotting just one model version:
        grainPrices_data, randomModel_autoCorr = plotFunctions.plotPrice(figNum, taxis, Px, grainPrices_deflated,
                                                                         grainPrices_year_cropped,
                                                                         int(grainPrices_month[0]), grainPrices_legends,
                                                                         figName, figSave, deflate='CPI',
                                                                         cropYear_month1=6, xlabels=True,
                                                                         yaxis_right=False, ndiff=0)
        # # For plotting two different model versions, first save the result of the first run...
        # with open('price_Fix.pkl','wb') as fp:
        # pickle.dump((Px), fp)
        # # ...and then load it again, and plot:
        # with open('price_Fix.pkl', 'rb') as fp:
        # Px_Fix = pickle.load(fp)
        # plotFunctions.plotPrice(figNum, taxis, [Px, Px_Fix], grainPrices_deflated, grainPrices_year_cropped, int(grainPrices_month[0]), grainPrices_legends, figName, figSave, deflate='CPI', cropYear_month1=6, xlabels=True, yaxis_right=False, ndiff=0)
        # # ...and afterwards remove the pickle file:
        # os.remove('price_Fix.pkl')

    # # Test sigificance of autocorrelation
    # priceModel_autoCorr = 0.292
    # signiFrac = np.sum(1 for i in randomModel_autoCorr if i > priceModel_autoCorr)
    # print 'Significance of autocorrelation: ', signiFrac, 'out of ', len(randomModel_autoCorr)

    # Plot of storage levels over time:
    figNum = 5
    # figName = 'storage_Fix_Flex.pdf'
    # figName = 'storage_Fix_Flex_policies.pdf'
    figName = 'storage_Fix_offset.pdf'
    # figName = 'storage_Fix_Corn_paramsWheat.pdf'
    # figName = 'storage_Flex_Corn_paramsWheat.pdf'
    # figName = 'panel_Fix_fixedStorage.pdf'
    # figName = 'panel_Fix_averagedConsumption.pdf'
    figSave = 1
    if settings.projection_mode:
        plotFunctions.plotStorage_NoObs(figNum, taxis, taxis_annual, Q_storage_prod, Q_storage_cons,
                                        prodCountry_years_var, importStrat, exportRestr, figName, figSave,
                                        ProdConsSeparate=True, storageCapacity=False, xlabels=False, yaxis_right=False)
    else:
        plotFunctions.plotStorage(figNum, taxis, taxis_annual, Q_storage_prod, Q_storage_cons, prodCountry_years_var,
                                  grainStorage_file, importStrat, exportRestr, figName, figSave, ProdConsSeparate=False,
                                  storageCapacity=False, xlabels=False, yaxis_right=False)
    # # For plotting two different model versions, first save the result of the first run...
    # with open('storage_Fix.pkl','wb') as fs:
    # pickle.dump((Q_storage_prod, Q_storage_cons), fs)
    # ...and then load it again, and plot:
    # with open('storage_Fix.pkl','rb') as fs:
    # Q_storage_prod_Fix, Q_storage_cons_Fix = pickle.load(fs)
    # plotFunctions.plotStorage(figNum, taxis, taxis_annual, [Q_storage_prod, Q_storage_prod_Fix], [Q_storage_cons, Q_storage_cons_Fix], prodCountry_years_var, grainStorage_file, importStrat, exportRestr, figName, figSave, ProdConsSeparate=False, storageCapacity=False, xlabels=False, yaxis_right=False)
    # # ...and afterwards remove the pickle file:
    # os.remove('storage_Fix.pkl')

    # Plot of global consumption over time:
    figNum = 4
    figName = 'consumption_surplus_storageCapacity.pdf'
    # figName = 'consumption_Fix_Flex.pdf'
    # figName = 'consumption_Fix_Flex_policies.pdf'
    # figName = 'panel_Fix_Flex.pdf'
    # figName = 'panel_Fix_Flex_policies.pdf'
    # figName = 'panel_Fix_Flex_policies_zoom.pdf'
    # figName = 'panel_Fix_Corn_paramsWheat.pdf'
    # figName = 'consumption_Flex_Corn_paramsWheat.pdf'
    # figName = 'panel_Fix_Flex_Extended2016.pdf'
    # figName = 'panel_Fix_Flex_Extended2016_policies_zoom.pdf'
    # figName = 'panel_Flex_Extended2016_policies_zoom.pdf'
    figSave = 0
    if (settings.consumption_varying == 1):
        plotFunctions.plotConsumption(figNum, taxis, taxis_annual, q_harvest, Q_out, q_out_min, q_out_min_annual,
                                      consCountry_quantity_var, prodCountry_years_var, figName, figSave, importStrat,
                                      q_storage_max_cons, anomalies=True, surplus=False, storageCapacity=False,
                                      xlabels=False, yaxis_right=False)
    # # For plotting two different model versions, first save the result of the first run...
    # with open('consumption_Fix.pkl','wb') as fc:
    # pickle.dump((Q_out), fc)
    # # ...and then load it again, and plot:
    # with open('consumption_Fix.pkl','rb') as fc:
    # Q_out_Fix = pickle.load(fc)
    # plotFunctions.plotConsumption(figNum, taxis, taxis_annual, q_harvest, [Q_out, Q_out_Fix], q_out_min, q_out_min_annual, consCountry_quantity_var, prodCountry_years_var, figName, figSave, importStrat, q_storage_max_cons, anomalies=True, surplus=False, storageCapacity=False, xlabels=False, yaxis_right=False)
    # # ...and afterwards remove the pickle file:
    # os.remove('consumption_Fix.pkl')

    # # Plot of world production over time:
    # figNum = 7
    # figName = 'production.pdf'
    # figSave = 0
    # plotFunctions.plotProduction(figNum, taxis, prodCountry_quantity_var, prodCountry_years_var, figName, figSave)

    # # Plot of price variables over time:
    # figNum = 6
    # plotFunctions.plotPminPmax(figNum, taxis, P_min_prod, P_max_prod)

    # # Plot of world market price *differences* over time:
    # figNum = 8
    # figName = 'price_1stDiff.pdf'
    # figSave = 0
    # plotFunctions.plotPriceDiff(figNum, taxis, Px, grainPrices_data, grainPrices_year, grainPrices_legends, figName, figSave)

    # # Plot export and import policy parameters
    # figNum = 10
    # # figName = 'policy_parameters_MatchRealistic.pdf'
    # # figName = 'policy_parameters_MatchPerfect.pdf'
    # # figName = 'policy_parameters_XD_importPolicy.pdf'
    # figName = 'policy_parameters_ND_importPolicy.pdf'
    # figSave = 0
    # plotFunctions.plotPolicyParameters(figNum, taxis, importStrat, exportRestr, settings.q_storage_max_cons_factor, figName, figSave)

    # # Test consumption model with reported prices
    # # tau = 1
    # Q_out_mid_full = q_out_min/settings.min_frac # Retrieving 100% of observed (long-term average) demand
    # Q_out_mid = Q_out_mid_full[settings.repeatFirstColumnNTimes+tau:]
    # ### Reference price: Average of previous years
    # grainPrices_data = grainPrices_data[10:]
    # Pref = [np.mean(grainPrices_data[n-tau:n]) for n in np.arange(tau,len(grainPrices_data))]
    # ### Domestic price index: World price modulated by relative storage level
    # Pdom = np.divide(grainPrices_data[tau:,0],Pref)
    # ### Consumption
    # Q_out = np.multiply(Q_out_mid[:,0],(Pdom**settings.elast_finalDemand))
    # q_out[q_out<(settings.q_out_cutoff*q_out_mid)] = settings.q_out_cutoff*q_out_mid[q_out<(settings.q_out_cutoff*q_out_mid)] # Cut-off
    # figNum = 6
    # figName = 'consumption_model_test.pdf'
    # figSave = 0
    # plotFunctions.plotConsumption(figNum, taxis[settings.repeatFirstColumnNTimes+tau:], taxis_annual, q_harvest, Q_out[:,np.newaxis], q_out_min[settings.repeatFirstColumnNTimes+tau:,:], q_out_min_annual, consCountry_quantity_var, prodCountry_years_var, figName, figSave, importStrat, q_storage_max_cons, anomalies=False, surplus=False, storageCapacity=False, xlabels=True)
    # # print 'Consumption correlation: ', np.corrcoef(Q_out[:], q_out_min[settings.repeatFirstColumnNTimes+tau:,0])[0,1]


if __name__ == '__main__':
    # TWIST_plot.py executed as script
    # 
    # As we're running in an interactive session, reload modules in case they have been changed:
    print("Caution: Executing TWIST plot module directly. \
      Make sure the inputfiles and settings used are as desired.")
    import TWIST_inputfiles as TWIST_inputfiles
    import TWIST_settings as settings;

    imp.reload(TWIST_functions)
    imp.reload(TWIST_plotFunctions)
    # Run the model:
    main(TWIST_inputfiles, settings)
