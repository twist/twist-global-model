#!/usr/bin/env python

import os
import sys
import imp
import numpy as np
import matplotlib.pyplot as plt
import TWIST_coreSimple
import TWIST_functions
from datetime import datetime
from matplotlib.ticker import MaxNLocator

import os
import sys
import warnings
import argparse
import csv


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(
    description='program to run TWIST with production decline, export restrictions and/or increased consumer demand')
parser.add_argument(
    '--crop', type=str, default='Corn',
    help="Choose commodity type current options 'Wheat', 'Rice' or 'Corn'")
# parser.add_argument(
#     '--include_exportRestr', type=str2bool, default=False,
#     help="Run TWIST with export restriction, set country/countries with argument --exportRest_countries")
parser.add_argument(
    '--exportRestr_countries', type=str, default='none',
    help="Run TWIST with export restriction. For wheat: 'RUS', 'UKR', 'KAZ', 'RUS,UKR' or 'RUS,UKR,KAZ'. For rice: 'VNM', 'THA', 'IND', 'VNM,THA' or 'VNM,THA,IND'. For corn: 'UKR', 'ARG', 'BRA', 'UKR,ARG' or 'UKR,ARG,BRA' or 'none' ")
parser.add_argument(
    '--include_prodDecline', type=str2bool, default=False,
    help="Run TWIST with a 20th percentile production decline in export countries and 5th percentile production decline in locust threatened countries")
parser.add_argument(
    '--include_importStrat', type=str2bool, default=False,
    help="Run TWIST with a 80th percentile increase in world stock-to-use, to estimate the effect of increased consumer import")


# Get agents input data and parameters
args = parser.parse_args()

# set the name of the folder containing the inout data used for the COVID-19 paper
if args.crop == 'Wheat':
    commodity = 'Wheat_proj'
elif args.crop == 'Rice':
    commodity = 'Rice_proj'
elif args.crop == 'Corn':
    commodity = 'Corn_proj'
else:
    warnings.warn("Crop {} not available (try 'Wheat', 'Rice' or 'Corn)")
    sys.exit(1)

scenario_mode = False  # 'constant_shock_size_all_years' or 'one_year_shock_with_proj'
historic_start_year = 1975
scenario_start_year = 2020
length_of_scenario = 2  # years


print('args.include_prodDecline', args.include_prodDecline)
print('args.include_importStrat', args.include_importStrat)
# print('args.include_exportRestr', args.include_exportRestr)
print('args.exportRestr_countries', args.exportRestr_countries)

if args.include_prodDecline == False:
    production_decline = {}
elif args.include_prodDecline == True:
    if args.crop == 'Wheat':
        production_decline = {2020: 1-0.0245}  # 2.45% decline in world production (1 => base line scenario)
    elif args.crop == 'Rice':
       production_decline = {2020: 1-0.0091}  # 0.91% decline in world production (1 => base line scenario)
    elif args.crop == 'Corn':
        production_decline = {2020: 1-0.0223}  # 2.23% decline in world production (1 => base line scenario)



if args.include_importStrat == True:
    if args.crop == 'Wheat':
        importStrat_dic = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2011:1.05, 2012:1.05, 2013:1.05, 2014:1.05, 2015:1.05, 2016:1.05, 2017:1.05, 2018:1.05, 2019:1.05, 2020:1.0712, 2021:1.05, 2022:1.05}
    elif args.crop == 'Rice':
        importStrat_dic = {2000: 0.98, 2001: 0.96, 2002: 0.94, 2003: 0.92, 2004: 0.92, 2005: 0.92, 2006: 0.92, 2015: 1.02, 2016: 1.02, 2017: 1.03, 2018: 1.04, 2019: 1.05, 2020: 1.0677, 2021: 1.05, 2022: 1.05}
    elif args.crop == 'Corn':
        importStrat_dic ={1998: 0.98, 1999:0.94, 2000:0.92, 2001:0.88, 2002:0.85, 2003:0.85, 2004:0.85, 2005:0.85, 2006:0.85, 2011:1.2, 2012:1.2, 2013:1.2, 2014:1.2, 2015:1.2, 2016:1.2, 2017:1.2, 2018:1.2, 2019:1.2, 2020:1.2232, 2021:1.2, 2022:1.2}
elif args.include_importStrat == False:
    # hardcoded calibrations of import policies for the baseline
    if args.crop == 'Wheat':
        importStrat_dic = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2011:1.05, 2012:1.05, 2013:1.05, 2014:1.05, 2015:1.05, 2016:1.05, 2017:1.05, 2018:1.05, 2019:1.05, 2020:1.05, 2021:1.05, 2022:1.05}
    elif args.crop == 'Rice':
        importStrat_dic = {2000: 0.98, 2001: 0.96, 2002: 0.94, 2003: 0.92, 2004: 0.92, 2005: 0.92, 2006: 0.92, 2015: 1.02, 2016: 1.02, 2017: 1.03, 2018: 1.04, 2019: 1.05, 2020: 1.05, 2021: 1.05, 2022: 1.05}
    elif args.crop == 'Corn':
        importStrat_dic ={1998: 0.98, 1999:0.94, 2000:0.92, 2001:0.88, 2002:0.85, 2003:0.85, 2004:0.85, 2005:0.85, 2006:0.85, 2011:1.2, 2012:1.2, 2013:1.2, 2014:1.2, 2015:1.2, 2016:1.2, 2017:1.2, 2018:1.2, 2019:1.2, 2020:1.2, 2021:1.2, 2022:1.2}




# if args.include_exportRestr == False:
#     include_consReduction = False
#     exportRestr_dic = {}
#     consumption_reduced = {}
# elif args.include_exportRestr == True:
#     include_consReduction = True
if args.exportRestr_countries == 'none':
    args.include_exportRestr = False
    include_consReduction = False
    exportRestr_dic = {}
    consumption_reduced = {}
else:
    args.include_exportRestr = True
    include_consReduction = True
    if args.crop == 'Wheat':
        if args.exportRestr_countries == 'RUS':
            exportRestr_dic     = {2020: 1 - 0.098}
            consumption_reduced = {2020: 1 - 0.055}
        elif args.exportRestr_countries == 'UKR':
            exportRestr_dic     = {2020: 1 - 0.034}
            consumption_reduced = {2020: 1 - 0.012}
        elif args.exportRestr_countries == 'KAZ':
            exportRestr_dic     = {2020: 1 - 0.019}
            consumption_reduced = {2020: 1 - 0.009}
        elif args.exportRestr_countries == 'RUS,UKR':
            exportRestr_dic     = {2020: 1 - 0.132}
            consumption_reduced = {2020: 1 - 0.067}
        elif args.exportRestr_countries == 'RUS,UKR,KAZ':
            exportRestr_dic     = {2020: 1 - 0.151}
            consumption_reduced = {2020: 1 - 0.076}
        else:
            warnings.warn("Select country/countries to impose export restrictions (set --exportRestr_countries to 'RUS', 'UKR', 'KAZ', 'RUS,UKR' or 'RUS,UKR,KAZ')")
            sys.exit(1)

    elif args.crop == 'Rice':
        if args.exportRestr_countries == 'VNM':
            exportRestr_dic     = {2020: 1 - 0.056}
            consumption_reduced = {2020: 1 - 0.044}
        elif args.exportRestr_countries == 'THA':
            exportRestr_dic     = {2020: 1 - 0.04}
            consumption_reduced = {2020: 1 - 0.024}
        elif args.exportRestr_countries == 'IND':
            exportRestr_dic     = {2020: 1 - 0.233}
            consumption_reduced = {2020: 1 - 0.204}
        elif args.exportRestr_countries == 'VNM,THA':
            exportRestr_dic     = {2020: 1 - 0.096}
            consumption_reduced = {2020: 1 - 0.068}
        elif args.exportRestr_countries == 'VNM,THA,IND':
            exportRestr_dic     = {2020: 1 - 0.329}
            consumption_reduced = {2020: 1 - 0.272}
        else:
            warnings.warn("Select country/countries to impose export restrictions (set --exportRestr_countries to 'VNM', 'THA', 'IND', 'VNM,THA' or 'VNM,THA,IND')")
            sys.exit(1)

    elif args.crop == 'Corn':
        if args.exportRestr_countries == 'UKR':
            exportRestr_dic     = {2020: 1 - 0.032}
            consumption_reduced = {2020: 1 - 0.005}
        elif args.exportRestr_countries == 'ARG':
            exportRestr_dic     = {2020: 1 - 0.045}
            consumption_reduced = {2020: 1 - 0.012}
        elif args.exportRestr_countries == 'BRA':
            exportRestr_dic     = {2020: 1 - 0.09}
            consumption_reduced = {2020: 1 - 0.06}
        elif args.exportRestr_countries == 'UKR,ARG':
            exportRestr_dic     = {2020: 1 - 0.077}
            consumption_reduced = {2020: 1 - 0.017}
        elif args.exportRestr_countries == 'UKR,ARG,BRA':
            exportRestr_dic     = {2020: 1 - 0.167}
            consumption_reduced = {2020: 1 - 0.077}
        else:
            warnings.warn("Select country/countries to impose export restrictions (set --exportRestr_countries to 'UKR', 'ARG', 'BAR', 'UKR,ARG' or 'UKR,ARG,BRA')")
            sys.exit(1)

# in case --exportRestr_countries is set to none, interpret this as no export restrictions
if args.exportRestr_countries == 'none':
    args.include_exportRestr = False
    exportRestr_dic = {}
    consumption_reduced = {}

consumption_fixed_reported = True  # no smoothing



def main(commodity, scenario_mode, historic_start_year, scenario_start_year, length_of_scenario, include_prodDecline, prodDecline_dic, include_importStrat, importStrat_dic, include_exportRestr, exportRestr_dic, include_consReduction, consReduction_dic, consumption_fixed_reported):
    date = datetime.now().strftime('%Y-%m-%d_%H:%M')

    print("\nTWIST COVID-19 scenario\n")
    print('Crop:', args.crop)
    print('Historic period:', historic_start_year, 'to', scenario_start_year-1)
    print('Scenario period:', scenario_start_year, 'to', scenario_start_year+length_of_scenario)
    print('Shocks year:', scenario_start_year, '\n')

    # Present directory is working directory
    workdir = os.path.dirname(os.path.realpath(__file__))
    settings = str(workdir + '/TWIST_settings.py')
    inputfiles = str(workdir + '/TWIST_inputfiles.py')


    _settings_ = imp.load_source('settings', settings)
    _inputfiles_ = imp.load_source('inputfiles', inputfiles)

    modelbasedir = os.path.abspath(_settings_.localbasedir)
    sys.path.append(modelbasedir)
    dataPath = '{0}/{1}'.format(_settings_.localbasedir, 'input')
    savePath = _settings_.outdir

    # create dictionary where the fitting results will be saved
    if not os.path.exists('savePath'):
        os.makedirs('savePath')

    # get the name and path of the input file for the selected commodity
    producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStock_file = _inputfiles_.inputFiles(
        commodity,
        dataPath)

    funs = TWIST_functions.Functions(_settings_.n_price_units)

    # Pre calibrated values for 1975-2019, used for the COVID-19 paper
    if commodity == 'wheat_proj' or commodity == 'Wheat_proj':
        historic_start_year = 1975
        cropYear_month1 = 6
        q_storage_max_cons_type = 0
        q_out_offset_type = 0
        loss_factor = 1.0

        q_out_offset = 2800570.445512435
        alpha_s = 0.12160632882103238
        alpha_d = 0.6865763166524175
        p_max_prod_const = 219.90003763140106
        p_max_cons_base = 1489.7230171361807
        q_storage_max_cons_factor = 1.678958539479257

    elif commodity == 'Rice_proj' or commodity == 'rice_proj':
        historic_start_year = 1975
        cropYear_month1 = 1
        q_storage_max_cons_type = 1
        q_out_offset_type = 0
        loss_factor = 1.0

        q_out_offset = 1528521.3501655988
        alpha_s = 0.14999999999822272
        alpha_d = 1.2611893937096115
        p_max_prod_const = 249.99999999999997
        p_max_cons_base = 1767.8939996361903
        q_storage_max_cons_factor = 1.2994046797590273

    elif commodity == 'Corn_proj' or commodity == 'corn_proj':
        historic_start_year = 1975
        cropYear_month1 = 8
        q_storage_max_cons_type = 0
        q_out_offset_type = 0
        loss_factor = 1.0

        q_out_offset = 1348071.5595195028
        alpha_s = 0.14976020867121806
        alpha_d = 0.7144599886411545
        p_max_prod_const = 109.61030026631633
        p_max_cons_base = 577.066376473279
        q_storage_max_cons_factor = 1.9083889232550795

    else:
        sys.exit('Model is not calibrated for commodity {} yet. Available crops are Wheat, Rice and Corn'.format(commodity))


    # use fix value, for the endogenuos determination of the final demand estimates (FlexCons)
    elast_finalDemand = -0.1

    historic_time_period = np.arange(historic_start_year, scenario_start_year)

    # get the beginning stock for the time period to be fitted
    beginning_stocks = funs.read_in_stock_data(grainStorage_file, historic_time_period)

    # set the initial storage level
    q_storage_init_prod = beginning_stocks[0]

    production_varying = 1
    consumption_varying = 1
    endogFinalDemand = 1
    filtersize = 5  # this is the window size which the reference consumption is calculated

    print("\nRun Scenario.")
    parameters = [commodity, historic_start_year, historic_start_year, q_storage_init_prod, production_varying, consumption_varying, \
                  endogFinalDemand, filtersize, q_out_offset, loss_factor, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base, \
                  q_storage_max_cons_factor, elast_finalDemand, scenario_mode, scenario_start_year, length_of_scenario,
                  include_prodDecline, prodDecline_dic,
                  True, importStrat_dic, # include_importStrat = True for both scenario with added import strategies and the baseline
                  include_exportRestr, exportRestr_dic,
                  include_consReduction, consReduction_dic,
                  consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

    # run scenario
    price, total_stocks, prod_stocks, cons_stocks, prod_stocks_spinup, cons_stocks_spinup, consumption, consumption_spinup, harvest, year_sim = TWIST_coreSimple.main(inputfiles, settings, parameters)

    print("\nRun Baseline and Projection")
    # run baseline and projection at the same time
    baseline = {}  # no prod decline, no export restrictions, no cons reduction. run as baseline scenario
    scenario_mode = False
    include_exportRestr = False
    # hardcoded calibrations of import policies
    if commodity == 'Wheat_proj':
        baseline_importStat = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2011:1.05, 2012:1.05, 2013:1.05, 2014:1.05, 2015:1.05, 2016:1.05, 2017:1.05, 2018:1.05, 2019:1.05, 2020:1.05, 2021:1.05}
    elif commodity == 'Corn_proj':
        baseline_importStat = {1998: 0.98, 1999:0.94, 2000:0.92, 2001:0.88, 2002:0.85, 2003:0.85, 2004:0.85, 2005:0.85, 2006:0.85, 2011:1.2, 2012:1.2, 2013:1.2, 2014:1.2, 2015:1.2, 2016:1.2, 2017:1.2, 2018:1.2, 2019:1.2, 2020:1.2, 2021:1.2, 2022:1.2}
    elif commodity == 'Rice_proj':
        baseline_importStat = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2015:1.02, 2016:1.02, 2017:1.03, 2018:1.04, 2019:1.05, 2020:1.05, 2021:1.05, 2022:1.05}
    else:
        baseline_importStat = {}


    parameters = [commodity, historic_start_year, historic_start_year, q_storage_init_prod, production_varying,
                  consumption_varying,
                  endogFinalDemand, filtersize, q_out_offset, loss_factor, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base,
                  q_storage_max_cons_factor, elast_finalDemand, scenario_mode, scenario_start_year, length_of_scenario,
                  include_prodDecline, baseline,
                  True, baseline_importStat,
                  include_exportRestr, baseline,
                  include_consReduction, baseline,
                  consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

    price_proj, total_stocks_proj, prod_stocks_proj, cons_stocks_proj, prod_stocks_proj_spinup, cons_stocks_proj_spinup, consumption_proj, consumption_proj_spinup, harvest_proj, year_sim_proj = TWIST_coreSimple.main(inputfiles, settings, parameters)

    #***********************************************************
    #                       Post processing
    #***********************************************************

    # read in the ending stock
    end_stock_rep = funs.read_in_stock_data(grainEndStock_file, historic_time_period)

    # read in the annual world market price
    WM_price_rep = funs.read_in_price_data(grainPrices_file, CPIfile, historic_time_period, cropYear_month1)

    # read in reported consumption
    Q_out_rep = funs.read_in_consumption_data(consumfileVar, historic_time_period)

    # read in reported production
    Q_harvest_rep = funs.read_in_production_data(producfileVar, historic_time_period)

    stock_to_use = (cons_stocks_spinup+prod_stocks_spinup)/consumption_spinup
    temp = stock_to_use[10:len(stock_to_use)-length_of_scenario]
    stock_to_use_mean = np.mean(stock_to_use[10:len(stock_to_use)-length_of_scenario])
    # print('\nHistoric mean S/U=', round(float(stock_to_use_mean), 3))
    # print('At scenario start S/U=', round(float(temp[-1]), 3))

    stock_to_use_rep = end_stock_rep/Q_out_rep
    stock_to_use_mean_rep = np.mean(stock_to_use_rep)
    # print('\nReported mean S/U=', round(float(stock_to_use_mean_rep), 3))
    # print('Reported at scenario start S/U=', round(float(stock_to_use_rep[-1]), 3))

    #***********************************************************
    #                       Plotting
    #***********************************************************

    fig, axs = plt.subplots(4, 1, figsize=(6, 12), sharex=True)
    start = historic_start_year
    scenario = scenario_start_year-1
    finish = scenario_start_year+length_of_scenario-1
    blue = '#538cc6'
    orange = '#ff8c1a'
    yellow = '#ffd11a'
    red = '#ff5c33'

    axs[0].set_title('WM price for '+args.crop + '\n Baseline 1975-2019, scenario and projection 2020-2021', size=14)

    # axs[1].set_title(commodity + ' WM price ')
    axs[0].plot(historic_time_period, WM_price_rep, 'k-', label='Reported')
    axs[0].plot(year_sim_proj, price_proj, '-', color='g', label='Baseline and projection')
    # axs[0].plot(year_sim[-4:-1], price[-4:-1], '-', color=orange, label='Scenario')
    axs[0].plot(year_sim[-4:-1], price[-4:-1], '-', color=orange, label='Scenario')
    axs[0].legend()
    axs[0].set_ylabel('Price (US$/mt)', size=12)
    axs[0].set_xlim([start, finish])
    if commodity == 'Rice_proj':
        axs[0].set_ylim([0, 750])
    axs[0].axvspan(scenario, finish, color='k', alpha=0.15)

    #axs[1].set_title(commodity + ' ending stocks ')
    axs[1].plot(historic_time_period, end_stock_rep*10**(-6), 'k-')#, label='Reported ending stock')
    #axs[1].plot(year_sim, cons_stocks*10**(-6), '-', color=orange, label='Consumer stocks')
    #axs[1].plot(year_sim_base, prod_stocks*10**(-6), '-', color=red, label='Producer stocks')
    axs[1].plot(year_sim_proj, total_stocks_proj*10**(-6), '-', color='g', label='Baseline and projection total ending stock')
    axs[1].plot(year_sim[-4:-1], total_stocks[-4:-1]*10**(-6), '-', color=orange, label='Scenario total ending stock')
    #axs[1].legend()
    axs[1].set_ylabel('Ending stocks (mmt)', size=12)
    axs[1].set_xlim([start, finish])
    axs[1].axvspan(scenario, finish, color='k', alpha=0.15)

    #axs[2].set_title(commodity + ' production ')
    axs[2].plot(historic_time_period, Q_harvest_rep*10**(-6), 'k-', label='Reported production')
    axs[2].plot(year_sim_proj, harvest_proj*10**(-6), '-', color='g', label='Baseline and projection production', alpha=0.6)
    axs[2].plot(year_sim[-4:-1], harvest[-4:-1]*10**(-6), '-', color=orange, label='Scenario prduction', alpha=0.6)
    #axs[2].legend()
    axs[2].set_ylabel('Production (mmt)', size=12)
    axs[2].set_xlim([start, finish])
    axs[2].axvspan(scenario, finish, color='k', alpha=0.15)

    #axs[3].set_title(commodity + ' consumption ')
    axs[3].plot(historic_time_period, Q_out_rep*10**(-6), 'k-', label='Reported consumption')
    axs[3].plot(year_sim_proj, consumption_proj*10**(-6), '-', color='g', label='Baseline and projection  consumption')
    axs[3].plot(year_sim[-4:-1], consumption[-4:-1]*10**(-6), '-', color=orange, label='Scenario consumption')
    #axs[2].legend()
    axs[3].set_xlabel('Year', size=13)
    axs[3].set_ylabel('Consumption (mmt)', size=12)
    axs[3].set_xlim([start, finish])
    axs[3].axvspan(scenario, finish, color='k', alpha=0.15)

    fig.tight_layout(rect=[0, 0.02, 1, 0.98])
    fig.savefig(savePath+'/TWIST_Plot')

    # save simple results in a csv
    with open(savePath+'/TWIST_result.csv', mode='w') as csv_file:
        csv_file = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        csv_file.writerow(["Year", "WM_price_scenario [US$/mt]", "WM_price_baseline [US$/mt]", "Production_scenario [mt]", "Production_baseline [mt]", "Ending_stock_scenario [mt]", "Ending_stock_baseline [mt]", "Consumption_scenario [mt]", "Consumption+baseline [mt]"])

        for i in range(len(year_sim)):
            csv_file.writerow([year_sim[i], price[i], price_proj[i], total_stocks[i][0], total_stocks_proj[i][0], harvest[i][0], harvest_proj[i][0], consumption[i][0], consumption_proj[i][0]])


    print()
    print('Shock:', scenario_start_year)
    print('Production decline:', prodDecline_dic)
    print('Export restriction', exportRestr_dic)
    print('Consumtion reduction:', consReduction_dic)
    print('Price scenario', price[-5:],' for years:', year_sim[-5:],)
    print('Price projection:', price_proj[-5:], 'for years:', year_sim_proj[-5:])
    print()
    print('Price scenario/Price projection=', np.array(price[-5:])/np.array(price_proj[-5:]))
    print('Change in scenario price 2020 compared to projected price 2020:',  np.round((np.array(price[-3])/np.array(price_proj[-3])-1)*100,1), '%')
    # print('Change in scenario price compared to 2019 baseline price:',  np.round(np.array(price[-3])/np.array(price_proj[-4])-1,3)*100, '%')
    print('Price 2020 =', np.round(np.array(price[-3]),2), 'USD/mt')

    plt.show()


# run program
main(commodity, scenario_mode, historic_start_year, scenario_start_year, length_of_scenario, args.include_prodDecline, production_decline, args.include_importStrat, importStrat_dic, args.include_exportRestr, exportRestr_dic, include_consReduction, consumption_reduced, consumption_fixed_reported)
