#!/usr/bin/env python

# -------------------------------------------------------------------------- #
# Execute this script to run an instance of the TWIST 
# (Trade WIth STorage) model. 
# Author: Jacob Schewe, 2017; jacob.schewe@pik-potsdam.de
# Modifications: M.J. Puma, 2018; mjp38@columbia.edu
# -------------------------------------------------------------------------- #

import os
import sys
import imp
import TWIST_coreSimple

# Specify model base directory, where source files (core, functions, plotFunctions, ...) are located
# Note: Change path in 1) TWIST_run.py;
#                      2) TWIST_settings.py;
#                      3) TWIST_inputfiles.py
# Present directory is working directory
workdir = os.path.dirname(os.path.realpath(__file__))
inputfiles = str(workdir + '/TWIST_inputfiles.py')
settings = str(workdir + '/TWIST_settings.py')

_settings_ = imp.load_source('settings', settings)
modelbasedir = os.path.abspath(_settings_.localbasedir)

sys.path.append(modelbasedir)



print(("Using inputfiles as specified in module ", inputfiles))
print(("Using settings as specified in module ", settings))

print("\nCalling core module...\n")
TWIST_coreSimple.main(inputfiles, settings)
print("\nDone.\n")
