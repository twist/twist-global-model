#!/usr/bin/env python
import numpy as np
import csv
import collections as cs
import os, sys
import imp
import pickle
from runningmean import runmean_matchLeftEdge as runmean


class Functions:
    """Functions for the TWIST model.
        Reading and preparation of input data; auxiliary computations;
        specification of trade policies (import strategies and export restrictions).
        Some parameters are also set here.
    """

    # def __init__(self, t_start, t_end, n_producers, n_consumers, n_price_units, producers, consumers):
    def __init__(self, n_price_units):
        # self.t_start = t_start
        # self.t_end = t_end
        self.n_price_units = n_price_units

    def producer_country_data(self, producfileVar, iso3file, harvDatefile, q_clip, n_producers_smallest):
        """Read reference production data, clip to largest producing countries, allocate producers per country. """
        # Load reference production data (country name, quantity in tonnes, cumulative fraction of world consumption)
        # ...atleast_1d ensures same number of dimensions even if only one country is in the list
        prodCountry_names = np.atleast_1d(
            np.genfromtxt(producfileVar, skip_header=1, delimiter=';', usecols=(0), dtype=str))
        prodCountry_quantity = np.atleast_1d(
            np.genfromtxt(producfileVar, skip_header=1, delimiter=';', usecols=(1), dtype=float))
        # Clip to largest producers
        # prodCountry_names = prodCountry_names[np.cumsum(prodCountry_quantity)<q_clip]
        # prodCountry_quantity = prodCountry_quantity[np.cumsum(prodCountry_quantity)<q_clip]
        prodCountry_names = prodCountry_names[
            np.divide(np.cumsum(prodCountry_quantity), np.sum(prodCountry_quantity)) < q_clip]
        prodCountry_quantity = prodCountry_quantity[
            np.divide(np.cumsum(prodCountry_quantity), np.sum(prodCountry_quantity)) < q_clip]
        # Assume multiple producers per country.
        self.n_producers_per_country = [int(round(qty * n_producers_smallest / np.min(prodCountry_quantity))) for qty in
                                        prodCountry_quantity]
        self.n_producers = sum(self.n_producers_per_country)
        # print self.n_producers_per_country, self.n_producers
        # Load ISO3 country codes to allocate harvest dates
        prodCountry_iso3_names = np.atleast_1d(np.genfromtxt(iso3file, delimiter=';', usecols=(0), dtype=str))
        prodCountry_iso3_codes = np.atleast_1d(np.genfromtxt(iso3file, delimiter=';', usecols=(1), dtype=str))
        # prodCountry_iso3 = np.vstack((prodCountry_iso3_names, prodCountry_iso3_codes))
        prodCountry_iso3 = dict(list(zip(prodCountry_iso3_names, prodCountry_iso3_codes)))
        # Load harvest dates, weighted average of irrigated and rainfed harvest
        harvest_dates_countries = np.genfromtxt(harvDatefile, delimiter=';', usecols=(1), dtype=str)
        harvest_dates_ir = np.atleast_1d(
            np.genfromtxt(harvDatefile, delimiter=';', usecols=(2), dtype=float, missing_values='NA'))
        harvest_dates_rf = np.genfromtxt(harvDatefile, delimiter=';', usecols=(3), dtype=float, missing_values='NA')
        harvest_dates_irfrac = np.genfromtxt(harvDatefile, delimiter=';', usecols=(4), dtype=float, missing_values='NA')
        harvest_dates_mean = np.atleast_1d(np.add(np.multiply(harvest_dates_ir, harvest_dates_irfrac / 100.),
                                                  np.multiply(harvest_dates_rf, (1 - harvest_dates_irfrac / 100.))))

        harvest_dates_mean_prodCountries = [
            harvest_dates_mean[np.atleast_1d(harvest_dates_countries == prodCountry_iso3[pC_name])] for pC_name in
            prodCountry_names]
        # print harvest_dates_mean_prodCountries
        days_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        harvest_months_mean_prodCountries = [np.where(np.cumsum(days_in_months) > hdate)[0][0] + 1 for hdate in
                                             harvest_dates_mean_prodCountries]
        # print harvest_months_mean_prodCountries
        self.month_of_harvest = np.repeat(harvest_months_mean_prodCountries, self.n_producers_per_country)
        return prodCountry_names, self.n_producers, self.n_producers_per_country, self.month_of_harvest

    def read_harvest_timeseries(self, producfileVar, prodCountry_names, start_year, q_trade):
        """Read time-varying production data."""
        # Load time-varying production data for all countries
        with open(producfileVar, 'r') as pf:
            prodCountry_years_var_raw = pf.readline().rstrip('\n').split(';')[1:]
        # If market year is given instead of calendar year, associate data to the first calendar year of the market year (e.g. 2010 for 2010/2011)
        prodCountry_years_var = np.array(list(map(int, [ay.split('/')[0] for ay in prodCountry_years_var_raw])))
        prodCountry_names_var = np.atleast_1d(
            np.genfromtxt(producfileVar, delimiter=';', skip_header=1, usecols=(0), dtype=str))

        prodCountry_quantity_var_full = np.atleast_2d(
            np.genfromtxt(producfileVar, delimiter=';', usecols=(np.arange(1, 1 + len(prodCountry_years_var))),
                          skip_header=1, missing_values='NA', dtype=float))
        # Select only the large producers that were identified in producer_country_data
        # prodCountry_quantity_var = np.squeeze([prodCountry_quantity_var_full[prodCountry_names_var == prodCountry_name, :] for prodCountry_name in prodCountry_names])
        prodCountry_quantity_var = np.array(
            [np.squeeze(prodCountry_quantity_var_full[prodCountry_names_var == prodCountry_name, :]) for
             prodCountry_name in prodCountry_names])
        # Time selection
        if start_year:
            self.lenmin = int(prodCountry_years_var[-1]) - start_year + 1
        else:
            # Select period with complete data (generally after USSR breakdown; only USSR totals available before that). First, find length of shortest non-NaN timeseries; then crop all timeseries to that length.
            self.lenmin = min([np.size(np.where(~np.isnan(row))) for row in prodCountry_quantity_var])
        prodCountry_quantity_var = prodCountry_quantity_var[:, -self.lenmin:]
        prodCountry_years_var = prodCountry_years_var[-self.lenmin:]
        # Part available for trade
        prodCountry_quantity_var = np.multiply(prodCountry_quantity_var, q_trade)
        # prodCountry_quantity_var = np.subtract(prodCountry_quantity_var, q_trade)
        return prodCountry_quantity_var, prodCountry_years_var

    def harvest_timeseries_constant(self, t_start, t_end, shock_year, shock_month, shock_factor,
                                    prodCountry_quantity_var, prodCountry_years_var, input_year, t_per_year):
        """Repeat reference harvest for all years. Induce artificial yield shock. """
        # Initalize indices
        index_of_selectedyear = 0
        index_of_qharvest = 0

        # Select desired year
        ###prodCountry_quantity_const = prodCountry_quantity_var[:,prodCountry_years_var.index(str(input_year))]
        index_of_selectedyear = np.where(prodCountry_years_var == input_year)
        prodCountry_quantity_const = prodCountry_quantity_var[:, index_of_selectedyear]
        # Split country production equally over all domestic producers
        q_harvest_per_producer = np.divide(prodCountry_quantity_const, self.n_producers_per_country)
        self.q_harvest_const = np.repeat(q_harvest_per_producer, self.n_producers_per_country)
        # Quantities harvested (each year, in metric tons):
        index_of_qharvest = int(np.ceil((t_end - t_start + 1) / t_per_year))
        q_harvest = np.fliplr([self.q_harvest_const] * index_of_qharvest)
        ###q_harvest = np.fliplr([self.q_harvest_const]*(np.ceil((t_end-t_start+1)/t_per_year)))
        # Include artificial yield shock.
        q_harvest[shock_year, 0] = q_harvest[shock_year, 0] * shock_factor
        q_harvest[shock_year + 100, 0] = q_harvest[shock_year + 100, 0] * shock_factor
        q_harvest[shock_year + 200, 0] = q_harvest[shock_year + 200, 0] * shock_factor
        q_harvest[shock_year + 300, 0] = q_harvest[shock_year + 300, 0] * shock_factor
        # puma q_harvest[shock_year, (self.month_of_harvest==shock_month)] = q_harvest[shock_year, (self.month_of_harvest==shock_month)]*shock_factor
        # q_harvest[shock_year, (self.month_of_harvest==shock_month+1)] = q_harvest[shock_year, (self.month_of_harvest==shock_month+1)]*shock_factor
        # q_harvest[shock_year, (self.month_of_harvest==shock_month+2)] = q_harvest[shock_year, (self.month_of_harvest==shock_month+2)]*shock_factor
        # q_harvest[shock_year, (self.month_of_harvest==shock_month+3)] = q_harvest[shock_year, (self.month_of_harvest==shock_month+3)]*shock_factor
        return q_harvest

    def harvest_timeseries_varying(self, prodCountry_quantity_var, t_per_year, repeatFirstColumnNTimes):
        """Prepare time-varying production data. Start and end month are also determined here. """
        # add repeatFirstColumnNTimes times the production of the first year
        if repeatFirstColumnNTimes > 0:
            prodCountry_quantity_var = np.concatenate((np.transpose(
                np.reshape(np.tile(prodCountry_quantity_var[:, 0], repeatFirstColumnNTimes),
                           (repeatFirstColumnNTimes, -1))), prodCountry_quantity_var), axis=1)

        # Split country production equally over all domestic producers
        q_harvest_per_producer_var = np.divide(prodCountry_quantity_var,
                                               np.array(self.n_producers_per_country)[:, np.newaxis])

        q_harvest_var = np.repeat(q_harvest_per_producer_var, self.n_producers_per_country, axis=0)

        # Determine start and end month
        t_start = 1
        t_end = np.shape(prodCountry_quantity_var)[1] * t_per_year

        return np.swapaxes(q_harvest_var, 0, 1), t_start, t_end

    def consumer_country_data(self, consumfileVar, q_clip):
        """Read consumption data, clip to largest consuming countries. """
        # Load consumption data (country name, quantity in tonnes, cumulative fraction of world consumption)
        # ...atleast_1d ensures same number of dimensions even if only one country is in the list
        consCountry_names = np.atleast_1d(
            np.genfromtxt(consumfileVar, skip_header=1, delimiter=';', usecols=(0), dtype=str))
        consCountry_quantity = np.atleast_1d(
            np.genfromtxt(consumfileVar, skip_header=1, delimiter=';', usecols=(1), dtype=float))
        # Clip to largest consumers
        # consCountry_names = consCountry_names[np.cumsum(consCountry_quantity)<q_clip]
        # consCountry_quantity = consCountry_quantity[np.cumsum(consCountry_quantity)<q_clip]
        consCountry_names = consCountry_names[
            np.divide(np.cumsum(consCountry_quantity), np.sum(consCountry_quantity)) < q_clip]
        consCountry_quantity = consCountry_quantity[
            np.divide(np.cumsum(consCountry_quantity), np.sum(consCountry_quantity)) < q_clip]
        self.n_consumers = len(consCountry_names)
        return consCountry_names, self.n_consumers

    def read_demand_timeseries(self, consumfileVar, consCountry_names, q_trade):
        """Read time-varying demand data."""
        # Load time-varying demand data (in metric tons) for all countries
        with open(consumfileVar, 'r') as pf:
            consCountry_years_var_raw = pf.readline().rstrip('\n').split(';')[1:]

        # If market year is given instead of calendar year, associate data to the first calendar year of the market year (e.g. 2010 for 2010/2011)
        consCountry_years_var = np.array(list(map(int, [ay.split('/')[0] for ay in consCountry_years_var_raw])))

        consCountry_names_var = np.atleast_1d(
            np.genfromtxt(consumfileVar, delimiter=';', skip_header=1, usecols=(0), dtype=str))
        consCountry_quantity_var = np.atleast_2d(
            np.genfromtxt(consumfileVar, delimiter=';', usecols=(np.arange(1, 1 + len(consCountry_years_var))),
                          skip_header=1, missing_values='NA', dtype=float))

        # Select period with complete data (as diagnosed from harvest data; assuming that data availability is same and both datasets end in the same year!)
        consCountry_quantity_var = consCountry_quantity_var[:, -self.lenmin:]


        consCountry_years_var = consCountry_years_var[-self.lenmin:]

        # Select only the large consumers that were identified in consumer_country_data
        # consCountry_quantity_var = np.squeeze([consCountry_quantity_var[consCountry_names_var == consCountry_name, :] for consCountry_name in consCountry_names])
        consCountry_quantity_var = np.array(
            [np.squeeze(consCountry_quantity_var[consCountry_names_var == consCountry_name, :]) for consCountry_name in
             consCountry_names])
        # Part available for trade
        consCountry_quantity_var = np.multiply(consCountry_quantity_var, q_trade)
        # consCountry_quantity_var = np.subtract(consCountry_quantity_var, q_trade)
        return consCountry_quantity_var, consCountry_years_var

    def demand_timeseries_constant(self, t_start, t_end, consCountry_quantity_var, consCountry_years_var, input_year,
                                   min_frac, p_max_cons_base, q_storage_max_cons_factor, t_per_year):
        """Repeat reference monthly demand for all years. """
        # Select desired year
        ###consCountry_quantity = consCountry_quantity_var[:,consCountry_years_var.index(str(input_year))]
        consCountry_quantity = consCountry_quantity_var[:, 0]
        q_out_min_monthly = consCountry_quantity * min_frac / t_per_year
        q_out_min = np.fliplr([q_out_min_monthly] * (t_end - t_start + 1))
        # Consumers' maximum price.
        index_of_pmax = int(np.ceil((t_end - t_start + 1) / t_per_year) + 1)
        p_max_cons = np.array([np.tile(p_max_cons_base, self.n_consumers)] * index_of_pmax)
        ###p_max_cons = np.array([np.tile(p_max_cons_base, self.n_consumers)]*(np.ceil((t_end-t_start+1)/t_per_year)+1))
        # Maximum consumer storage capacity
        q_storage_max_cons = np.multiply(q_out_min, q_storage_max_cons_factor)
        # return q_out_min, p_max_cons
        return q_out_min, p_max_cons, q_storage_max_cons

    def demand_timeseries_varying(self, consCountry_quantity_var, filtersize, t_per_year, repeatFirstColumnNTimes,
                                  min_frac, p_max_cons_base, match_q_spinup, q_harvest_total_init,
                                  q_storage_max_cons_factor,q_storage_max_cons_type, q_out_offset, q_out_offset_type, consumption_fixed_reported, loss_factor):

        """Prepare time-varying consumption data."""
        ### Add offset
        if q_out_offset_type == 0:
            # Constant absolute offset
            consCountry_quantity_var = np.add(consCountry_quantity_var, np.divide(q_out_offset, self.n_consumers))
        elif q_out_offset_type == 1:
            # Multiplicative offset
            consCountry_quantity_var = np.multiply(consCountry_quantity_var, q_out_offset)
        elif q_out_offset_type == 2:
            # Spillage rate, applied in main time loop in TWIST_coreSimple.py
            pass
        elif q_out_offset_type == 3:
            # Constant absolute offset together with spillage rate
            consCountry_quantity_var = np.add(consCountry_quantity_var, np.divide(q_out_offset, self.n_consumers))
        elif q_out_offset_type == 4:

            # Multiplicative offset
            offset_part_1 = np.empty(len(consCountry_quantity_var[0])-22)
            offset_part_2 = np.empty(22)
            offset_part_1.fill(q_out_offset)
            offset_part_2.fill(loss_factor)

            offset_factor = np.concatenate([offset_part_1, offset_part_2])
            consCountry_quantity_var = np.multiply(consCountry_quantity_var, offset_factor)



        # Smoothing
        if consumption_fixed_reported == True:
            # no smoothing set to reported values
            q_out_min_annual = consCountry_quantity_var
            q_out_min_annual_cap = consCountry_quantity_var
        else:
            q_out_min_annual = np.array([runmean(timeseries, filtersize) for timeseries in consCountry_quantity_var])
            q_out_min_annual_cap = np.array([runmean(timeseries, 5) for timeseries in consCountry_quantity_var])

        # add repeatFirstColumnNTimes times the consumption of the first year
        if repeatFirstColumnNTimes > 0:
            match_factor = 1.
            if match_q_spinup == 1:
                match_factor = np.divide(q_harvest_total_init, np.sum(q_out_min_annual[:, 0]))
            q_out_min_annual = np.concatenate((np.transpose(
                np.reshape(np.tile(q_out_min_annual[:, 0] * match_factor, repeatFirstColumnNTimes),
                           (repeatFirstColumnNTimes, -1))), q_out_min_annual), axis=1)
            q_out_min_annual_cap = np.concatenate((np.transpose(
                np.reshape(np.tile(q_out_min_annual_cap[:, 0] * match_factor, repeatFirstColumnNTimes),
                           (repeatFirstColumnNTimes, -1))), q_out_min_annual_cap), axis=1)
        q_out_min_annual = q_out_min_annual * min_frac
        q_out_min_annual_cap = q_out_min_annual_cap * min_frac


        # # Assume monthly demand doesn't change within one year:
        # q_out_min = np.repeat(q_out_min_annual/12., 12, axis=1)

        # Monthly demand increases --> linear interpolation between adjacent years
        q_out_min = np.zeros((np.shape(q_out_min_annual)[0], np.shape(q_out_min_annual)[1] * t_per_year))
        q_out_min_cap = np.zeros((np.shape(q_out_min_annual_cap)[0], np.shape(q_out_min_annual_cap)[1] * t_per_year))

        # for month in np.arange(0.,np.shape(q_out_min_annual)[1]*t_per_year):
        for month in np.arange(0, np.shape(q_out_min_annual)[1] * t_per_year):
            year = int(np.floor(np.divide(month, t_per_year)))
            yearMax = int(np.shape(q_out_min_annual)[1]) - 2
            yearMax_cap = int(np.shape(q_out_min_annual_cap)[1]) - 2
            q_out_min[:, month] = np.true_divide(q_out_min_annual[:, year], t_per_year) + np.true_divide(
                np.divide(q_out_min_annual[:, min(year, yearMax) + 1], t_per_year) - np.divide(
                    q_out_min_annual[:, year], t_per_year), t_per_year) * (month % t_per_year)
            q_out_min_cap[:, month] = np.true_divide(q_out_min_annual_cap[:, year], t_per_year) + np.true_divide(
                np.divide(q_out_min_annual_cap[:, min(year, yearMax_cap) + 1], t_per_year) - np.divide(
                    q_out_min_annual_cap[:, year], t_per_year), t_per_year) * (month % t_per_year)



        # Consumers' maximum price.
        p_max_cons = np.tile(p_max_cons_base, self.n_consumers)
        # Maximum consumer storage capacity
        # q_storage_max_cons = np.multiply(np.swapaxes(q_out_min_cap,0,1), q_storage_max_cons_factor)
        # q_storage_max_cons = np.tile(np.multiply(q_out_min_cap[0,0], q_storage_max_cons_factor), np.shape(np.swapaxes(q_out_min_cap,0,1)))

        # q_storage_max_cons = np.add(np.swapaxes(q_out_min_cap, 0, 1),
        #                             np.multiply(q_out_min_cap[:, 0], q_storage_max_cons_factor - 1.))

        # Maximum consumer storage capacity
        if q_storage_max_cons_type == 0:
            # as in Jacob's 2017 paper
            q_storage_max_cons = np.add(np.swapaxes(q_out_min_cap, 0, 1),
                                        np.multiply(q_out_min_cap[:, 0], q_storage_max_cons_factor - 1.))
        elif q_storage_max_cons_type == 1:
            # constant percent above consumption
            q_storage_max_cons = np.multiply(np.swapaxes(q_out_min_cap, 0, 1), q_storage_max_cons_factor)

        elif q_storage_max_cons_type == 2:
            factor_first_part = np.linspace(q_storage_max_cons_factor, 0.1, 25)
            factor_second_part = np.linspace(0.1, q_storage_max_cons_factor-0.3, len(q_out_min_cap[0])-25)
            factor_new = np.concatenate((factor_first_part, factor_second_part))
            q_storage_max_cons = np.multiply(np.swapaxes(q_out_min_cap, 0, 1), factor_new)

            # q_storage_max_cons = np.add(np.swapaxes(q_out_min_cap, 0, 1), np.multiply(q_out_min_cap[:, 0], 0.7))


            # gradual increase in percent above consumption
            # factor = np.linspace(q_storage_max_cons_factor-0.2, q_storage_max_cons_factor, len(q_out_min_cap[0]))
            # q_storage_max_cons = np.multiply(np.swapaxes(q_out_min_cap, 0, 1), factor)

            # old
            # diff1 = np.multiply(np.swapaxes(q_out_min_cap, 0, 1), q_storage_max_cons_factor - 1.)
            # diff2 = np.multiply(list(q_out_min_cap[:, 0]) * len(q_out_min_cap[0]), q_storage_max_cons_factor - 1.)
            #
            # diff = [(diff1[i] + diff2[i]) / 2 for i in range(len(diff1))]
            # q_storage_max_cons = np.add(np.swapaxes(q_out_min_cap, 0, 1), np.array(diff))

        return np.swapaxes(q_out_min, 0, 1), p_max_cons, q_storage_max_cons, q_out_min_annual

    def countries(self, prodCountry_names):
        """Associate producers with their home countries. """
        countries_producers = np.repeat(prodCountry_names, self.n_producers_per_country)
        return countries_producers

    def producerInput(self, t_start, t_end, q_trade, q_storage_init_prod):
        """For all producers at once, provide input parameters. """
        # Storage costs per month (dollars per metric ton)
        p_storage = np.random.rand(self.n_producers) * 1 + 1
        # Initial storage level for producers:
        q_storage_prod = [np.divide(q_storage_init_prod * q_trade, self.n_producers)] * self.n_producers
        # q_storage_prod = pickle.load(open('q_storage_prod_init.pkl', 'rb'))
        return p_storage, q_storage_prod

    # def consumerInput(self, q_out_min, q_storage_max_cons_factor):
    def consumerInput(self, q_out_min):
        """For all consumers at once, provide input parameters. """
        # # Maximum consumer storage capacity
        # q_storage_max_cons = np.multiply(q_out_min, q_storage_max_cons_factor)
        # Initial storage level for consumers:
        # q_storage_cons = [20]*self.n_consumers
        q_storage_cons = np.multiply(q_out_min[0, :], 0.)
        # q_storage_cons = pickle.load(open('q_storage_cons_init.pkl', 'rb'))
        # return q_storage_cons, q_storage_max_cons
        return q_storage_cons

    def read_priceDeflator(self, CPIfile):
        """Read price index (e.g. U.S. Consumer Price Index, CPI) for converting nominal to real prices. """
        priceDeflator_year = np.genfromtxt(CPIfile, delimiter=';', usecols=(0), dtype=int)
        priceDeflator_value = np.genfromtxt(CPIfile, delimiter=';', usecols=(1), dtype=float)
        priceDeflator_list = list(zip(priceDeflator_year, priceDeflator_value))
        priceDeflator_dict = dict(priceDeflator_list)
        return priceDeflator_dict

    def exportRestrictions(self, include_exportRestr):
        exportRestr = {}
        if include_exportRestr == True:
            # if current_year == 2007:
            # # if month_of_year == 7:
            # if month_of_year in np.arange(1,13):
            # exportRestr['Ukraine'] =  1
            # if month_of_year in np.arange(1,13):
            # exportRestr['Pakistan'] =  1
            # if month_of_year in np.arange(2,13):
            # exportRestr['India'] =  1
            # if month_of_year in np.arange(3,13):
            # exportRestr['Argentina'] =  1
            # if current_year == 2008:
            # if month_of_year in np.arange(1,6):
            # exportRestr['Ukraine'] =  1
            # exportRestr['Russian Federation'] =  1
            # if month_of_year in np.arange(1,13):
            # exportRestr['India'] =  1
            # if month_of_year in np.arange(1,13):
            # exportRestr['Pakistan'] =  1
            # if current_year == 2009:
            # if month_of_year in np.arange(1,7):
            # exportRestr['India'] =  1
            # if month_of_year in np.arange(1,10):
            # exportRestr['Pakistan'] =  1
            # # exportRestr['World'] = {2007 : 0.9, 2008 : 1.}
            # # exportRestr['World'] = {2007 : 1., 2008 : 0.85}
            # # exportRestr['World'] = {2007:0.9, 2008:0.97, 2009:0.97, 2010:0.95, 2011:0.95, 2012:0.95, 2013:0.95}
            # # exportRestr['World'] = {2007:0.9, 2008:0.97, 2009:0.97, 2010:0.95}
            exportRestr['World'] = {2007:0.97, 2010:0.97}
            # exportRestr['World'] = {2019: 0.95}
            # # exportRestr['World'] = {2010:0.95}
            # # exportRestr['World'] = {2009:0.95, 2010:0.95}
            # # # exportRestr['World'] = {2007 : 1., 2008 : 0.85, 2011 : 0.85, 2012 : 0.85}
        return exportRestr

    #  def importStrategies(self, repeatFirstColumnNTimes, start_year, consCountry_names, consCountry_years_var):
    def importStrategies(self):
        importStrat = {}
        #    importStrat = cs.defaultdict(dict)
        #    ### Initialize with baseline value
        #    for n in consCountry_names:
        #      for y in np.arange(start_year-repeatFirstColumnNTimes, int(consCountry_years_var[-1])+1):
        #        importStrat[n][y] = 1.
        #    ### Nominal Assistance Coefficients (NAC; derived from World Bank data):
        #    NACfile = 'C:\Documents and Settings\schewe\My Documents\localcopy\EXPACT\data\WorldBank_AgDistortions\NAC_WheatAnnualWorld.csv'
        #    NAC_year = np.genfromtxt(NACfile, delimiter=';', usecols=(0), dtype=int)
        #    NAC_value = np.genfromtxt(NACfile, delimiter=';', usecols=(1), dtype=float)
        #    NAC_list = list(zip(NAC_year, NAC_value))
        #    NAC_dict = dict(NAC_list)
        #    # print NAC_dict[1975]
        #    # for y in np.arange(start_year, 2011+1):
        #      # # importStrat['World'][y] = (NAC_dict[y-1]-1.)*(-0.1) + 1.
        #      # importStrat['World'][y] = (NAC_dict[y]-1.)*(-0.1) + 1.05
        #    ### Replace with anomalies
        #    # importStrat['World'][2000]=0.98
        #    # importStrat['World'][2001]=0.96
        #    # importStrat['World'][2002]=0.94
        #    # importStrat['World'][2003]=0.92
        #    # importStrat['World'][2004]=0.92
        #    # importStrat['World'][2005]=0.92
        #    # importStrat['World'][2006]=0.92
        #    # # importStrat['World'][2007]=0.96
        #    # # importStrat['World'][2008]=0.96
        #    # # importStrat['World'][2009]=0.96
        #    # # importStrat['World'][2007]=1.03
        #    # # importStrat['World'][2008]=1.03
        #    # # importStrat['World'][2009]=1.03
        #    # # importStrat['World'][2010]=1.03
        #    # importStrat['World'][2011]=1.05
        #    # importStrat['World'][2012]=1.05
        #    # importStrat['World'][2013]=1.05
        #    # importStrat['World'][2014]=1.05
        #    # importStrat['World'][2015]=1.05
        #    # importStrat['World'][2016]=1.05
        return importStrat

    def producerVariables(self, t_per_year, tm, year, month_of_year, Px, p_storage, month_of_harvest, q_harvest_last,
                          Q_storage_prod, p_max_prod_const):
        """For an individual producer, compute minimum and maximum price and return them as single values. """
        # # Determine number of months since last harvest:
        # month_after_harvest = (month_of_year - month_of_harvest)%12
        # Compute or set p_min_prod and p_max_prod:
        p_min_prod = 1  # Producers don't sell anything at price zero
        p_max_prod = np.around(p_max_prod_const)
        # Make sure that always p_max_prod >= p_min_prod
        p_max_prod = max(p_max_prod, p_min_prod)
        if np.any(p_max_prod > self.n_price_units):
            print("Warning: p_max_prod is outside price axis limits!")
        if np.any(p_min_prod > self.n_price_units):
            print("Warning: p_min_prod is outside price axis limits!")

        return p_min_prod, p_max_prod

    def producerSupplyCurve(self, q_storage_prod, p_min_prod, p_max_prod, alpha_s):
        """Compute supply-curve (quantity vs. price) for an individual producer, and return it as array. """
        supply = np.zeros((self.n_price_units))
        p_min_index = max(0, int(p_min_prod - 1))
        supply[0:p_min_index] = 0
        supply[p_min_index:int(p_max_prod)] = q_storage_prod * (
            np.true_divide(np.arange(0, p_max_prod - p_min_index), (p_max_prod - p_min_index))) ** alpha_s
        supply[int(p_max_prod):] = q_storage_prod
        return supply

    def consumerDemandCurve(self, consumer, q_storage_cons, q_out, p_max_cons, q_storage_max_cons, alpha_d, abs_consumption_reduction):
        """Compute demand-curve (quantity vs. price) for an individual consumer, and return it as array. """

        demand = np.zeros((self.n_price_units))
        # demand[0:int(p_max_cons[consumer]+1)] = ((q_out[consumer] - q_storage_cons[consumer]) - (q_storage_max_cons[consumer] - q_storage_cons[consumer])
        # )*(np.true_divide(np.arange(0, int(p_max_cons[consumer] + 1)), int(p_max_cons[consumer] + 1))
        # )**alpha_d + (q_storage_max_cons[consumer] - q_storage_cons[consumer])


        demand[0:int(p_max_cons[consumer] + 1)] = ((-1.) * (q_storage_max_cons[consumer] - q_storage_cons[consumer])
                                                   ) * (np.true_divide(np.arange(0, int(p_max_cons[consumer] + 1)),
                                                                       int(p_max_cons[consumer] + 1))
                                                        ) ** alpha_d + (
                                                          q_storage_max_cons[consumer] - q_storage_cons[consumer])

        # with possible consumption reduction taken into account
        demand[0:int(p_max_cons[consumer] + 1)] = ((-1.) * (q_storage_max_cons[consumer] - q_storage_cons[consumer] - abs_consumption_reduction)
                                                   ) * (np.true_divide(np.arange(0, int(p_max_cons[consumer] + 1)),
                                                                       int(p_max_cons[consumer] + 1))
                                                        ) ** alpha_d + (
                                                          q_storage_max_cons[consumer] - q_storage_cons[consumer] - abs_consumption_reduction)

        # demand[0:int(p_max_cons[consumer]+1)] = ((q_storage_cons[consumer] - q_storage_max_cons[consumer])
        # )*(np.true_divide(np.arange(0, int(p_max_cons[consumer] + 1)), int(p_max_cons[consumer] + 1))
        # ) + (q_storage_max_cons[consumer] - q_storage_cons[consumer])


        demand[int(p_max_cons[consumer] + 1):] = 0.

        # demand[np.where(demand < 0)] = 0
        if any(demand < 0):
            print("Warning: demand has negative value(s)!")
        return demand

    def worldMarketPrice(self, glob_demand, glob_supply):
        """Find world market price = intersection of global supply- and demand-curve. Return floating-point price and integer below. """
        # Last integer price below intersection:
        # p1 = np.where(glob_demand>glob_supply)[0][-1]
        p1 = np.where(glob_demand < glob_supply)[0][0] - 1
        # slope of demand-curve
        md = glob_demand[p1 + 1] - glob_demand[p1]
        # q-axis intercept of demand-curve
        bd = glob_demand[p1] - md * p1
        # slope of supply-curve
        ms = glob_supply[p1 + 1] - glob_supply[p1]
        # q-axis intercept of supply-curve
        bs = glob_supply[p1] - ms * p1
        # Intersection
        px_float = (bs - bd) / (md - ms)
        # px = int(round(px_float))
        px = px_float
        return px, p1

    def readCSVinput(self, inputfile):
        """Helper function for reading from csv files.
        """
        with open(inputfile, 'rt') as infile:
            indat = csv.reader(infile, delimiter='\t')
            indatlist = list()
            for column in indat:
                indatlist.append(column)
        return indatlist

    def harvestingPeriod(self, t_per_year, month_of_harvest, harvest_distribution, producers):
        """Distribute harvest over multiple months around the indicated month of harvest.
            Only relevant when there are multiple time steps (months) per year.
        """
        harvest_distrYear = np.zeros((len(producers), t_per_year))
        nHarvMonths = len(harvest_distribution)
        if nHarvMonths % 2 == 0:
            print(
                'NOTE: harvest_distribution has even number of elements; assuming harvest period to be skewed towards end of year.')
        for producer in producers:
            # Wrap around indices outside of [0:11]
            month_of_harvest_ind = month_of_harvest[producer] - 1
            if nHarvMonths % 2 == 0:
                ind = np.remainder(np.arange(np.int(month_of_harvest_ind - nHarvMonths / 2 + 1),
                                             np.int(month_of_harvest_ind + nHarvMonths / 2 + 1)), t_per_year)
            else:
                ind = np.remainder(np.arange(np.int(month_of_harvest_ind - np.floor(nHarvMonths / 2)),
                                             np.int(month_of_harvest_ind + np.ceil(nHarvMonths / 2) + 1)), t_per_year)
            # Insert harvest_distribution; other months remain zero
            harvest_distrYear[producer, ind] = harvest_distribution

        return harvest_distrYear

    def read_in_stock_data(self, datafile, time_period):

        # Reported data of ending stocks
        with open(datafile, 'r') as pf:
            year_raw = pf.readline().rstrip('\n').split(';')[3:]

        # If market year is given instead of calendar year, associate data to the first calendar year of the market year
        # (e.g. 2010 for 2010/2011)
        stocks_year = np.array([int(ay.split('/')[0]) for ay in year_raw])
        stocks_data = np.genfromtxt(datafile, delimiter=';', usecols=(np.arange(3, 3 + len(stocks_year))),
                                    skip_header=1, missing_values='NA', dtype=float)

        # select the years for which the fitting given by start_year and end_year, and also change the unit from 1000 mt to mt
        index_of_years = np.where(np.isin(stocks_year, time_period))

        # change from units from 1000MT to MT
        stocks = np.array([stocks_data[i] * 1000 for i in index_of_years]).flatten()

        return stocks

    def read_in_consumption_data(self, datafile, time_period):

        # Reported data of ending stocks
        with open(datafile, 'r') as pf:
            year_raw = pf.readline().rstrip('\n').split(';')[1:]

        # If market year is given instead of calendar year, associate data to the first calendar year of the market year
        # (e.g. 2010 for 2010/2011)
        stocks_year = np.array([int(ay.split('/')[0]) for ay in year_raw])
        stocks_data = np.genfromtxt(datafile, delimiter=';', usecols=(np.arange(1, 1 + len(stocks_year))),
                                    skip_header=1, missing_values='NA', dtype=float)

        # select the years for which the fitting given by start_year and end_year, and also change the unit from 1000 mt to mt
        index_of_years = np.where(np.isin(stocks_year, time_period))

        # units in MT
        consumption = np.array([stocks_data[i] for i in index_of_years]).flatten()

        return consumption

    def read_in_production_data(self, datafile, time_period):

        # Reported data of ending stocks
        with open(datafile, 'r') as pf:
            year_raw = pf.readline().rstrip('\n').split(';')[1:]

        # If market year is given instead of calendar year, associate data to the first calendar year of the market year
        # (e.g. 2010 for 2010/2011)
        stocks_year = np.array([int(ay.split('/')[0]) for ay in year_raw])
        stocks_data = np.genfromtxt(datafile, delimiter=';', usecols=(np.arange(1, 1 + len(stocks_year))),
                                    skip_header=1, missing_values='NA', dtype=float)

        # select the years for which the fitting given by start_year and end_year, and also change the unit from 1000 mt to mt
        index_of_years = np.where(np.isin(stocks_year, time_period))

        # units in MT
        production = np.array([stocks_data[i] for i in index_of_years]).flatten()

        return production

    def read_in_price_data(self, grainPrices_file, CPIfile, time_period, cropYear_month1):

        # Read World Bank grain prices:
        grainPrices_list = self.readCSVinput(grainPrices_file)
        grainPrices_array = np.array(grainPrices_list)
        grainPrices_year = [item.split('M')[0] for item in grainPrices_array[2:, 0]]
        grainPrices_month = [item.split('M')[1] for item in grainPrices_array[2:, 0]]
        grainPrices_legends = grainPrices_array[0, 1:]
        grainPrices_data_str = grainPrices_array[2:, 1:]
        grainPrices_data_str[grainPrices_data_str == 'NA'] = np.nan
        grainPrices_data = grainPrices_data_str.astype(np.float)
        # Correct for inflation:
        priceDeflator_dict = self.read_priceDeflator(CPIfile)
        grainPrices_deflator = np.array([priceDeflator_dict[int(year)] for year in grainPrices_year if
                                         int(year) in list(priceDeflator_dict.keys())])
        grainPrices_data_cropped = np.array([row for i, row in enumerate(grainPrices_data) if
                                             int(grainPrices_year[i]) in list(priceDeflator_dict.keys())])
        grainPrices_year_cropped = np.array(
            [year for year in grainPrices_year if int(year) in list(priceDeflator_dict.keys())])
        grainPrices_deflated = np.divide(grainPrices_data_cropped, grainPrices_deflator[:, np.newaxis]) * 100.


        # annual_year_list = np.arange(float(grainPrices_year[0]), float(grainPrices_year[-1]) + 1)
        # average_annual_price = np.mean(np.array(grainPrices_deflated).reshape(-1, 12), axis=1)
        #
        grainPrices_month1 = int(grainPrices_month[0])

        if grainPrices_month1 != 1:
            grainPrices_data = grainPrices_deflated[(12 - grainPrices_month1 + 1):]
            grainPrices_year = grainPrices_year_cropped[(12 - grainPrices_month1 + 1):]
        if len(grainPrices_deflated) % 12 != 0:
            grainPrices_data_fullyears = grainPrices_deflated[:-(len(grainPrices_deflated) % 12), :]
            grainPrices_year_fullyears = grainPrices_year_cropped[:-(len(grainPrices_year_cropped) % 12)]
        else:
            grainPrices_data_fullyears = grainPrices_deflated
            grainPrices_year_fullyears = grainPrices_year_cropped

        # Average over *crop year*:
        grainPrices_data_crop = grainPrices_data_fullyears[cropYear_month1 - 1:-(12 - (cropYear_month1 - 1)), :]
        grainPrices_data_reshaped = np.reshape(grainPrices_data_crop, (-1, 12, np.shape(grainPrices_data_fullyears)[1]))
        grainPrices_data = np.mean(grainPrices_data_reshaped, axis=1)


        # get the crop year of annual price
        grainPrices_year_crop = grainPrices_year_fullyears[cropYear_month1 - 1:-(12 - (cropYear_month1 - 1))]
        grainPrices_year_reshaped = grainPrices_data_reshaped = np.reshape(grainPrices_year_crop, (-1, 12, np.shape(grainPrices_data_fullyears)[1]))
        annual_year = np.array(grainPrices_year_reshaped[:, -1]).flatten()

        # convert to a numpy array of ints in order to use np.isin() to locate the elements which are overlaping in the
        # input file and the time series to be fitted.
        # the minus one if because what is called e.g. 1960 harvest is July 1960 to June 1961,
        year_temp = np.array([int(x)-1 for x in annual_year])

        # select the years for which the fitting given by start_year and end_year
        index_of_years = np.where(np.isin(year_temp, time_period))

        # select the time series
        annual_price = np.array([grainPrices_data[i] for i in index_of_years]).flatten()

        return annual_price

    def fit_end_storage(self, commodity, time_period, grainStorage_file, grainEndStock_file, producfileVar, consumfileVar, param_fixed_value, q_storage_max_cons_type, q_out_offset_type, save_path, make_plot=False):

        import TWIST_coreSimple
        from scipy.optimize import least_squares

        workdir = os.path.dirname(os.path.realpath(__file__))
        module_inputfiles = str(workdir + '/TWIST_inputfiles.py')
        module_settings = str(workdir + '/TWIST_settings.py')


        # get the beginning stock for the time period to be fitted
        beginning_stocks = self.read_in_stock_data(grainStorage_file, time_period)

        # set the initial storage level
        q_storage_init_prod = beginning_stocks[0]

        # read in the ending stock
        ending_stocks = self.read_in_stock_data(grainEndStock_file, time_period)

        # Use the FixCon model when determining the initial storage level and the off-set parameter
        production_varying = 1
        consumption_varying = 1
        endogFinalDemand = 0
        filtersize = 0

        # the pre-set values of the "other" free parameters which will be fixed when determining the storage offset level
        alpha_s = param_fixed_value[0]
        alpha_d = param_fixed_value[1]
        p_max_prod_const = param_fixed_value[2]
        p_max_cons_base = param_fixed_value[3]
        q_storage_max_cons_factor = param_fixed_value[4]
        e_d = param_fixed_value[5]

        # define variables before the if loop
        loss_factor = 0
        q_out_offset = 0

        if q_out_offset_type == 0:
            loss_factor = 1.0
        elif q_out_offset_type == 1:
            loss_factor = 1.0
        elif q_out_offset_type == 2:
            q_out_offset = 0.0
        elif q_out_offset_type == 3:
            pass
        elif q_out_offset_type == 4:
            pass
            # # read in reported consumption
            # consumption = self.read_in_consumption_data(consumfileVar, time_period)
            # # read in reported production
            # production = self.read_in_production_data(producfileVar, time_period)
            # # ending stock value first time step derived from reported beginning stocks, production and consumption
            # derived_ending_stocks_first_year = beginning_stocks[0] + production[0] - consumption[0]
            # #  calculate the offset in the first timestep between beginning stock+harvest-consumption and the reported ending stock
            # q_out_offset = ending_stocks[0]-derived_ending_stocks_first_year

        def model(years, free_param, loss_factor, q_out_offset):

            if q_out_offset_type == 0:
                q_out_offset = free_param
                #loss_factor  = 1.0
            elif q_out_offset_type == 1:
                q_out_offset = free_param
                # loss_factor = 1.0
            elif q_out_offset_type == 2:
                #q_out_offset = 0.0
                loss_factor = free_param
            elif q_out_offset_type == 3:
                q_out_offset, loss_factor = free_param
                # loss_factor = free_param
            elif q_out_offset_type == 4:
                q_out_offset, loss_factor = free_param





            scenario_mode = False #'one_year_shock_with_proj'  # False
            scenario_start_year = 2017
            length_of_scenario = 1
            include_prodDecline = False
            prodDecline_dic = {}
            include_importStrat = False
            importStrat_dic = {}
            include_exportRestr = False
            exportRestr_dic = {}  # {2019: 1 - 0.1323}
            include_consReduction = False
            consReduction_dic = {}
            consumption_fixed_reported = True  # no smoothing


            # the parameter needed to run the fitting routine
            fitting_param = [commodity, years[0], years[0], q_storage_init_prod, production_varying,
                             consumption_varying, endogFinalDemand,
                             filtersize, q_out_offset, loss_factor, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base,
                             q_storage_max_cons_factor, e_d, scenario_mode, scenario_start_year, length_of_scenario,
                             include_prodDecline, prodDecline_dic, include_importStrat, importStrat_dic,
                             include_exportRestr, exportRestr_dic, include_consReduction, consReduction_dic,
                             consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

            # price, total_stocks, prod_stocks, cons_stocks, prod_stocks_spinup, cons_stocks_spinup, consumption, consumption_spinup, harvest, year_sim

            # Run the TWIST model with the updated settings file
            price_sim, storage_sim, prod_stocks, cons_stocks, prod_stocks_spinup, cons_stocks_spinup, consumption_sim, \
            consumption_spinup, harvest_rep, year_sim = TWIST_coreSimple.main(module_inputfiles,
                                                                                                   module_settings,
                                                                                                   fitting_param)

            # Select only the years which are overlapping with the reported data (time_period) of world market prices using
            index_of_years = np.where(np.isin(year_sim, years))
            # make into a list with only the selected years
            Q_tot_sim_fix = [storage_sim[i] for i in index_of_years[0]]

            return np.array(Q_tot_sim_fix).flatten()

        # the difference between your model and the observed data
        def residuals(free_param, end_stock_rep, years):
            return end_stock_rep - model(years, free_param, loss_factor, q_out_offset)

        # # Run fitting
        # solution = least_squares(residuals, 10 ** 6,
        #                          args=(ending_stocks, time_period),
        #                          bounds=(10 ** 4, 10 ** 8),
        #                          xtol=10 ** (-15))

        # fit the offset as a constant (like in Jacob's 2017 paper)
        if q_out_offset_type == 0:
            solution = least_squares(residuals, 10,
                                      args=(ending_stocks, time_period),
                                      bounds=(-10 ** 8, 10 ** 8),
                                      xtol=10 ** (-15))

            # best fitted value to make the total ending stock of the fixCons model be as close as possible to the reported ending
            # stock value
            q_out_offset = solution.x[0]

        # fit the offset as a multiple of consumption
        elif q_out_offset_type == 1:
            solution = least_squares(residuals, 0.97,
                                      args=(ending_stocks, time_period),
                                      bounds=(0.5, 1.5),
                                      xtol=10 ** (-15))

            # best fitted value to make the total ending stock of the fixCons model be as close as possible to the reported ending
            # stock value
            q_out_offset = solution.x[0]


        # fit the offset as a spillage rate, where part of the carryover stock from the previous time step is lost
        elif q_out_offset_type == 2:
            solution = least_squares(residuals, 0.97,
                                      args=(ending_stocks, time_period),
                                      bounds=(0.5, 1.5),
                                      xtol=10 ** (-15))

            # best fitted value to make the total ending stock of the fixCons model be as close as possible to the reported ending
            # stock value
            loss_factor = solution.x[0]

        # In case of using both a constant offset and spillage rate. Before fitting the spillage rate, shift the storage
        # content by a constant amount for the whole time series so that the ending stocks fit in the first time step
        elif q_out_offset_type == 3:
            # fit both the offset and the loss factor
            solution = least_squares(residuals, [q_out_offset, 0.97],
                                      args=(ending_stocks, time_period),
                                      bounds=([-10 ** 8, 0.5], [10 ** 8, 1.0]),
                                      xtol=10 ** (-15))
            # best fitted value to make the total ending stock of the fixCons model be as close as possible to the reported ending
            # stock value
            q_out_offset = solution.x[0]
            loss_factor = solution.x[1]

        elif q_out_offset_type == 4:
            # fit the offset as a multiple of consumption in two intervals
            solution = least_squares(residuals, [0.95, 0.97],
                                     args=(ending_stocks, time_period),
                                     bounds=([0.5, 0.5], [1.5, 1.5]),
                                     xtol=10 ** (-15))
            # best fitted value to make the total ending stock of the fixCons model be as close as possible to the reported ending
            # stock value
            q_out_offset = solution.x[0]
            loss_factor = solution.x[1]
            # # Set offset to zero then fit the loss factor.
            # q_out_offset = 0.0
            # solution = least_squares(residuals, 0.97,
            #                           args=(ending_stocks, time_period),
            #                           bounds=(0.5, 1.5),
            #                           xtol=10 ** (-15))
            #
            # loss_factor = solution.x[0]
            # # calculate offset after loss rate have been fitted
            #
            # # read in reported consumption
            # consumption = self.read_in_consumption_data(consumfileVar, time_period)
            # # read in reported production
            # production = self.read_in_production_data(producfileVar, time_period)
            # # ending stock value first time step derived from reported beginning stocks, production and consumption
            # derived_ending_stocks_first_year = beginning_stocks[0]*loss_factor + production[0] - consumption[0]
            # #  calculate the offset in the first timestep between beginning stock+harvest-consumption and the reported ending stock
            # q_out_offset = ending_stocks[0]-derived_ending_stocks_first_year


        # #
        # # best fitted value to make the total ending stock of the fixCons model be as close as possible to the reported ending
        # # stock value
        # q_out_offset = solution.x[0]

        print('')
        print('Get initial storage and fit offset parameter')
        print('time period:', time_period[0], 'to', time_period[-1])
        print('Commodity:', commodity)
        print('number of evaluations=', solution.nfev)
        print('Best fit:')
        print('q_storage_init_prod=', q_storage_init_prod)
        print('q_out_offset=', q_out_offset)
        print('loss_factor=', loss_factor)


        if make_plot:
            import matplotlib.pyplot as plt
            if q_out_offset_type == 0:
                fitted_param = q_out_offset
            elif q_out_offset_type == 1:
                fitted_param = q_out_offset
            elif q_out_offset_type == 2:
                fitted_param = loss_factor
            elif q_out_offset_type == 3:
                fitted_param = [q_out_offset, loss_factor]
            elif q_out_offset_type == 4:
                fitted_param = [q_out_offset, loss_factor]

            # get the simulated ending stock time series for the estimated best of q_out_offset

            sim_ending_stock = model(time_period, fitted_param, loss_factor, q_out_offset)

            fig = plt.figure('Ending Stock')

            frame1 = fig.add_axes((.1, .3, .8, .55))

            plt.plot(time_period, ending_stocks, 'k-', label='Reported')
            plt.plot(time_period, sim_ending_stock, 'b--', label='FixCons')
            plt.legend()
            plt.title(commodity + '\n q_out_offset=' + str(round(q_out_offset, 3)) + ' loss_factor=' + str(
                round(loss_factor, 3)), y=1.05)

            frame1.set_xticklabels([])  # Remove x-tic labels for the first frame

            # Residual plot
            residual = sim_ending_stock - ending_stocks
            frame2 = fig.add_axes((.1, .1, .8, .2))
            plt.plot(time_period, np.zeros(len(time_period)), 'k-', alpha=0.6)
            plt.plot(time_period, residual, 'b--', alpha=0.6)
            plt.tight_layout()
            plt.savefig(save_path+'/Fitting_endStorage.png')


        return q_out_offset, loss_factor, q_storage_init_prod #, residual

    def leastsq_grid_steps(self, lo_bound, up_bound, initial_guess, grid_steps):

        steps = []
        i = 0
        for number_of_steps in grid_steps:
            if number_of_steps == 1:
                # if only one step is given use the value of the initial guess
                steps.append([initial_guess[i]])
            else:
                # make linear spacing 10% from the edges, so that the values does not start at the boundary
                steps.append([initial_guess[i]] + list(np.linspace(lo_bound[i]*1.1, up_bound[i]*0.9, number_of_steps)))

            i += 1

        steps_alpha_s = steps[0]
        steps_alpha_d = steps[1]
        steps_P_max_prod = steps[2]
        steps_P_max_cons = steps[3]
        steps_I_max_cons = steps[4]

        return steps_alpha_s, steps_alpha_d, steps_P_max_prod, steps_P_max_cons, steps_I_max_cons

    def demand_timeseries_scenario(self, finalCons, q_storage_max_cons, consCountry_quantity_var, consCountry_years_var, scenario_start_year, repeatFirstColumnNTimes,length_of_scenario, q_out_offset_type, q_out_offset, consumption_fixed_reported):


        # add spin up years with the normal timeseries
        spin_up = np.repeat(consCountry_years_var[0], repeatFirstColumnNTimes)
        consumption_timeseries = np.concatenate((spin_up, consCountry_years_var))

        ### Add offset
        if q_out_offset_type == 0:
            # Constant absolute offset
            consCountry_quantity_var = np.add(consCountry_quantity_var, np.divide(q_out_offset, self.n_consumers))
        elif q_out_offset_type == 1:
            # Multiplicative offset
            consCountry_quantity_var = np.multiply(consCountry_quantity_var, q_out_offset)
        elif q_out_offset_type == 2:
            # Spillage rate, applied in main time loop in TWIST_coreSimple.py
            pass
        elif q_out_offset_type == 3:
            # Constant absolute offset
            consCountry_quantity_var = np.add(consCountry_quantity_var, np.divide(q_out_offset, self.n_consumers))

        # add the consumption during the spin-up to the original unsmoothed consumption
        initial_consumption = np.repeat(consCountry_quantity_var[0][0], repeatFirstColumnNTimes)
        consumption_unsmoothed = np.concatenate((initial_consumption, consCountry_quantity_var[0]))

        if consumption_fixed_reported == True:
            # keep the whole timeseries
            cons_historic_part = finalCons[consumption_timeseries < scenario_start_year]
            q_storage_max_hist_part = q_storage_max_cons[consumption_timeseries < scenario_start_year+length_of_scenario]
            cons_unsmoothed_hist_part = consumption_unsmoothed[consumption_timeseries < scenario_start_year+length_of_scenario]

            # # First get only the historic period
            # harvest_scenario = q_harvest[harvest_timeseries < scenario_start_year]

            # for i in range(length_of_scenario):
            #     projected_con = finalCons[consumption_timeseries == (scenario_start_year + i)]
                # projection scenario when production remains constant
                # if consumption_reduced == 1:
                #     #print('No consumption reduction') #remove for fitting
                #     cons_historic_part = np.append(cons_historic_part, projected_con)
                # # scenario where we have production failure to a certain degree, then add consumption reduction
                # else:
                #     # Apply the shock only the first year
                #     if i == 0:
                #         # print(scenario_start_year + i, ' shock, apply consumption reduction') #remove for fitting
                #         # print(consumption_reduced) #remove for fitting
                #         # cons_historic_part = np.append(cons_historic_part, projected_con * consumption_reduced)
                #         cons_historic_part = np.append(cons_historic_part, projected_con* consumption_reduced)
                #     else:
                #         # print(scenario_start_year + i, ' no production or consumption decline') #remove for fitting
                #         cons_historic_part = np.append(cons_historic_part, projected_con)
            # cons_historic_part = np.array([[x] for x in cons_historic_part])

        else:
            # Flexible consumption
            # Get only the historic period the following scenario years will be added a from the calculated flex consumption
            cons_historic_part = finalCons[consumption_timeseries < scenario_start_year]
            q_storage_max_hist_part = q_storage_max_cons[consumption_timeseries < scenario_start_year]
            cons_unsmoothed_hist_part = consumption_unsmoothed[consumption_timeseries < scenario_start_year]

        # smoothing, get the running mean values
        #cons_mean = runmean(cons_scenario[:, 0], filtersize)

        # average_of3years = np.average(cons_scenario[-3:])
        #
        # # Second determine the consumption for the scenario period and add it to the historic time series, as the average of the previous 3 years
        # for i in range(length_of_scenario):
        #     cons_scenario = np.append(cons_scenario, average_of3years)


        return np.array([[x] for x in cons_historic_part]), q_storage_max_hist_part, cons_unsmoothed_hist_part

    def harvest_timeseries_scenario_all_years(self, q_harvest, prodCountry_years_var, scenario_start_year, repeatFirstColumnNTimes,
                                    length_of_scenario, production_decline):


        # add spin up years with the normal timeseries
        spin_up = np.repeat(prodCountry_years_var[0], repeatFirstColumnNTimes)
        harvest_timeseries = np.concatenate((spin_up, prodCountry_years_var))

        # First get only the historic period
        harvest_scenario = q_harvest[harvest_timeseries < scenario_start_year]

        # setting the harvest for the shock year relative the year before the shock,
        # so using no information about either the expected harvest or reported harvest for the shock year
        # # Second add the production of the years for the scenario period
        year_before_scenario_prod = harvest_scenario[-1]

        for i in range(length_of_scenario):

            # base line scenario when production remains constant
            if production_decline == 1:
                harvest_scenario = np.append(harvest_scenario, year_before_scenario_prod)
            # scenario where we have production failure to a certain degree
            else:
                harvest_scenario = np.append(harvest_scenario, year_before_scenario_prod*production_decline)

        q_harvest = np.array([[x] for x in harvest_scenario])
        t_end = len(harvest_scenario)

        return q_harvest, t_end

    def harvest_timeseries_scenario_one_year(self, q_harvest, prodCountry_years_var, scenario_start_year,
                                        repeatFirstColumnNTimes, length_of_scenario, production_decline):

        # add spin up years with the normal timeseries
        spin_up = np.repeat(prodCountry_years_var[0], repeatFirstColumnNTimes)
        harvest_timeseries = np.concatenate((spin_up, prodCountry_years_var))

        # First get only the historic period
        harvest_scenario = q_harvest[harvest_timeseries < scenario_start_year]

        for i in range(length_of_scenario):
            projected_prod = q_harvest[harvest_timeseries == (scenario_start_year + i)]
            # projection scenario when production remains constant
            if production_decline == 1:
                # print(scenario_start_year + i, ' No production decline') # remove for fitting
                harvest_scenario = np.append(harvest_scenario, projected_prod)
            # scenario where we have production failure to a certain degree
            else:
                # Apply the shock only the first year
                if i == 0:
                    # print(scenario_start_year + i, ' shock, production decline applied') # remove for fitting
                    harvest_scenario = np.append(harvest_scenario, projected_prod * production_decline)
                else:
                    # print(scenario_start_year + i, ' No decline') # remove for fitting
                    harvest_scenario = np.append(harvest_scenario, projected_prod)
        q_harvest = np.array([[x] for x in harvest_scenario])
        t_end = len(harvest_scenario)
        timestep_of_scenario_year = np.where(harvest_timeseries == scenario_start_year)[0][0]

        return q_harvest, t_end, timestep_of_scenario_year