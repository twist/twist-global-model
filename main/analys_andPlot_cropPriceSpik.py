import os
import imp
import sys
import numpy as np
import matplotlib.pyplot as plt
import TWIST_coreSimple
import TWIST_functions
from runningmean import runmean_matchLeftEdge as runmean

# Present directory is working directory
workdir = os.path.dirname(os.path.realpath(__file__))
settings = str(workdir + '/TWIST_settings.py')
inputfiles = str(workdir + '/TWIST_inputfiles.py')

_settings_ = imp.load_source('settings', settings)
_inputfiles_ = imp.load_source('inputfiles', inputfiles)

modelbasedir = os.path.abspath(_settings_.localbasedir)
sys.path.append(modelbasedir)
dataPath = '{0}/{1}'.format(_settings_.localbasedir, 'input')
savePath = '{0}/{1}'.format(_settings_.localbasedir, 'scenario_result')




funs = TWIST_functions.Functions(_settings_.n_price_units)
time_period = np.arange(1961, 2018)


#*********************  Wheat_******************************

commodity = 'Wheat'
cropYear_month1 = 6

# get the name and path of the input file for the selected commodity
producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStock_file = _inputfiles_.inputFiles(
    commodity,
    dataPath)




# get the beginning stock for the time period to be fitted
beginning_stocks_wheat = funs.read_in_stock_data(grainStorage_file, time_period)

# read in the ending stock
end_stock_wheat = funs.read_in_stock_data(grainEndStock_file, time_period)

# read in the annual world market price
price_wheat = funs.read_in_price_data(grainPrices_file, CPIfile, time_period, cropYear_month1)

# read in reported consumption
consumption_wheat = funs.read_in_consumption_data(consumfileVar, time_period)

# read in reported production
production_wheat = funs.read_in_production_data(producfileVar, time_period)


stock_to_use_wheat = (end_stock_wheat/consumption_wheat)*100

i = 0
change_in_prod_wheat = []
change_in_storage_wheat = []
change_in_price_wheat = []
change_in_quantiity_years = []


while i < len(production_wheat)-1:
    change_in_quantiity_years.append(time_period[i+1])
    change_in_prod_wheat.append(round((production_wheat[i+1]/production_wheat[i]-1)*100, 1))
    change_in_storage_wheat.append(round((end_stock_wheat[i+1]/end_stock_wheat[i]-1)*100, 1))
    change_in_price_wheat.append(round((price_wheat[i+1]/price_wheat[i]-1)*100, 1))
    i += 1


change_storage_3yearmean_wheat = runmean(change_in_storage_wheat, 2)
change_prod_3yearmean_wheat = runmean(change_in_prod_wheat, 2)
change_price_3yearmean_wheat = runmean(change_in_price_wheat, 2)
stock_to_use_3yearmean_wheat = runmean(stock_to_use_wheat, 2)




#*********************  Rice  ******************************

commodity = 'Rice'
cropYear_month1 = 6


# get the name and path of the input file for the selected commodity
producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStock_file = _inputfiles_.inputFiles(
    commodity,
    dataPath)




# get the beginning stock for the time period to be fitted
beginning_stocks_rice = funs.read_in_stock_data(grainStorage_file, time_period)

# read in the ending stock
end_stock_rice = funs.read_in_stock_data(grainEndStock_file, time_period)

# read in the annual world market price
price_rice = funs.read_in_price_data(grainPrices_file, CPIfile, time_period, cropYear_month1)

# read in reported consumption
consumption_rice = funs.read_in_consumption_data(consumfileVar, time_period)

# read in reported production
production_rice = funs.read_in_production_data(producfileVar, time_period)


stock_to_use_rice = (end_stock_rice/consumption_rice)*100



i = 0
change_in_prod_rice = []
change_in_storage_rice = []
change_in_price_rice = []
change_in_quantiity_rice = []


while i < len(production_rice)-1:
    change_in_prod_rice.append(round((production_rice[i+1]/production_rice[i]-1)*100, 1))
    change_in_storage_rice.append(round((end_stock_rice[i+1]/end_stock_rice[i]-1)*100, 1))
    change_in_price_rice.append(round((price_rice[i+1]/price_rice[i]-1)*100, 1))
    i += 1


change_storage_3yearmean_rice = runmean(change_in_storage_rice, 2)
change_prod_3yearmean_rice = runmean(change_in_prod_rice, 2)
change_price_3yearmean_rice = runmean(change_in_price_rice, 2)
stock_to_use_3yearmean_rice = runmean(stock_to_use_rice, 2)






#*********************  Corn _******************************

commodity = 'Corn'
cropYear_month1 = 6



# get the name and path of the input file for the selected commodity
producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStock_file = _inputfiles_.inputFiles(
    commodity,
    dataPath)




# get the beginning stock for the time period to be fitted
beginning_stocks_corn = funs.read_in_stock_data(grainStorage_file, time_period)

# read in the ending stock
end_stock_corn = funs.read_in_stock_data(grainEndStock_file, time_period)

# read in the annual world market price
price_corn = funs.read_in_price_data(grainPrices_file, CPIfile, time_period, cropYear_month1)

# read in reported consumption
consumption_corn = funs.read_in_consumption_data(consumfileVar, time_period)

# read in reported production
production_corn = funs.read_in_production_data(producfileVar, time_period)


stock_to_use_corn = (end_stock_corn/consumption_corn)*100



i = 0
change_in_prod_corn = []
change_in_storage_corn = []
change_in_price_corn = []
change_in_quantiity_corn = []


while i < len(production_corn)-1:
    change_in_prod_corn.append(round((production_corn[i+1]/production_corn[i]-1)*100, 1))
    change_in_storage_corn.append(round((end_stock_corn[i+1]/end_stock_corn[i]-1)*100, 1))
    change_in_price_corn.append(round((price_corn[i+1]/price_corn[i]-1)*100, 1))
    i += 1


change_storage_3yearmean_corn = runmean(change_in_storage_corn, 2)
change_prod_3yearmean_corn = runmean(change_in_prod_corn, 2)
change_price_3yearmean_corn = runmean(change_in_price_corn, 2)
stock_to_use_3yearmean_corn = runmean(stock_to_use_corn, 2)





#_______________________ PLOT________________________________


# fig, axs = plt.subplots(1, 4, figsize=(16, 6))
#
# axs[0].set_title(commodity + ' World Market Price ')
# axs[0].plot(historic_time_period, WM_price_rep, 'k-', label='reported  WM price')
# axs[0].plot(year_sim, price, 'g-', label='Scenario')
# axs[0].plot(year_sim_base, price_base, 'b-', label='Baseline')
# axs[0].set_xlabel('Year')
# axs[0].set_ylabel('Price (US$/mt)')
# axs[0].legend()


blue = '#538cc6'
orange = '#ff8c1a'
yellow = '#ffd11a'
red = '#ff5c33'

fig, ax1 =  plt.subplots(1, 3, figsize=(16, 5))

ax1[0].plot(change_in_quantiity_years, change_in_price_wheat, '-k', label='Price')
ax1[0].plot(change_in_quantiity_years, change_in_prod_wheat, '-', color=blue, label='Production')
ax1[0].plot(change_in_quantiity_years, change_in_storage_wheat, '-', color=orange, label='End storage')
#ax1[0].plot(time_period, stock_to_use_wheat, '-', color=yellow, label='Stock-to-Ues')
ax1[0].set_ylabel('Change since previous year (%)')
ax1[0].set_xlabel('Year')
ax1[0].set_title('Wheat')
ax1[0].legend()

ax1[1].plot(change_in_quantiity_years, change_in_price_rice, '-k', label='Price')
ax1[1].plot(change_in_quantiity_years, change_in_prod_rice, '-', color=blue, label='Production')
ax1[1].plot(change_in_quantiity_years, change_in_storage_rice, '-', color=orange, label='End storage')
#ax1[1].plot(time_period, stock_to_use_rice, '-', color=yellow, label='Stock-to-Ues')
ax1[1].set_xlabel('Year')
ax1[1].set_title('Rice')
#ax1[1].legend()

ax1[2].plot(change_in_quantiity_years, change_in_price_corn, '-k', label='Price')
ax1[2].plot(change_in_quantiity_years, change_in_prod_corn, '-', color=blue, label='Production')
ax1[2].plot(change_in_quantiity_years, change_in_storage_corn, '-', color=orange, label='End storage')
#ax1[2].plot(time_period, stock_to_use_corn, '-', color=yellow, label='Stock-to-Ues')
ax1[2].set_xlabel('Year')
ax1[2].set_title('Corn')
#ax1[2].legend()



fig, ax2 =  plt.subplots(1, 3, figsize=(16, 5))

ax2[0].plot(change_in_quantiity_years, change_price_3yearmean_wheat, '-k', label='Price')
ax2[0].plot(change_in_quantiity_years, change_storage_3yearmean_wheat, '-', color=orange, label='End storage')
ax2[0].plot(time_period, stock_to_use_3yearmean_wheat, '-', color=yellow, label='Stock-to-Ues')
ax2[0].set_ylabel('3 year running mean  (%)')
ax2[0].set_xlabel('Year')
ax2[0].set_title('Wheat')
ax2[0].legend()

ax2[1].plot(change_in_quantiity_years, change_price_3yearmean_rice, '-k', label='Price')
ax2[1].plot(change_in_quantiity_years, change_storage_3yearmean_rice, '-', color=orange, label='End storage')
ax2[1].plot(time_period, stock_to_use_3yearmean_rice, '-', color=yellow, label='Stock-to-Ues')
ax2[1].set_xlabel('Year')
ax2[1].set_title('Rice')
#ax2[1].legend()

ax2[2].plot(change_in_quantiity_years, change_price_3yearmean_corn, '-k', label='Price')
ax2[2].plot(change_in_quantiity_years, change_storage_3yearmean_corn, '-', color=orange, label='End storage')
ax2[2].plot(time_period, stock_to_use_3yearmean_corn, '-', color=yellow, label='Stock-to-Ues')
ax2[2].set_xlabel('Year')
ax2[2].set_title('Corn')
#ax2[2].legend()




plt.show()
