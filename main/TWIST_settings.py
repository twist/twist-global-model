#!/usr/bin/env python
import numpy as np
import os

# ----------------------------- User settings: ---------------------------------- #

# Define output directory for figures
workdir = os.path.dirname(os.path.realpath(__file__))
subdirname = os.path.basename(workdir)

# Set the variable `localbasedir` to wherever you have located the TWIST repository locally
# TWIST_path = "/home/theresaf/Projects/TWIST/TWIST-WorldModelers"
TWIST_path = os.path.join( os.getcwd(), '..')
localbasedir = os.path.abspath(TWIST_path)




outdir = os.path.abspath(localbasedir + "/" + subdirname + "/TWIST_Results/")
if(localbasedir == "/"):
    localbasedir = '/twist'
    localsubdir = 'main'
    TWIST_path = '/twist/main/'

outdir = os.path.abspath(localbasedir + "/" + subdirname + "/TWIST_Results/")

if not os.path.isdir(outdir) and localbasedir == '/twist':
    print('The output directory for this experiment does not exist - creating it... ')
    os.mkdir(localbasedir + "/"  + subdirname + "/TWIST_Results/")


# Global (0) or multi-country (1) model?
multi_country = 0

commodity = 'Corn'  # calibrated for 'Wheat', 'Corn', 'Rice'
extendTo2016 = True
projection_mode = False  # not implemented yet
scenario_mode = False   # run with a scenario at specific year


# Set global parameters

# Constant (0) or time-varying (1) production and consumption?
production_varying = 1
consumption_varying = 1
# use reported historic consumption without smoothing it
consumption_fixed_reported = True  # for True consumption_varying should be 1

# Number of timesteps per year (integer)
t_per_year = 1

# For varying production/consumption runs, repeat initial year for spin-up
repeatFirstColumnNTimes = 10

# For varying consumption runs, set start year; if start_year is empty, max period with non-NaN data will be selected
# from harvest data file
# start_year = ''
start_year = 1962

# Number of price units:
n_price_units = 3000

# For constant production/consumption runs, set start and end months, the reference year from which to read input data,
# and timing and strength of artificial yield shock (shock_factor is the fraction by which yield is reduced):
constant_t_start = 1
constant_t_end = 350
input_year = 1975
shock_year = 2
shock_month = 1  # 7
shock_factor = 1  #0.1  # 1=> no shock

# For running a scenario (e.g. a drought like the one 2007/2008) set which year the event happens
scenario_start_year = 2018
length_of_scenario = 1  # in years

# production_decline = 1  # 1 => base line scenario, otherwise the percentile decrease compared to year before scenario
include_prodDecline = False
prodDecline_dic = {}  # 1 => base line scenario, example prodDecline_dic = {2007 : 1., 2008 : 0.85} 15% decline in 2018

# run with or without export restrictions
include_exportRestr = False
exportRestr_dic = {}  # 1 => base line scenario, example exportRestr_dic = {2007 : 1., 2008 : 0.85}

# global demand reduced because the country with export restriction is feeding their people directly,
# the absolute amount of consumption which is reduced is removed directly from the producer storage
include_consReduction = False
consReduction_dic = {}  # 1 => base line scenario, consReduction_dic = {2007 : 1., 2008 : 0.85}

# changed in
include_importStrat = False
importStrat_dic = {}


if multi_country == 1:
    # Select only the largest (by volume) countries...
    # # ...by use of a common quantity, e.g. 600 Mio. tons, which is less than
    # 90% of total production, but more than 90% of total consumption:
    # # q_clip = 6.e8
    # Alternatively, use cumulative *fraction* of total quantity, instead
    # of absolute quantity:
    q_clip = 0.8
elif multi_country == 0:
    # # If a single "country" (e.g. world total) is given, make sure it is
    # included by setting q_clip>1:
    q_clip = 1.001

# Fraction of total production and consumption (*after* q_clip) considered available for trade
q_trade = 1.0

# Final demand (anomalies) prescribed to reported consumption (0, FixCons) or endogenous (1, FlexCons)?
# (If prescribed, set filtersize = 0 for reported annual consumption)
endogFinalDemand = 0

# # Add offset to reported consumption values (q_out_offset_type: 0 for constant absolute offset; 1 for multiplicative offset; 2 for percentile spillage rate of carry over stocks )
q_out_offset_type = 0
if commodity == 'Wheat':
    q_out_offset = 2.7e6 * q_trade  # wheat
    loss_factor = 1 # 1 -> no loss, 0.97 -> means 3%
elif commodity == 'Corn':
    #q_out_offset = 1.2e6 * q_trade  # for corn
    q_out_offset = 1.e6 * q_trade  # for corn
    loss_factor = 1
# elif commodity == 'Rice':
#     q_out_offset = 0.7e6 * q_trade  # rice
#     loss_factor = 1
elif commodity == 'Sorghum':
    #q_out_offset = 1.e6 * q_trade
    #q_out_offset = 0.4e6 * q_trade  # sorghum
    q_out_offset = 47427 * q_trade  # sorghum
elif commodity == 'Sugar':
    q_out_offset = 0.


# Set size of moving window to smooth the demand input data. Total window size is 2*filtersize + 1, i.e.
# filtersize = 0 means no smoothing, filtersize = 5 means 11 years average
if endogFinalDemand == 1:
    filtersize = 5
else:
    filtersize = 0


# Should total demand match total harvest during spin-up? (Not necessarily the case in a given year.) 1: yes. 0: no.
match_q_spinup = 1

# Number of producers in the smallest selected producing country (will be scaled up for other countries):
n_producers_smallest = 1.

# Distribution of harvest over a multi-month period (needs to sum up to 1.)
harvest_distribution = np.repeat(1. / t_per_year, t_per_year)

# exponent of supply curve
alpha_s = 0.8  # 0.1

# exponent of demand curve
alpha_d = 1.

# Producers' maximum price (baseline):
if commodity == 'Wheat':
    p_max_prod_const = 350
elif commodity == 'Corn':
    p_max_prod_const = 350
elif commodity == 'Rice':
    p_max_prod_const = 350
elif commodity == 'Sorghum':
    p_max_prod_const = 600
elif commodity == 'Sugar':
    p_max_prod_const = 850

# Consumers' maximum price:
if commodity == 'Wheat':
    p_max_cons_base = 850
elif commodity == 'Corn':
    p_max_cons_base = 850
elif commodity == 'Rice':
    p_max_cons_base = 850
elif commodity == 'Sorghum':
    p_max_cons_base = 1200
elif commodity == 'Sugar':
    p_max_cons_base = 1500

# Determine the maximum consumer storage from the reported consumption  (storage_type: 0 as in the paper,
# I_max_con = Q_con + I_com_+, where I_com_+ is determined from the staring year and then keep constant),
# (storage_type: 1  I_max_con = Q_con * I_com_+ a constant percent higher then the yearly consumption )
q_storage_max_cons_type = 0

# Consumer storage capacity (units of average monthly demand):
if commodity == 'Wheat':
    q_storage_max_cons_factor = 1.55
elif commodity == 'Corn':
    q_storage_max_cons_factor = 1.55
elif commodity == 'Rice':
    q_storage_max_cons_factor = 1.55
elif commodity == 'Sorghum':
    q_storage_max_cons_factor = 1.55
elif commodity == 'Sugar':
    q_storage_max_cons_factor = 1.

# Initial producer stocks (used to match starting year reported stocks)
if commodity == 'Wheat':
    q_storage_init_prod = .8e8  # for wheat
elif commodity == 'Corn':
    #q_storage_init_prod = .56e8  # for Corn
    q_storage_init_prod = .5e8  # for Corn
elif commodity == 'Rice':
    q_storage_init_prod = .12e8  # for Rice
elif commodity == 'Sorghum':
    #q_storage_init_prod = .23e8  # for Sorghum
    q_storage_init_prod = 7656000  # for Sorghum
elif commodity == 'Sugar':
    # q_storage_init_prod = .17e8 # for sugar
    # q_storage_init_prod = .27e8 # for sugar FAO
    q_storage_init_prod = 1.e8  # for projection mode


# Parameters for endogenous final demand:
# Fraction of observed demand that is considered the necessary minimum (assuming reported demand (actual domestic
# supply) is somewhat higher than minimum):
min_frac = 1.

# Price elasticity of final demand (example: Sharma (2011) cites -0.1436 for soybeans, according to Dowd (2009);
# Headey (2010) estimates a net short-run elasticity for wheat of -0.3.)
elast_finalDemand = -0.1

# Number of time steps to compute reference price
tau = 3

# Consumer storage level at which final demand equals observed long-term average demand, in units of average consumption
# per timestep
# # storFrac_mid = 1.24
# storFrac_mid = 1.2

# Cutoff level below which final demand is not allowed to drop, as fraction of q_out_mid
q_out_cutoff = 0.

# Seed the random number generator? 
np.random.seed(42)

# --------------------------- Global plot settings ----------------------------- #
qty_multiplier = 1.e-6
ticklsize = 17.
axlsize = 17.

# Plot a set of supply and demand curves in the middle of the simulation? (This is done in the core module!)
plotExampleCurves = True

# --------------------------- End of user settings. ----------------------------- #