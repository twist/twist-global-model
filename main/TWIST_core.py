#!/usr/bin/env python

# -------------------------------------------------------------------------- #
# Core module of the TWIST 
# (Trade WIth STorage) model. 
#
# jacob.schewe@pik-potsdam.de
# Converted to Python 3, 6/9/2018 (mjp38@columbia.edu)
# -------------------------------------------------------------------------- #

import numpy as np
import pickle
import matplotlib.pyplot as plt
import os, sys
import imp

import TWIST_functions
import TWIST_plotFunctions

class TWIST:
    """
    TWIST class. Can be invoked with:

    ```
    TWIST invocation stub code goes here 
    ```
    """

    def __init__(self, module_inputfiles, module_settings):
        self.inputfiles = imp.load_source('inputfiles', module_inputfiles)
        self.settings = imp.load_source('settings', module_settings)
        self.TWIST_plot = imp.load_source('TWIST_plot', os.path.abspath(self.settings.workdir + '/TWIST_plot.py'))
        self.dataPath = '{0}/{1}'.format(self.settings.localbasedir, 'input')
        

    def initialize(self):
        """
        Initialize TWIST
             Loads input data as specified in TWIST_inputfiles.py;
             imports settings from TWIST_settings.py;
             runs the model; 
             outputs results into a pickle file; 
             calls plot module TWIST_plot.py. 
        """

        # -------------------- Get input data and parameters --------------------

        self.producfileVar, self.consumfileVar, self.iso3file, self.harvDatefile, self.CPIfile, self.grainPrices_file, self.grainStorage_file = self.inputfiles.inputFiles(self.settings.commodity, self.dataPath, self.settings.multi_country, self.settings.extendTo2016, self.settings.projection_mode)

        self.funs                                                                                                        = TWIST_functions.Functions(self.settings.n_price_units)
        self.prodCountry_names, self.n_producers, \
        self.n_producers_per_country, self.month_of_harvest                                 = self.funs.producer_country_data(self.producfileVar, self.iso3file, self.harvDatefile, self.settings.q_clip, self.settings.n_producers_smallest)
        self.consCountry_names, self.n_consumers                                                        = self.funs.consumer_country_data(self.consumfileVar, self.settings.q_clip)
        self.prodCountry_quantity_var, self.prodCountry_years_var                     = self.funs.read_harvest_timeseries(self.producfileVar, self.prodCountry_names, self.settings.start_year, self.settings.q_trade)
        self.consCountry_quantity_var, self.consCountry_years_var                     = self.funs.read_demand_timeseries(self.consumfileVar, self.consCountry_names, self.settings.q_trade)

        # Lists of Producers and consumers
        self.producers = np.arange(0, self.n_producers)
        self.consumers = np.arange(0, self.n_consumers)

        # Get n_producers x t_per_year array of fraction of annual harvest per month/timestep
        self.harvest_distrYear = self.funs.harvestingPeriod(self.settings.t_per_year, self.month_of_harvest, self.settings.harvest_distribution, self.producers)


        if (self.settings.production_varying == 0):
            # For constant annual harvests: 
            self.t_start = self.settings.constant_t_start
            self.t_end = self.settings.constant_t_end
            self.q_harvest                                                                 = self.funs.harvest_timeseries_constant(self.t_start, self.t_end, self.settings.shock_year, self.settings.shock_month, self.settings.shock_factor, self.prodCountry_quantity_var, self.prodCountry_years_var, self.settings.input_year, self.settings.t_per_year)
        elif (self.settings.production_varying == 1):
            # For time-varying annual harvests: 
            self.q_harvest, self.t_start, self.t_end                                 = self.funs.harvest_timeseries_varying(self.prodCountry_quantity_var, self.settings.t_per_year, self.settings.repeatFirstColumnNTimes)
        if (self.settings.consumption_varying == 0):
            # For constant monthly demand: 
            self.finalCons, self.p_max_cons, self.q_storage_max_cons                             = self.funs.demand_timeseries_constant(self.t_start, self.t_end, self.consCountry_quantity_var, self.consCountry_years_var, self.settings.input_year, self.settings.min_frac, self.settings.p_max_cons_base,    self.settings.q_storage_max_cons_factor, self.settings.t_per_year)
        elif (self.settings.consumption_varying ==1):
            # For time-varying monthly demand: 
            self.q_harvest_total_init = np.sum(self.q_harvest[0,:])
            self.finalCons, self.p_max_cons, self.q_storage_max_cons, self.q_out_min_annual                                = self.funs.demand_timeseries_varying(self.consCountry_quantity_var, self.settings.filtersize, self.settings.t_per_year, self.settings.repeatFirstColumnNTimes, self.settings.min_frac, self.settings.p_max_cons_base, self.settings.match_q_spinup, self.q_harvest_total_init, self.settings.q_storage_max_cons_factor, self.settings.q_out_offset, self.settings.q_out_offset_type)
        # Further parameters defining agents' behaviour:
        self.p_storage, self.q_storage_prod                                                                 = self.funs.producerInput(self.t_start, self.t_end, self.settings.q_trade, self.settings.q_storage_init_prod)
        self.q_storage_cons                                                                                        = self.funs.consumerInput(self.finalCons)
        self.countries_producers                                                                             = self.funs.countries(self.prodCountry_names)

        # Create an *independent* array
        self.q_out_min = np.array([item for item in self.finalCons])


        # Load (prescribed) export restrictions and import strategies
        self.exportRestr = self.funs.exportRestrictions()
        self.importStrat = self.funs.importStrategies()
        ###importStrat = funs.importStrategies(settings.repeatFirstColumnNTimes, settings.start_year, consCountry_names, consCountry_years_var)

        # Global plot settings
        self.qty_multiplier = self.settings.qty_multiplier 
        self.ticklsize = self.settings.ticklsize 
        self.axlsize = self.settings.axlsize 
        # Load plot functions
        self.plotFunctions = TWIST_plotFunctions.PlotFunctions(self.settings.outdir, self.settings.t_per_year, self.settings.start_year, self.t_start, self.t_end, self.settings.repeatFirstColumnNTimes, self.settings.consumption_varying, self.qty_multiplier, self.axlsize, self.ticklsize, self.settings.endogFinalDemand, subplots=False)


        # Initialize arrays
        # Supply- and demand-curves
        self.supply_prod = np.zeros((self.n_producers, self.settings.n_price_units))
        self.demand_cons = np.zeros((len(self.consumers), self.settings.n_price_units))

        # Balance of residual quantities that arise from discrete nature of the curves:
        self.q_resid = 0
        # Sum of unsold produce discarded by producers:
        self.q_discard = 0
        # Counter of consumers running out of stocks:
        self.n_cons_fail = 0
        # Lists for recording time step results:
        self.Px = []
        # Q_out = []
        self.Q_resid = []
        self.Q_storage_prod = np.empty(((self.t_end-self.t_start+1), self.n_producers))
        self.Q_storage_cons = np.empty(((self.t_end-self.t_start+1), self.n_consumers))
        self.Q_out = np.empty(((self.t_end-self.t_start+1), self.n_consumers))
        self.P_min_prod = np.empty(((self.t_end-self.t_start+1), self.n_producers))
        self.P_max_prod = np.empty(((self.t_end-self.t_start+1), self.n_producers))
        self.q_harvest_last = np.zeros((self.n_producers))

        # Log file: 
        self.fo = 'TWIST_out.txt'

        # --------------------------- Run ----------------------------- #

        # Tell user what they chose
        self.t = self.t_start
        return 'We are ready to run at {0} time steps ("months") per year.'.format(self.settings.t_per_year)
        
    

    def update(self):
        """
        Updates model by one time step
        """
        if self.t <= self.t_end: 
            self.month_total = self.t
            self.year = int(np.floor(np.divide((self.t-1),self.settings.t_per_year)))
            self.month_of_year = self.t-(self.year)*self.settings.t_per_year
            
            fo = open(self.fo, 'a')
            print('This is month ', int(self.month_of_year), ' of year ', int(self.year), file=fo)
            

            # Producer side: 
            # Harvest and computation of individual and global supply-curves
            for producer in self.producers:
                # Harvest
                if self.harvest_distrYear[producer, self.month_of_year-1] != 0:
                    # Calculate this month's new harvest
                    self.new_harvest = self.q_harvest[self.year, producer]*self.harvest_distrYear[producer, self.month_of_year-1]
                    # Add new harvest to storage
                    self.q_storage_prod[producer] += self.new_harvest
                    print('Producer ', producer, ' has harvested ', self.new_harvest, ' units.', file=fo)
                    # Record last harvest quantity for use in producerVariables
                    self.q_harvest_last[producer] = self.new_harvest
                # print 'Producer ', producer, ' storage level is ', q_storage_prod[producer], ' units.'
                # Determine p_min_prod, p_max_prod
                self.p_min_prod, self.p_max_prod, self.alpha = self.funs.producerVariables(self.settings.t_per_year, self.t, self.year, self.month_of_year, self.Px, self.p_storage[producer], self.month_of_harvest[producer], self.q_harvest[self.year, producer], self.Q_storage_prod[:,producer], self.settings.p_max_prod_const) 
                # print >>fo, "Supply curve exponent alpha is ", alpha
                self.P_min_prod[self.t-1, producer] = self.p_min_prod
                self.P_max_prod[self.t-1, producer] = self.p_max_prod
                # (Prescribed) export restrictions make some of the storage volume unavailable for trade
                try:
                    self.export_multiplier = self.exportRestr[self.countries_producers[producer]][self.year-self.settings.repeatFirstColumnNTimes+self.settings.start_year]
                except KeyError: 
                    self.export_multiplier = 1.
                # Numerical supply-curve (1-D array):
                # supply_prod[producer] = funs.producerSupplyCurve(q_storage_prod[producer], p_min_prod, p_max_prod, alpha)
                self.supply_prod[producer] = self.funs.producerSupplyCurve(self.q_storage_prod[producer]*self.export_multiplier, self.p_min_prod, self.p_max_prod, self.alpha)
                # # (Prescribed) export restrictions modify supply curve
                # if countries_producers[producer] in exportRestr.keys():
                    # print current_year, month_of_year, 'Export restriction in ', countries_producers[producer], '. Removing ', exportRestr[countries_producers[producer]]*100., 'percent from supply curve of producer ', producer
                    # supply_prod[producer] = supply_prod[producer]*(1. - exportRestr[countries_producers[producer]])
                     
            # print >>fo, "Producer storage levels are ", q_storage_prod
            # print >>fo, "Last harvests were ", q_harvest_last
            
            # Construct global supply-curve
            self.glob_supply = np.sum(self.supply_prod, axis=0)

            # Consumer side: 
            # Determine q_out
            # ...of first time step (when there is no history); to use for construction of demand curve, not for actual consumption
            if self.t == 1:
                self.q_out = self.q_out_min[self.t-1,:]
            # Export restrictions (if applicable) and computation of individual demand-curves
            for consumer in self.consumers: 
                # ### (Prescribed) export restrictions
                # if consCountry_names[consumer] in exportRestr.keys():
                    # # Find domestic producers and transfer some or all of their storage contents to consumer
                    # producers_dom = np.squeeze(np.where(countries_producers == consCountry_names[consumer]))
                    # q_available = np.sum([q_storage_prod[p] for p in producers_dom])
                    # q_wanted = q_storage_max_cons[t-1, consumer] - q_storage_cons[consumer]
                    # if q_available <= q_wanted: 
                        # for p in producers_dom:
                            # print 'Removing ', q_storage_prod[p], 'from storage of producer', p
                            # q_storage_prod[p] = 0.
                        # print 'Adding ', q_available, 'to storage of consumer', consumer
                        # q_storage_cons[consumer] += q_available
                    # elif q_available > q_wanted: 
                        # # print 'Consumer ', consumer, 'has room for ', q_wanted, '; domestic producers have ', q_available, 'available.'
                        # for p in producers_dom:
                            # print 'Removing ', q_storage_prod[p]*np.divide(q_wanted, q_available), 'from storage of producer', p
                            # q_storage_prod[p] -= q_storage_prod[p]*np.divide(q_wanted, q_available)
                        # print 'Adding ', q_wanted, 'to storage of consumer', consumer
                        # q_storage_cons[consumer] += q_wanted
                # print 'Consumer ', consumer, ' storage level is ', q_storage_cons[consumer], ' units.'
                ### Prescribed import strategies are expressed as changes in maximum consumer storage capacity 
                ###import_multiplier = importStrat[consCountry_names[consumer]][year-settings.repeatFirstColumnNTimes+settings.start_year]
                ### Numerical demand-curve (1-D array):
                # demand_cons[consumer] = funs.consumerDemandCurve(consumer, q_storage_cons, q_out, p_max_cons[year,:], q_storage_max_cons[t-1,:]*import_multiplier)
                #demand_cons[consumer] = funs.consumerDemandCurve(consumer, q_storage_cons, q_out, p_max_cons[:], q_storage_max_cons[t-1,:]*import_multiplier)
                self.demand_cons[consumer] = self.funs.consumerDemandCurve(consumer, self.q_storage_cons, self.q_out, self.p_max_cons[:], self.q_storage_max_cons[self.t-1,:])            
            # Construct global demand-curve
            self.glob_demand = np.sum(self.demand_cons, axis=0)

            # Find world market price = intersection of global supply- and demand-curve. 
            self.px, self.p1 = self.funs.worldMarketPrice(self.glob_demand, self.glob_supply)
            if np.isnan(self.px):
                print('Error: world market price is NaN.')
                sys.exit("Aborting...")
            print('World market price is ', self.px, ', last integer below is ', self.p1, file=fo)
            self.Px.append(self.px)
            
            # Calculate global traded quantity 
            # # Trade only as much as consumers want to buy:
            # # slope
            # m = glob_demand[p1+1] - glob_demand[p1]
            # # q-axis intercept
            # b = glob_demand[p1] - m*p1
            # # qx
            # qx_glob = m*px + b
            # Trade only as much as producers want to sell:
            # slope
            self.m = self.glob_supply[self.p1+1] - self.glob_supply[self.p1]
            # q-axis intercept
            self.b = self.glob_supply[self.p1] - self.m*self.p1
            # qx
            self.qx_glob = self.m*self.px + self.b

            # Transactions
            self.sold_units = 0
            for producer in self.producers: 
                # Compute qx = q(px) analytically to avoid underestimation due to discrete p-axis
                # slope
                self.m = self.supply_prod[producer,self.p1+1] - self.supply_prod[producer,self.p1]
                # q-axis intercept
                self.b = self.supply_prod[producer,self.p1] - self.m*self.p1
                # qx
                self.qx = self.m*self.px + self.b
                # Sell
                self.q_storage_prod[producer] -= self.qx
                # q_storage_prod[producer] -= new_harvest
                if self.q_storage_prod[producer] < 0:
                    print('Error: Producer ', producer, ' storage level has become negative!')
                    sys.exit("Aborting...")
                # Count sold units 
                self.sold_units += self.qx
                # if producer == 1:
                    # print 'Producer ', producer, ' has sold ', (qx), ' units. Storage level is ', q_storage_prod[producer], ' units.'
            self.Q_storage_prod[self.t-1] = self.q_storage_prod

            self.purchased_units = 0
            for consumer in self.consumers: 
                # Compute qx = q(px) analytically to avoid underestimation due to discrete p-axis
                # slope
                self.m = self.demand_cons[consumer,self.p1+1] - self.demand_cons[consumer,self.p1]
                # q-axis intercept
                self.b = self.demand_cons[consumer,self.p1] - self.m*self.p1
                # qx
                self.qx = self.m*self.px + self.b
                # Buy
                self.q_storage_cons[consumer] += self.qx
                # Count purchased units 
                self.purchased_units += self.qx
                # print 'Consumer ', consumer, ' has bought ', (qx), ' units. Storage level is ', q_storage_cons[consumer], ' units.'

            # Determine new q_out; this is what will be consumed this month
            # Completely isoelastic final demand (assuming relative consumer storage level as proxy for domestic price):
            if self.settings.endogFinalDemand == 1: 
                if self.t<=self.settings.repeatFirstColumnNTimes:
                    self.q_out = self.q_out_min[self.t-1,:] 
                elif self.t>self.settings.repeatFirstColumnNTimes:
                    self.q_out_mid = self.q_out_min[self.t-1,:]/self.settings.min_frac # Retrieving 100% of observed (long-term average) demand
                    # q_out = q_out_mid*np.true_divide(q_storage_cons, storFrac_mid*q_storage_max_cons[t-1,:])**(settings.elast_finalDemand*(-1)) # Avoid division by zero
                    # q_out = q_out_mid*np.true_divide(q_storage_cons, storFrac_mid*finalCons[t-1,:])**(settings.elast_finalDemand*(-1)) # Avoid division by zero
                    ### Reference price: Average of previous years
                    self.tau = 3
                    self.pref = np.mean(self.Px[-self.tau:])
                    ### Domestic price index: World price modulated by relative storage level
                    self.pdom = self.px/self.pref
                    ### Consumption
                    self.q_out = self.q_out_mid*(self.pdom**self.settings.elast_finalDemand) 
                    self.q_out[self.q_out<(self.settings.q_out_cutoff*self.q_out_mid)] = self.settings.q_out_cutoff*self.q_out_mid[self.q_out<(self.settings.q_out_cutoff*self.q_out_mid)] # Cut-off 
            elif self.settings.endogFinalDemand == 0: 
                self.q_out = self.q_out_min[self.t-1,:]
                if self.t>self.settings.repeatFirstColumnNTimes:
                    self.q_out = self.q_out_min[self.t-1,:]
                    
            for consumer in self.consumers: 
                # Consume
                if (self.q_storage_cons[consumer] - self.q_out[consumer]) < 0: 
                    self.shortage = abs(self.q_storage_cons[consumer] - self.q_out[consumer])
                    print('Consumer ', consumer, ' has run out of stocks! Storage level falls short of required consumption by ', self.shortage, ' units.', file=fo)
                    self.n_cons_fail += 1
                    self.q_storage_cons[consumer] = 0.
                    self.Q_out[self.t-1,consumer] = self.q_out[consumer] - self.shortage
                    # # ***Dynamic export restrictions***: Compute sum of stocks of the producers in this country; compute common fraction that has to be deducted from their stocks
                    # domestic_producers = ravel(where(countries_producers==consumer))
                    # prodsum = 0
                    # for dp in domestic_producers: 
                        # prodsum += q_storage_prod[dp]
                    # print >>fo, 'Sum of stocks of domestic producers is ', prodsum, ' units.'
                    # if prodsum == 0: 
                        # continue
                    # else: 
                        # fract = min(shortage/prodsum, 1)
                        # print >>fo, 'Deducting ', fract*100, '% of stocks from domestic producers and adding it to consumer storage. NOTE: Not yet implemented. '
                else:
                    self.q_storage_cons[consumer] -= self.q_out[consumer]
                    self.Q_out[self.t-1,consumer] = self.q_out[consumer]
                # # Neglecting missing quantity. 
                # q_storage_cons[consumer] = max(0, q_storage_cons[consumer])
                # print 'Consumer ', consumer, ' has consumed ', q_out[consumer], ' units. Storage level is ', q_storage_cons[consumer], ' units.'
                self.Q_storage_cons[self.t-1] = self.q_storage_cons
                
            # Check for residual quantity
            # print 'Total units sold, purchased: ', sold_units, purchased_units
            # print 'difference between units sold and units purchased is ', (sold_units - purchased_units)
            self.q_resid += (self.sold_units - self.purchased_units)
            self.Q_resid.append(self.q_resid)
            

            if self.settings.plotExampleCurves: 
                # Example plot of supply- and demand curves at a given time:
                self.plotm = 10 
                if (self.settings.production_varying == 1):
                    self.plotm += self.settings.repeatFirstColumnNTimes*self.settings.t_per_year
                if self.t in [self.plotm, (self.plotm + 1)]: 
                    self.figNum = (self.t-self.plotm+1)
                    self.figName = 'supply_demand_curves.pdf'
                    self.figSave = 0
                    self.plotFunctions.plotSupplyDemandCurves(self.figNum, self.t, self.glob_supply, self.glob_demand, self.figName, self.figSave)
            
            # Step up time counter
            self.t += 1
            fo.close()
            return 'We have run one time step for year {0} and month {1}'.format(self.year, self.month_of_year)
            

    def finalize(self):
        """
        Runs model until completion
        """
        while self.t <= self.t_end:
            self.update()

        ##### Pickle #####

        # Results file: 
        fp = open('TWIST_results.pkl','wb')
        pickle.dump((self.Px, self.Q_out, self.Q_storage_prod, self.Q_storage_cons, self.P_min_prod, self.P_max_prod), fp)
        fp.close()

        # # Final producer and consumer storage levels, to use for initialization: 
        # fp = open('q_storage_prod_init.pkl','wb')
        # pickle.dump((q_storage_prod), fp)
        # fp.close()
        # fp = open('q_storage_cons_init.pkl','wb')
        # pickle.dump((q_storage_cons), fp)
        # fp.close()

        ### Call plot module ###
        
        # Switch on interactive mode so that the plot module may plot on the screen
        plt.ion()
        
        print("\nCalling plot module...\n")
        self.TWIST_plot.main(self.inputfiles, self.settings)

        print('---------- Summary ----------')
        print('We have run for ', self.month_total, ' time steps.') 
        print('Residual quantity of goods is ', self.q_resid, '.')
        print('Consumers have run out of stocks ', self.n_cons_fail, ' times.')
        print('Producers have discarded ', self.q_discard/1.e9, ' billion units of unsold produce.')            

        return 'Model run has completed.'


    
if __name__ == '__main__':
    # TWIST_core.py executed as script
    # 
    # As we're running in an interactive session, reload modules in case they have been changed:
    print("Caution: Executing TWIST main module directly. \
        Make sure the inputfiles and settings used are as desired.")

    # Present directory is working directory
    workdir = os.path.dirname(os.path.realpath(__file__))
    inputfiles = str(workdir + '/TWIST_inputfiles.py')
    settings = str(workdir + '/TWIST_settings.py')

    # Instantiate the model:
    twist = TWIST(inputfiles, settings)
    twist.initialize()

    # Run the model:
    twist.finalize()