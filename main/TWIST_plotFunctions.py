import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
import os
import matplotlib.patches as patches
from runningmean import runmean

class PlotFunctions:
  """Plot functions for TWIST model."""
  
  def __init__(self, outdir, t_per_year, start_year, t_start, t_end, repeatFirstColumnNTimes, consumption_varying, qty_multiplier, axlsize, ticklsize, endogFinalDemand, subplots=False):
    self.t_per_year = t_per_year
    self.start_year = start_year
    self.repeatFirstColumnNTimes = repeatFirstColumnNTimes
    self.axlsize = axlsize
    self.ticklsize = ticklsize
    self.outdir = outdir
    self.t_start = t_start
    self.t_end = t_end
    self.qty_multiplier = qty_multiplier
    self.consumption_varying = consumption_varying
    self.endogFinalDemand = endogFinalDemand
    self.subplots = subplots
    self.modelcolorFix = [178/256.,171/256.,210/256.]
    self.modelcolorFlex = np.divide([241,163,64],256.)
    # self.figuresize = (8,5)
    self.figuresize = None
    # self.aspect_ratio = .07
    self.aspect_ratio = .2
    self.x_axis_extension = 0.5
    self.xlims = [self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*self.x_axis_extension]
    # self.xlims = [self.repeatFirstColumnNTimes*self.t_per_year+15, self.t_end + self.t_per_year*self.x_axis_extension]
    self.SpecialLineStyle1 = '-.'
    self.SpecialColor1 = 'grey'
    # self.SpecialColor1 = 'lightcoral'
    # self.SpecialColor1 = 'burlywood'
    # self.SpecialColor2 = 'lightgrey'
    self.SpecialColor2 = 'mistyrose'
    self.SpecialColor3 = 'gainsboro'
    
  def plotPrice(self, fign, taxis, Px_load, grainPrices_data, grainPrices_year, grainPrices_month1, grainPrices_legends, figName, figSave, deflate=None, cropYear_month1=1, xlabels=True, yaxis_right=False, ndiff=0):
    """
    Manipulate reported price timeseries, and plot it along with simulated price. 
    Optional arguments: 
      deflate: Correct for inflation. 
      cropYear_month1: Specify 1st calendar-month of crop year. If 1, crop year is identical to calendar year. 
      ndiff: If 0 (default), plot annual price; if 1, plot price difference to previous year.
    """


    if np.shape(Px_load)[0]==2: 
      Px = Px_load[0]
      Px_Fix = Px_load[1]
    else: 
      Px = Px_load
    if self.subplots:
      self.multifig = plt.figure(6, figsize=(8,15))
      # self.multifig = plt.figure(6, figsize=(8,10)) # for two panels
      self.multifig.subplots_adjust(left=0.2, hspace=0.)
      plt.clf()
      # self.multifig.add_subplot(311)
      self.multifig.add_subplot(313)
      # self.multifig.add_subplot(212) # for two panels
    else: 
      plt.figure(fign, figsize=self.figuresize)
      plt.clf()
    # plt.axes().set_aspect(aspect=self.aspect_ratio/plt.axes().get_data_ratio())
    # plt.axes().set_aspect(aspect=self.aspect_ratio)
    if ndiff==0: 
      plt.ylim([0, 300])
      #puma plt.ylim([0, 200])
    elif ndiff==1: 
      plt.ylim([-150, 150])
    else: 
      print("Warning: invalid ndiff argument in plotPrice.")
    # plt.ylim([-100, 300])
    # Reported price: 
    # n_grains = [11] # for file including multiple grains
    n_grain = 0 
    if self.t_per_year==1:

      # Crop extra months at start and end of timeseries (so it starts in January and ends in December):
      if grainPrices_month1 != 1:
        print('first')
        grainPrices_data = grainPrices_data[(12-grainPrices_month1+1):]
        grainPrices_year = grainPrices_year[(12-grainPrices_month1+1):]
        print(grainPrices_year)

      if len(grainPrices_data)%12 != 0:
        print('second')
        grainPrices_data_fullyears = grainPrices_data[:-(len(grainPrices_data)%12), :]
        grainPrices_year_fullyears = grainPrices_year[:-(len(grainPrices_data)%12)]
      else:
        print('third')
        grainPrices_data_fullyears = grainPrices_data
        grainPrices_year_fullyears = grainPrices_year


      # # Annual average of price:
      # grainPrices_data = np.array([np.mean(np.squeeze(grainPrices_data_fullyears[np.where(np.array(grainPrices_year_fullyears)==yr),:]), axis=0) for yr in sorted(set(grainPrices_year_fullyears))])
      # Average over *crop year*:
      grainPrices_data_crop = grainPrices_data_fullyears[cropYear_month1-1:-(12-(cropYear_month1-1)),:]
      grainPrices_data_reshaped = np.reshape(grainPrices_data_crop, (-1,12,np.shape(grainPrices_data_fullyears)[1]))
      grainPrices_data = np.mean(grainPrices_data_reshaped, axis=1)

    elif self.t_per_year not in [1, 12]:
      print('Time step is neither yearly nor monthly - dont know what to do with observed price timeseries. Trying monthly, but do not trust it!')
    starting_month = (self.start_year - int(grainPrices_year[0]))*self.t_per_year 
    xaxis = np.arange(self.repeatFirstColumnNTimes*self.t_per_year+1, self.repeatFirstColumnNTimes*self.t_per_year + len(grainPrices_data[starting_month:,n_grain])+1)
    xaxis_tick = np.arange(self.repeatFirstColumnNTimes*self.t_per_year+1, self.repeatFirstColumnNTimes*self.t_per_year + len(grainPrices_data[starting_month:,n_grain])+1, self.t_per_year*4.0)
    plt.plot(xaxis[ndiff:], np.diff(grainPrices_data[starting_month:,n_grain],n=ndiff), '-k', linewidth=1.5, label='reported')

    # plt.xticks(np.arange(1, len(grainPrices_data[starting_month:,n_grain])+1, 24.0), grainPrices_year[starting_month::24], rotation=60)
    #puma plt.xticks(xaxis_tick, grainPrices_year[starting_month*(12//self.t_per_year)::48], rotation=60)
    if xlabels==False:
      plt.gca().axes.xaxis.set_ticklabels([])
    if deflate == None:
      plt.ylabel('price (US\$ nominal)', size=self.axlsize)
    elif deflate == 'CPI': 
      plt.ylabel('price (US\$/mt, CPI-deflated)', size=self.axlsize)
    else: 
      print('Error: deflate flag unknown.')
    # Model price:
    try: 
      plt.plot(taxis[ndiff:], np.diff(Px_Fix, n=ndiff), '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')  
      # plt.plot(taxis[ndiff:], np.diff(Px_Fix, n=ndiff), '--', linewidth=2., color=self.modelcolorFix, label='fixed storage')  
      # plt.plot(taxis[ndiff:], np.diff(Px_Fix, n=ndiff), '--', linewidth=2., color=self.modelcolorFix, label='averaged consumption')  
      # plt.plot(taxis, Px_Fix, '--', linewidth=2., color=self.modelcolorFlex, label='no policy')  
    except NameError: 
      pass
    if self.endogFinalDemand: 
      label1 = 'FlexCons model'
      # label1 = 'policy'
      color1 = self.modelcolorFlex
    else: 
      label1 = 'FixCons model'
      color1 = self.modelcolorFix
    plt.plot(taxis[ndiff:], np.diff(Px, n=ndiff), '-', linewidth=2., color=color1, label=label1)  
    # plt.plot(taxis[ndiff:], np.diff(Px, n=ndiff), '-', linewidth=2., color=color1, label='flexible storage')  
    # plt.plot(taxis[ndiff:], np.diff(Px, n=ndiff), '-', linewidth=2., color=color1, label='annual consumption')  
    # # Difference model-observation: 
    # plt.plot(xaxis, Px[-39:]-grainPrices_data[starting_month:,n_grain], '--', linewidth=3., color=self.modelcolor)  
    # plt.plot([taxis[0], taxis[-1]], [0, 0] , '-k')
    # plt.legend(np.append(['reported'], ['model']), loc='best')
    # plt.legend(loc='best')
    plt.grid(True)
    if ndiff==0: 
      plt.text(self.xlims[0]+3, 60, 'Price', size=self.axlsize)
    elif ndiff==1: 
      plt.text(self.xlims[0]+3, -90, 'Price difference', size=self.axlsize)
    # plt.text(self.xlims[0]+3, 160, 'Price', size=self.axlsize)
    plt.tick_params(axis='both', which='major', labelsize=self.ticklsize)
    # Crop spin-up part
    # plt.xlim([self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*self.x_axis_extension])
    #puma plt.xlim(self.xlims)
    plt.xlim([0,350])
    if yaxis_right==True:
      plt.gca().yaxis.tick_right()
      plt.gca().yaxis.set_label_position("right")      
    # Mark "out-of-sample" period
    ymin, ymax = plt.gca().get_ylim()
    plt.gca().add_patch(patches.Rectangle((taxis[-4]+0.5, ymin), 3.5, ymax-ymin, facecolor=self.SpecialColor3, edgecolor='none'))
    # plt.ion()
    # plt.show()
    if ~self.subplots:
      plt.tight_layout()
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
#      eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)
    # Correlation
    priceModel_orig = Px[self.repeatFirstColumnNTimes:]
    # print np.shape(priceModel_orig)
    priceModel_trend = runmean(priceModel_orig,5)
    priceModel_detrended = priceModel_orig-priceModel_trend
    priceModel = priceModel_detrended
    # priceModel = priceModel_orig
    # priceObs_orig = grainPrices_data[starting_month:starting_month+39,n_grain]
    priceObs_orig = grainPrices_data[starting_month:starting_month+self.t_end-self.repeatFirstColumnNTimes,n_grain]
    # print np.shape(priceObs_orig)
    # priceObs_orig = grainPrices_data[:,n_grain]
    priceObs_trend = runmean(priceObs_orig,5)
    priceObs_detrended = priceObs_orig-priceObs_trend
    priceObs = priceObs_detrended
    # priceObs = priceObs_orig
    try: 
      print(('Price correlation: ', np.corrcoef(priceModel_orig, priceObs_orig)[0,1]))
    except ValueError: 
      print(('Warning: Observed price series length ', len(priceObs_orig), ' seems to be shorter than simulated one ', len(priceModel_orig), '. Trying to adjust simulated one for correlation analysis.'))
      print(('Price correlation: ', np.corrcoef(priceModel_orig[:(len(priceObs_orig)-len(priceModel_orig))], priceObs_orig)[0,1]))
    print(('Price auto-correlation (model) lag 1: ', np.corrcoef(priceModel[:-1], priceModel[1:])[0,1]))
    print(('Price auto-correlation (model) lag 2: ', np.corrcoef(priceModel[:-2], priceModel[2:])[0,1]))
    print(('Price auto-correlation (model) lag 3: ', np.corrcoef(priceModel[:-3], priceModel[3:])[0,1]))
    print(('Price skewness (model): ', scipy.stats.skew(priceModel)))
    print(('Price kurtosis (model): ', scipy.stats.kurtosis(priceModel)))
    print(('Price auto-correlation (obs) lag 1: ', np.corrcoef(priceObs[:-1], priceObs[1:])[0,1]))
    print(('Price auto-correlation (obs) lag 2: ', np.corrcoef(priceObs[:-2], priceObs[2:])[0,1]))
    print(('Price auto-correlation (obs) lag 3: ', np.corrcoef(priceObs[:-3], priceObs[3:])[0,1]))
    print(('Price skewness (obs): ', scipy.stats.skew(priceObs)))
    print(('Price kurtosis (obs): ', scipy.stats.kurtosis(priceObs)))
    randomModel_autoCorr = []
    # priceModel_detrended_mean = np.mean(priceModel_detrended)
    # priceModel_detrended_std = np.std(priceModel_detrended)
    # randomModel = np.random.normal(priceModel_detrended_mean, priceModel_detrended_std, (100000, len(priceModel_detrended)))
    # for row in randomModel: 
      # randomModel_autoCorr.append(np.corrcoef(row[:-1], row[1:])[0,1])
    # priceDiffSquaresRoot = np.sum(np.subtract(priceModel, priceObs)**2)**0.5
    # print 'Price root of sum of squares: ',  priceDiffSquaresRoot
    return grainPrices_data, randomModel_autoCorr

  def plotPriceDiff(self, fign, taxis, Px, grainPrices_data, grainPrices_year, grainPrices_legends, figName, figSave):
    plt.figure(fign)
    plt.clf()
    plt.plot(taxis[1:], np.diff(Px), '-k', linewidth=2.)  
    plt.plot([taxis[0], taxis[-1]+4], [0, 0], '-k', linewidth=1.)  
    plt.ylim([-100, 100])
    n_grains = [11]
    cols = ['g', 'b']
    if self.t_per_year==1:
      # Annual average of price: 
      grainPrices_data = np.array([np.mean(np.squeeze(grainPrices_data[np.where(np.array(grainPrices_year)==yr),:]), axis=0) for yr in  sorted(set(grainPrices_year))])
    elif self.t_per_year not in [1, 12]:
      print('Time step is neither yearly nor monthly - dont know what to do with observed price timeseries. Trying monthly, but do not trust it!')
    starting_month = (self.start_year - 1960)*self.t_per_year 
    for i, n_grain in enumerate(n_grains):
      xaxis = np.arange(self.repeatFirstColumnNTimes*self.t_per_year+1, self.repeatFirstColumnNTimes*self.t_per_year + len(grainPrices_data[starting_month:,n_grain])+1)
      xaxis_tick = np.arange(self.repeatFirstColumnNTimes*self.t_per_year+1, self.repeatFirstColumnNTimes*self.t_per_year + len(grainPrices_data[starting_month:,n_grain]), self.t_per_year*4.0)
      plt.plot(xaxis[1:], np.diff(grainPrices_data[starting_month:,n_grain]), '--', color=cols[i])
      plt.xticks(xaxis_tick, grainPrices_year[starting_month*(12/self.t_per_year)::48], rotation=60)
    plt.xlabel('year')
    plt.ylabel('price change')
    plt.legend(np.append(['model'], [grainPrices_legends[n_grain].replace('$', '\$') for n_grain in n_grains]), loc='best')
    plt.xlim([self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*3])
    plt.show()
    
  def plotPrice_NoObs(self, fign, taxis, Px_load, prodCountry_years_var, figName, figSave, xlabels=True, yaxis_right=False, ndiff=0):
    """
    Plot simulated price only, without observations. 
    Optional arguments: 
      deflate: Correct for inflation. 
      cropYear_month1: Specify 1st calendar-month of crop year. If 1, crop year is identical to calendar year. 
      ndiff: If 0 (default), plot annual price; if 1, plot price difference to previous year.
    """
    if np.shape(Px_load)[0]==2: 
      Px = Px_load[0]
      Px_Fix = Px_load[1]
    else: 
      Px = Px_load
    if self.subplots:
      self.multifig = plt.figure(6, figsize=(8,15))
      # self.multifig = plt.figure(6, figsize=(8,10)) # for two panels
      self.multifig.subplots_adjust(left=0.2, hspace=0.)
      plt.clf()
      # self.multifig.add_subplot(311)
      self.multifig.add_subplot(313)
      # self.multifig.add_subplot(212) # for two panels
    else: 
      plt.figure(fign, figsize=self.figuresize)
      plt.clf()
    # plt.axes().set_aspect(aspect=self.aspect_ratio/plt.axes().get_data_ratio())
    # plt.axes().set_aspect(aspect=self.aspect_ratio)
    if ndiff==0: 
      plt.ylim([0, 650])
      # plt.ylim([0, 200])
    elif ndiff==1: 
      plt.ylim([-150, 150])
    else: 
      print("Warning: invalid ndiff argument in plotPrice.")
    # plt.ylim([-100, 300])
    # Model price:
    try: 
      plt.plot(taxis[ndiff:], np.diff(Px_Fix, n=ndiff), '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')  
      # plt.plot(taxis[ndiff:], np.diff(Px_Fix, n=ndiff), '--', linewidth=2., color=self.modelcolorFix, label='fixed storage')  
      # plt.plot(taxis[ndiff:], np.diff(Px_Fix, n=ndiff), '--', linewidth=2., color=self.modelcolorFix, label='averaged consumption')  
      # plt.plot(taxis, Px_Fix, '--', linewidth=2., color=self.modelcolorFlex, label='no policy')  
    except NameError: 
      pass
    if self.endogFinalDemand: 
      label1 = 'FlexCons model'
      # label1 = 'policy'
      color1 = self.modelcolorFlex
    else: 
      label1 = 'FixCons model'
      color1 = self.modelcolorFix
    plt.plot(taxis[ndiff:], np.diff(Px, n=ndiff), '-', linewidth=2., color=color1, label=label1)  
    # plt.plot(taxis[ndiff:], np.diff(Px, n=ndiff), '-', linewidth=2., color=color1, label='flexible storage')  
    # plt.plot(taxis[ndiff:], np.diff(Px, n=ndiff), '-', linewidth=2., color=color1, label='annual consumption')  
    # # Difference model-observation: 
    # plt.plot(xaxis, Px[-39:]-grainPrices_data[starting_month:,n_grain], '--', linewidth=3., color=self.modelcolor)  
    # plt.plot([taxis[0], taxis[-1]], [0, 0] , '-k')
    # plt.legend(np.append(['reported'], ['model']), loc='best')
    # plt.legend(loc='best')
    plt.grid(True)
    if ndiff==0: 
      plt.text(self.xlims[0]+3, 60, 'Price', size=self.axlsize)
    elif ndiff==1: 
      plt.text(self.xlims[0]+3, -90, 'Price difference', size=self.axlsize)
    # plt.text(self.xlims[0]+3, 160, 'Price', size=self.axlsize)
    xaxis_tick = np.arange(self.t_start+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end+1, self.t_per_year*4.)
    plt.xticks(xaxis_tick, np.arange(int(prodCountry_years_var[0]), int(prodCountry_years_var[-1])+1, 4), rotation=60)
    plt.tick_params(axis='both', which='major', labelsize=self.ticklsize)
    # Crop spin-up part
    # plt.xlim([self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*self.x_axis_extension])
    plt.xlim(self.xlims)
    if yaxis_right==True:
      plt.gca().yaxis.tick_right()
      plt.gca().yaxis.set_label_position("right")      
    # Mark "out-of-sample" period
    ymin, ymax = plt.gca().get_ylim()
    plt.gca().add_patch(patches.Rectangle((taxis[-4]+0.5, ymin), 3.5, ymax-ymin, facecolor=self.SpecialColor3, edgecolor='none'))
    # plt.ion()
    # plt.show()
    if ~self.subplots:
      plt.tight_layout()
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
      #eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)

  def plotPriceDiff(self, fign, taxis, Px, grainPrices_data, grainPrices_year, grainPrices_legends, figName, figSave):
    plt.figure(fign)
    plt.clf()
    plt.plot(taxis[1:], np.diff(Px), '-k', linewidth=2.)  
    plt.plot([taxis[0], taxis[-1]+4], [0, 0], '-k', linewidth=1.)  
    plt.ylim([-100, 100])
    n_grains = [11]
    cols = ['g', 'b']
    if self.t_per_year==1:
      # Annual average of price: 
      grainPrices_data = np.array([np.mean(np.squeeze(grainPrices_data[np.where(np.array(grainPrices_year)==yr),:]), axis=0) for yr in  sorted(set(grainPrices_year))])
    elif self.t_per_year not in [1, 12]:
      print('Time step is neither yearly nor monthly - dont know what to do with observed price timeseries. Trying monthly, but do not trust it!')
    starting_month = (self.start_year - 1960)*self.t_per_year 
    for i, n_grain in enumerate(n_grains):
      xaxis = np.arange(self.repeatFirstColumnNTimes*self.t_per_year+1, self.repeatFirstColumnNTimes*self.t_per_year + len(grainPrices_data[starting_month:,n_grain])+1)
      xaxis_tick = np.arange(self.repeatFirstColumnNTimes*self.t_per_year+1, self.repeatFirstColumnNTimes*self.t_per_year + len(grainPrices_data[starting_month:,n_grain]), self.t_per_year*4.0)
      plt.plot(xaxis[1:], np.diff(grainPrices_data[starting_month:,n_grain]), '--', color=cols[i])
      plt.xticks(xaxis_tick, grainPrices_year[starting_month*(12/self.t_per_year)::48], rotation=60)
    plt.xlabel('year')
    plt.ylabel('price change')
    plt.legend(np.append(['model'], [grainPrices_legends[n_grain].replace('$', '\$') for n_grain in n_grains]), loc='best')
    plt.xlim([self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*3])
    plt.show()
    

  def plotConsumption(self, fign, taxis, taxis_annual, q_harvest, Q_out_load, q_out_min, q_out_min_annual, consCountry_quantity_var, prodCountry_years_var, figName, figSave, importStrat, q_storage_max_cons, anomalies=False, surplus=False, storageCapacity=False, xlabels=False, yaxis_right=False):
    if np.shape(Q_out_load)[0]==2: 
      Q_out = Q_out_load[0]
      Q_out_Fix = Q_out_load[1]
    else: 
      Q_out = Q_out_load
    if self.subplots: 
      # ax1 = self.multifig.add_subplot(313)
      ax1 = self.multifig.add_subplot(312)
    else: 
      fig = plt.figure(fign, figsize=self.figuresize)
      ax1 = fig.add_subplot(1,1,1)
      plt.clf()
    # plt.axes().set_aspect(aspect=self.aspect_ratio/plt.axes().get_data_ratio())
    simlen = self.t_end//self.t_per_year-self.repeatFirstColumnNTimes
    # Reported: 
    if self.consumption_varying == 1: 
      if anomalies==False:
        axis_start = np.shape(consCountry_quantity_var)[1]
        data_reported = np.divide(np.sum(consCountry_quantity_var, axis=0),self.t_per_year)*self.qty_multiplier
      elif anomalies==True: 
        axis_start = simlen
        data_reported = np.divide(np.sum(consCountry_quantity_var[:,-simlen:], axis=0)-np.sum(q_out_min_annual[:,-simlen:], axis=0),self.t_per_year)*self.qty_multiplier
      plt.plot(taxis_annual[-axis_start:], data_reported, '-k', linewidth=2., label='reported')
    # Model: 
    if self.endogFinalDemand: 
      label1 = 'FlexCons model'
      # label1 = 'Equation 7 with \nreported prices'
      color1 = self.modelcolorFlex
    else: 
      label1 = 'FixCons model'
      color1 = self.modelcolorFix
    if anomalies==False:
      try: 
        data_model_Fix = np.sum(Q_out_Fix,axis=1)*self.qty_multiplier
        # plt.plot(taxis, data_model_Fix, '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')
      except NameError: 
        pass
      data_model = np.sum(Q_out,axis=1)*self.qty_multiplier
      # plt.plot(taxis, np.sum(q_out_min, axis=1)*self.qty_multiplier,'-b')
    elif anomalies==True: 
      try: 
        data_model_Fix = (np.sum(Q_out_Fix,axis=1)-np.sum(q_out_min, axis=1))*self.qty_multiplier
        # plt.plot(taxis, data_model_Fix, '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')
      except NameError: 
        pass
      data_model = (np.sum(Q_out,axis=1)-np.sum(q_out_min, axis=1))*self.qty_multiplier
      # plt.plot(taxis, data_model, '-', linewidth=2., color=color1, label=label1)
      try: 
        # print 'Consumption correlation: ', np.corrcoef(data_model[self.repeatFirstColumnNTimes*self.t_per_year:], data_reported)[0,1]
        print(('Consumption correlation: ', np.corrcoef(data_model, data_reported[-len(data_model):])[0,1]))
      except ValueError: 
        print('Cannot compute consumption correlation coefficient...')
    try: 
      plt.plot(taxis, data_model_Fix, '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')
      # plt.plot(taxis, data_model_Fix, '--', linewidth=2., color=self.modelcolorFlex, label='no policy')
    except NameError: 
      pass
    plt.plot(taxis, data_model, '-', linewidth=2., color=color1, label=label1)
    # plt.plot(taxis, data_model, '-', linewidth=2., color=color1, label='policy')
    if surplus==True:
      # plt.plot(taxis_annual, np.cumsum(np.divide(np.sum(q_harvest, axis=1),self.t_per_year)-np.divide(np.sum(q_out_min, axis=1),self.t_per_year))*self.qty_multiplier, '--r', label='cumul. surplus')
      plt.plot(taxis_annual[self.repeatFirstColumnNTimes:], np.cumsum(np.divide(np.sum(q_harvest[self.repeatFirstColumnNTimes:], axis=1),self.t_per_year)-np.divide(np.sum(consCountry_quantity_var, axis=0),self.t_per_year))*self.qty_multiplier, '--k', label='cumul. surplus')
    # plt.legend(loc='best')
    plt.plot([taxis[0], taxis[-1]], [0, 0], '-k')
    plt.grid(True)
    if anomalies==False:
      plt.ylim([0.e8*self.qty_multiplier, 10.e8*self.qty_multiplier])
    elif anomalies==True: 
      plt.ylim([-0.5e8*self.qty_multiplier, .5e8*self.qty_multiplier])
    # plt.xticks(np.arange(self.t_start-1, self.t_end, 24.))
    xaxis_tick = np.arange(self.t_start+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end+1, self.t_per_year*4.)
    plt.xticks(xaxis_tick, np.arange(int(prodCountry_years_var[0]), int(prodCountry_years_var[-1])+1, 4), rotation=60)
    plt.ylabel('quantity (mmt)', size=self.axlsize)
    if yaxis_right==True:
      plt.gca().yaxis.tick_right()
      plt.gca().yaxis.set_label_position("right")      
    # plt.xlabel('month')
    # plt.xlabel('year', size=self.axlsize)
    if anomalies==False:
      plt.text(self.xlims[0]+3, 2.1e8*self.qty_multiplier, 'Consumption', size=self.axlsize)
      # plt.text(self.repeatFirstColumnNTimes+7, 3.6e8*self.qty_multiplier, 'Consumption', size=self.axlsize)
      # plt.text(self.repeatFirstColumnNTimes+5, 7.1e8*self.qty_multiplier, '$I_{max,c}$', size=self.axlsize)
      # plt.text(self.repeatFirstColumnNTimes+22, 2.4e8*self.qty_multiplier, 'Cum. surplus', size=self.axlsize)
    elif anomalies==True:
      plt.text(self.xlims[0]+3, -.3e8*self.qty_multiplier, 'Consumption anomalies', size=self.axlsize)
    plt.tick_params(axis='both', which='major', labelsize=self.ticklsize)
    # Crop spin-up part
    # plt.xlim([self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*self.x_axis_extension])
    plt.xlim(self.xlims)
    if storageCapacity==True:
      ### !!! TO DO: In the line below, make sure that the order of countries as read from the importStrat dict matches the order of countries in q_storage_max_cons. !!! 
      import_multipliers = [list(importStrat[country].values()) for country in list(importStrat.keys())]
      # Line...
      plt.plot(taxis, np.multiply(np.squeeze(np.sum(np.multiply(q_storage_max_cons, np.transpose(import_multipliers)), axis=1)), self.qty_multiplier), '--', color=[0.6]*3, linewidth=2., label='storage capacity')
      # # ...or Bars
      # ax2 = ax1.twinx()
      # barcolor1 = 'indianred'
      # barcolor2 = np.divide([103,169,207],256.)
      # ax2.bar(taxis[-15:]-0.4, np.subtract(import_multipliers[-15:], 1.), color=barcolor2)
      # ax2.bar([taxis[-7]-0.3, taxis[-4]-0.3], np.subtract([0.97, 0.95], 1.), width=[0.6, 0.6], color=barcolor1)
      # ax2.set_ylim([-0.52, 0.13])
      # # Move axis into plot area
      # ax2.yaxis.tick_left()
      # ax2.yaxis.set_label_position("left")      
      # ax2.spines['left'].set_position(('data', 324))
      # ax2.spines['left'].set_smart_bounds(True)
      # ax2.text(self.repeatFirstColumnNTimes+26, 0.03, 'Trade \npolicies', size=self.axlsize-4)
      # ax2.text(self.repeatFirstColumnNTimes+33, 0.04, 'Import', size=self.axlsize-4, color=barcolor2) #, fontweight='bold')
      # ax2.text(self.repeatFirstColumnNTimes+33.9, -0.08, 'Export', size=self.axlsize-4, color=barcolor1)
      # # ax2.set_xlim([self.repeatFirstColumnNTimes*self.t_per_year, self.t_end + self.t_per_year*self.x_axis_extension])
      # ax2.set_xlim(self.xlims)
    if xlabels==False:
      ax1.axes.xaxis.set_ticklabels([])
    if self.subplots:
      # self.multifig.tight_layout()
      pass
    else: 
      plt.tight_layout()
    # Mark "out-of-sample" period
    ymin, ymax = ax1.get_ylim()
    ax1.add_patch(patches.Rectangle((taxis[-4]+0.5, ymin), 3.5, ymax-ymin, facecolor=self.SpecialColor3, edgecolor='none'))
    # Correlation
    from runningmean import runmean
    harvestObs_orig = q_harvest[self.repeatFirstColumnNTimes-1:self.repeatFirstColumnNTimes+39,0]
    harvestObs_trend = runmean(harvestObs_orig,5)
    harvestObs_detrended = harvestObs_orig-harvestObs_trend
    harvestObs = harvestObs_detrended
    print((np.shape(harvestObs)))
    print(('Production auto-correlation (obs) lag 1: ', np.corrcoef(harvestObs[:-1], harvestObs[1:])[0,1]))
    print(('Production auto-correlation (obs) lag 2: ', np.corrcoef(harvestObs[:-2], harvestObs[2:])[0,1]))
    print(('Production auto-correlation (obs) lag 3: ', np.corrcoef(harvestObs[:-3], harvestObs[3:])[0,1]))
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
      eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)

  def plotStorage(self, fign, taxis, taxis_annual, Q_storage_prod_load, Q_storage_cons_load, prodCountry_years_var, grainStorage_file, importStrat, exportRestr, figName, figSave, ProdConsSeparate=False, storageCapacity=False, xlabels=False, yaxis_right=False):
    if np.shape(Q_storage_prod_load)[0]==2 and np.shape(Q_storage_cons_load)[0]==2: 
      Q_storage_prod = Q_storage_prod_load[0]
      Q_storage_prod_Fix = Q_storage_prod_load[1]
      Q_storage_cons = Q_storage_cons_load[0]
      Q_storage_cons_Fix = Q_storage_cons_load[1]
    elif np.shape(Q_storage_prod_load)[0]!=np.shape(Q_storage_cons_load)[0]==1: 
      print('Error: Inconsistent numbers of Q_storage_prod and Q_storage_cons input timeseries.') 
    else: 
      Q_storage_prod = Q_storage_prod_load
      Q_storage_cons = Q_storage_cons_load
    if self.subplots: 
      # ax1 = self.multifig.add_subplot(312)
      ax1 = self.multifig.add_subplot(311)
      # ax1 = self.multifig.add_subplot(211) # for two panels
    else: 
      fig = plt.figure(fign, figsize=self.figuresize)
      ax1 = fig.add_subplot(1,1,1)
      # ax1 = plt.gca()
      plt.clf()
    # plt.axes().set_aspect(aspect=self.aspect_ratio/plt.axes().get_data_ratio())
    # Reported USDA World beginning stocks: 
    with open(grainStorage_file, 'r') as pf:
      for il, line in enumerate(pf):
        if il == 0:
          USDA_agYears = line.rstrip('\n').split(';')[3:]
        if il == 1:
          USDA_begStorStr = line.rstrip('\n').split(';')[3:]
    # Associate beginning stock to (September of) the first calendar year of the agricultural year (e.g. 2010 for 2010/2011)
    #puma# USDA_years = np.array(list(map(int, [ay.split('/')[0] for ay in USDA_agYears])))
    #puma# USDA_begStor = np.array(list(map(float, [bs.replace(",", "") for bs in USDA_begStorStr])))
    # xaxis_USDA = np.arange(self.t_start+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end-1, self.t_per_year) + 9.
    #puma# xaxis_USDA = np.arange(self.t_start-1+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end+1, self.t_per_year) 
    #puma# start_USDA = int(np.squeeze(np.where(USDA_years==self.start_year)))
    #puma# end_USDA = int(np.squeeze(np.where(USDA_years==(self.t_end/self.t_per_year + self.start_year - self.repeatFirstColumnNTimes))))
    #puma# plt.plot(xaxis_USDA, np.multiply(USDA_begStor[start_USDA:end_USDA + 1], 1e3)*self.qty_multiplier, '-k', linewidth=2., label='reported')
    # Model: 
    if ProdConsSeparate:
      print('NOTE: Plotting prod and cons storage separately; plotting multiple model versions is not yet implemented for this option.') 
      plt.plot(taxis, np.sum(Q_storage_prod, axis=1)*self.qty_multiplier, '-k', label='model (producers)')
      plt.plot(taxis, np.sum(Q_storage_cons, axis=1)*self.qty_multiplier, '-b', label='model (consumers)')
    if self.endogFinalDemand: 
      label1 = 'FlexCons model'
      color1 = self.modelcolorFlex
    else: 
      label1 = 'FixCons model'
      color1 = self.modelcolorFix
    try: 
      plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')
      # plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '--', linewidth=2., color=self.modelcolorFix, label='fixed storage')
      # plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '--', linewidth=2., color=self.modelcolorFix, label='averaged consumption')
      # plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '--', linewidth=2., color=self.modelcolorFlex, label='no policy')
    except NameError: 
      pass
    plt.plot(taxis, np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))*self.qty_multiplier, '-', linewidth=2., color=color1, label=label1)
    # plt.plot(taxis, np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))*self.qty_multiplier, '-', linewidth=2., color=color1, label='annual consumption')
    # plt.plot(taxis, np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))*self.qty_multiplier, '-', linewidth=2., color=color1, label='policy')
    plt.grid(True)
    # plt.ylim([0, 0.35e9*self.qty_multiplier])
    # plt.ylim([0, 0.4e9*self.qty_multiplier])
    plt.ylim([0, 0.26e9*self.qty_multiplier])
    # plt.xticks(np.arange(self.t_start-1, self.t_end, 24.))
    xaxis_tick = np.arange(self.t_start+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end+1, self.t_per_year*4.)
    plt.xticks(xaxis_tick, np.arange(int(prodCountry_years_var[0]), int(prodCountry_years_var[-1])+1, 4), rotation=60)
    plt.ylabel('quantity (mmt)', size=self.axlsize)
    if yaxis_right==True:
      plt.gca().yaxis.tick_right()
      plt.gca().yaxis.set_label_position("right")      
    # plt.xlabel('month')
    # plt.xlabel('year')
    # plt.legend(loc=4)
    plt.text(self.xlims[0]+3, .6e8*self.qty_multiplier, 'Ending storage levels', size=self.axlsize)
    # plt.text(self.xlims[0]+3, 1.2e8*self.qty_multiplier, 'Ending storage levels', size=self.axlsize)
    plt.tick_params(axis='both', which='major', labelsize=self.ticklsize)
    # print 'Storage correlation: ', np.corrcoef(np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))[self.repeatFirstColumnNTimes:-1], np.multiply(USDA_begStor[start_USDA+1:end_USDA-1], 1e3))[0,1]
    # Crop spin-up part
    #puma plt.xlim(self.xlims)
    plt.xlim([0,350])
    
    # Mark "out-of-sample" period
    ymin, ymax = ax1.get_ylim()
    ax1.add_patch(patches.Rectangle((taxis[-4]+0.5, ymin), 3.5, ymax-ymin, facecolor=self.SpecialColor3, edgecolor='none'))
    if storageCapacity==True:
      # Mark droughts
      ax1.plot([2010-1975+1+self.repeatFirstColumnNTimes*self.t_per_year], [0.21e9*self.qty_multiplier], 'v', color=self.SpecialColor1, markeredgecolor='none', markersize=10, zorder=1)
      ax1.text(self.repeatFirstColumnNTimes+31, 2.33e8*self.qty_multiplier, 'Droughts:', size=self.axlsize-3, color=self.SpecialColor1, horizontalalignment='right')
      ax1.text(self.repeatFirstColumnNTimes+33., 2.2e8*self.qty_multiplier, 'UKR\nAUS\nIND', size=self.axlsize-4, color=self.SpecialColor1, horizontalalignment='center')
      ax1.text(self.repeatFirstColumnNTimes+36, 2.2e8*self.qty_multiplier, 'RUS\nUSA\nCHN', size=self.axlsize-4, color=self.SpecialColor1, horizontalalignment='center')
      ax1.plot([2007-1975+1+self.repeatFirstColumnNTimes*self.t_per_year], [0.21e9*self.qty_multiplier], 'v', color=self.SpecialColor1, markeredgecolor='none', markersize=10, zorder=1)
      # Plot trade policy parameters
      import_multipliers = list(importStrat['World'].values())
      ax2 = ax1.twinx()
      # Line...
      # ax2.plot(taxis, np.multiply(np.multiply(np.squeeze(q_storage_max_cons), import_multipliers), self.qty_multiplier), '--', color=[0.6]*3, linewidth=2., label='storage capacity')
      # ax2.set_ylim([0.e8*self.qty_multiplier, 10.e8*self.qty_multiplier])
      # ...or Bars
      barcolor1 = 'indianred'
      barcolor2 = np.divide([103,169,207],256.)
      endyear = self.t_end/self.t_per_year + self.start_year - self.repeatFirstColumnNTimes
      ax2.bar(taxis[1999-endyear:]-0.4, np.subtract(import_multipliers[1999-endyear:], 1.), color=barcolor2)
      # ax2.bar([taxis[2007-endyear]-0.3, taxis[2010-endyear]-0.3], np.subtract([0.97, 0.95], 1.), width=[0.6, 0.6], color=barcolor1)
      try: 
        exportRestrYears = list(exportRestr['World'].keys())
      except KeyError: 
        print('Error: Cannot plot export restrictions for countries other than World. Adjust plot routine!') 
      for yr in exportRestrYears: 
          ax2.bar([taxis[yr-endyear]-0.3], np.subtract(exportRestr['World'][yr], 1.), width=0.6, color=barcolor1)
      ax2.set_ylim([-0.15, 0.5])
      # Move axis into plot area
      ax2.yaxis.tick_left()
      ax2.yaxis.set_label_position("left")      
      ax2.spines['left'].set_position(('data', self.repeatFirstColumnNTimes+24))
      ax2.spines['left'].set_smart_bounds(True)
      # ax2.text(self.repeatFirstColumnNTimes+17, 0.0, 'Trade \npolicies', size=self.axlsize-2, horizontalalignment='left')
      ax2.text(self.xlims[0]+10, 0.05, 'Trade policies', size=self.axlsize-2, horizontalalignment='left')
      ax2.text(self.repeatFirstColumnNTimes+35, 0.06, 'Import', size=self.axlsize-4, color=barcolor2) #, fontweight='bold')
      ax2.text(self.repeatFirstColumnNTimes+33.9, -0.06, 'Export', size=self.axlsize-4, color=barcolor1)
      ax2.set_xlim(self.xlims)
    if xlabels==False:
      ax1.axes.xaxis.set_ticklabels([])
    if self.subplots==False:
      plt.tight_layout()
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
#      eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)

  def plotStorage_NoObs(self, fign, taxis, taxis_annual, Q_storage_prod_load, Q_storage_cons_load, prodCountry_years_var, importStrat, exportRestr, figName, figSave, ProdConsSeparate=False, storageCapacity=False, xlabels=False, yaxis_right=False):
    if np.shape(Q_storage_prod_load)[0]==2 and np.shape(Q_storage_cons_load)[0]==2: 
      Q_storage_prod = Q_storage_prod_load[0]
      Q_storage_prod_Fix = Q_storage_prod_load[1]
      Q_storage_cons = Q_storage_cons_load[0]
      Q_storage_cons_Fix = Q_storage_cons_load[1]
    elif np.shape(Q_storage_prod_load)[0]!=np.shape(Q_storage_cons_load)[0]==1: 
      print('Error: Inconsistent numbers of Q_storage_prod and Q_storage_cons input timeseries.') 
    else: 
      Q_storage_prod = Q_storage_prod_load
      Q_storage_cons = Q_storage_cons_load
    if self.subplots: 
      # ax1 = self.multifig.add_subplot(312)
      ax1 = self.multifig.add_subplot(311)
      # ax1 = self.multifig.add_subplot(211) # for two panels
    else: 
      fig = plt.figure(fign, figsize=self.figuresize)
      ax1 = fig.add_subplot(1,1,1)
      # ax1 = plt.gca()
      plt.clf()
    # plt.axes().set_aspect(aspect=self.aspect_ratio/plt.axes().get_data_ratio())
    # Model: 
    if ProdConsSeparate:
      print('NOTE: Plotting prod and cons storage separately; plotting multiple model versions is not yet implemented for this option.') 
      plt.plot(taxis, np.sum(Q_storage_prod, axis=1)*self.qty_multiplier, '-k', label='model (producers)')
      plt.plot(taxis, np.sum(Q_storage_cons, axis=1)*self.qty_multiplier, '-b', label='model (consumers)')
    if self.endogFinalDemand: 
      label1 = 'FlexCons model'
      color1 = self.modelcolorFlex
    else: 
      label1 = 'FixCons model'
      color1 = self.modelcolorFix
    try: 
      plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '-', linewidth=2., color=self.modelcolorFix, label='FixCons model')
      # plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '--', linewidth=2., color=self.modelcolorFix, label='fixed storage')
      # plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '--', linewidth=2., color=self.modelcolorFix, label='averaged consumption')
      # plt.plot(taxis, np.add(np.sum(Q_storage_prod_Fix, axis=1), np.sum(Q_storage_cons_Fix, axis=1))*self.qty_multiplier, '--', linewidth=2., color=self.modelcolorFlex, label='no policy')
    except NameError: 
      pass
    plt.plot(taxis, np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))*self.qty_multiplier, '-', linewidth=2., color=color1, label=label1)
    # plt.plot(taxis, np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))*self.qty_multiplier, '-', linewidth=2., color=color1, label='annual consumption')
    # plt.plot(taxis, np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))*self.qty_multiplier, '-', linewidth=2., color=color1, label='policy')
    plt.grid(True)
    # plt.ylim([0, 0.35e9*self.qty_multiplier])
    # plt.ylim([0, 0.4e9*self.qty_multiplier])
    plt.ylim([0, 0.26e9*self.qty_multiplier])
    # plt.xticks(np.arange(self.t_start-1, self.t_end, 24.))
    xaxis_tick = np.arange(self.t_start+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end+1, self.t_per_year*4.)
    plt.xticks(xaxis_tick, np.arange(int(prodCountry_years_var[0]), int(prodCountry_years_var[-1])+1, 4), rotation=60)
    plt.ylabel('quantity (mmt)', size=self.axlsize)
    if yaxis_right==True:
      plt.gca().yaxis.tick_right()
      plt.gca().yaxis.set_label_position("right")      
    # plt.xlabel('month')
    # plt.xlabel('year')
    # plt.legend(loc=4)
    plt.text(self.xlims[0]+3, .6e8*self.qty_multiplier, 'Ending storage levels', size=self.axlsize)
    # plt.text(self.xlims[0]+3, 1.2e8*self.qty_multiplier, 'Ending storage levels', size=self.axlsize)
    plt.tick_params(axis='both', which='major', labelsize=self.ticklsize)
    # print 'Storage correlation: ', np.corrcoef(np.add(np.sum(Q_storage_prod, axis=1), np.sum(Q_storage_cons, axis=1))[self.repeatFirstColumnNTimes:-1], np.multiply(USDA_begStor[start_USDA+1:end_USDA-1], 1e3))[0,1]
    # Crop spin-up part
    plt.xlim(self.xlims)
    # Mark "out-of-sample" period
    ymin, ymax = ax1.get_ylim()
    ax1.add_patch(patches.Rectangle((taxis[-4]+0.5, ymin), 3.5, ymax-ymin, facecolor=self.SpecialColor3, edgecolor='none'))
    if storageCapacity==True:
      # Mark droughts
      ax1.plot([2010-1975+1+self.repeatFirstColumnNTimes*self.t_per_year], [0.21e9*self.qty_multiplier], 'v', color=self.SpecialColor1, markeredgecolor='none', markersize=10, zorder=1)
      ax1.text(self.repeatFirstColumnNTimes+31, 2.33e8*self.qty_multiplier, 'Droughts:', size=self.axlsize-3, color=self.SpecialColor1, horizontalalignment='right')
      ax1.text(self.repeatFirstColumnNTimes+33., 2.2e8*self.qty_multiplier, 'UKR\nAUS\nIND', size=self.axlsize-4, color=self.SpecialColor1, horizontalalignment='center')
      ax1.text(self.repeatFirstColumnNTimes+36, 2.2e8*self.qty_multiplier, 'RUS\nUSA\nCHN', size=self.axlsize-4, color=self.SpecialColor1, horizontalalignment='center')
      ax1.plot([2007-1975+1+self.repeatFirstColumnNTimes*self.t_per_year], [0.21e9*self.qty_multiplier], 'v', color=self.SpecialColor1, markeredgecolor='none', markersize=10, zorder=1)
      # Plot trade policy parameters
      import_multipliers = list(importStrat['World'].values())
      ax2 = ax1.twinx()
      # Line...
      # ax2.plot(taxis, np.multiply(np.multiply(np.squeeze(q_storage_max_cons), import_multipliers), self.qty_multiplier), '--', color=[0.6]*3, linewidth=2., label='storage capacity')
      # ax2.set_ylim([0.e8*self.qty_multiplier, 10.e8*self.qty_multiplier])
      # ...or Bars
      barcolor1 = 'indianred'
      barcolor2 = np.divide([103,169,207],256.)
      endyear = self.t_end/self.t_per_year + self.start_year - self.repeatFirstColumnNTimes
      ax2.bar(taxis[1999-endyear:]-0.4, np.subtract(import_multipliers[1999-endyear:], 1.), color=barcolor2)
      # ax2.bar([taxis[2007-endyear]-0.3, taxis[2010-endyear]-0.3], np.subtract([0.97, 0.95], 1.), width=[0.6, 0.6], color=barcolor1)
      try: 
        exportRestrYears = list(exportRestr['World'].keys())
      except KeyError: 
        print('Error: Cannot plot export restrictions for countries other than World. Adjust plot routine!') 
      for yr in exportRestrYears: 
          ax2.bar([taxis[yr-endyear]-0.3], np.subtract(exportRestr['World'][yr], 1.), width=0.6, color=barcolor1)
      ax2.set_ylim([-0.15, 0.5])
      # Move axis into plot area
      ax2.yaxis.tick_left()
      ax2.yaxis.set_label_position("left")      
      ax2.spines['left'].set_position(('data', self.repeatFirstColumnNTimes+24))
      ax2.spines['left'].set_smart_bounds(True)
      # ax2.text(self.repeatFirstColumnNTimes+17, 0.0, 'Trade \npolicies', size=self.axlsize-2, horizontalalignment='left')
      ax2.text(self.xlims[0]+10, 0.05, 'Trade policies', size=self.axlsize-2, horizontalalignment='left')
      ax2.text(self.repeatFirstColumnNTimes+35, 0.06, 'Import', size=self.axlsize-4, color=barcolor2) #, fontweight='bold')
      ax2.text(self.repeatFirstColumnNTimes+33.9, -0.06, 'Export', size=self.axlsize-4, color=barcolor1)
      ax2.set_xlim(self.xlims)
    if xlabels==False:
      ax1.axes.xaxis.set_ticklabels([])
    if self.subplots==False:
      plt.tight_layout()
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
      #eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)

  def plotPminPmax(self, fign, taxis, P_min_prod, P_max_prod):
    plt.figure(fign)
    plt.clf()
    plt.plot(taxis, P_min_prod[:,0], '--k')
    # plt.plot(taxis, P_min_prod[:,1], '--k')
    # plt.plot(taxis, P_min_prod[:,2], '--k')
    plt.plot(taxis, P_max_prod[:,0], '-k')
    # plt.plot(taxis, P_max_prod[:,1], '-k')
    # plt.plot(taxis, P_max_prod[:,2], '-k')
    plt.grid(True)
    # ylim([0, 1000])
    plt.ylabel('price')
    plt.xlabel('month')
    plt.title('p_max, p_min')

  def plotPolicyParameters(self, fign, taxis, importStrat, exportRestr, q_storage_max_cons_factor, figName, figSave):
    plt.figure(fign)
    plt.clf()
    fig, ax1 = plt.subplots(num=fign, figsize=(7,4))
    # ax2 = ax1.twinx()
    # policyYears = np.arange(2001, 2014)
    policyYears = np.arange(1997, 2014)
    # plt.plot(policyYears, q_storCons_mult*q_storage_max_cons_factor)
    import_mult = []
    export_mult = []
    for i,year in enumerate(policyYears):
      try:
        import_mult.append(importStrat['World'][year])
      except KeyError: 
        import_mult.append(q_storage_max_cons_factor)
      try:
        export_mult.append(exportRestr['World'][year])
      except KeyError: 
        export_mult.append(1.)
    ax1.plot(policyYears, import_mult, 'o--k')
    # ax1.set_ylim([1., 2.])
    ax1.set_ylim([q_storage_max_cons_factor-0.5, q_storage_max_cons_factor+0.5])
    ax1.set_xlim([policyYears[0],policyYears[-1]])
    ax1.plot([policyYears[0],policyYears[-1]], [q_storage_max_cons_factor, q_storage_max_cons_factor], '-k')
    try:
      # ax2.plot(policyYears, export_mult, 'o--b', markerfacecolor='none')
      ax2.plot(policyYears, export_mult, 's--b')
      # plt.legend(['$I_{max,c}$', '$\delta_{trade}$'], loc=1)
      ax2.set_ylim([.9, 1.1])
      ax2.set_ylabel('$\delta_{trade}$', color='b')
      for tl in ax2.get_yticklabels():
          tl.set_color('b')
    except NameError: 
      print("No second axis defined.")
    ax1.set_ylabel('$I_{max,c}$', color='k')
    plt.ion()
    plt.show()
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
      #eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)
      
  def plotSupplyDemandCurves(self, fign, timestep, glob_supply, glob_demand, figName, figSave):
    plt.figure(fign)
    plt.clf()
    plt.plot(glob_supply[np.arange(0,1000)],np.arange(1,1001), '-k')
    plt.plot(glob_demand[np.arange(0,850)],np.arange(1,851), '-b')
    # plt.plot(supply_prod[0,:], '--k')
    # plt.plot(supply_prod[1,:], '--k')
    # plt.plot(supply_prod[2,:], '--k')
    # # plt.plot(supply_prod[3,:], '--k')
    # # plt.plot(supply_prod[4,:], '--k')
    # plt.plot(demand_cons[0,:], '--b')
    # plt.plot(demand_cons[1,:], '--b')
    # plt.plot(demand_cons[2,:], '--b')
    plt.xlabel('quantity')
    plt.ylabel('price')
    plt.legend(['supply', 'demand'], loc=9)
    plt.ylim([0, 1000])
    plt.grid(True)
    plt.ion()
    plt.show()
    plt.title(str('Time step '+ str(timestep)))
    # plt.draw()
    # raw_input("Press Enter to continue...")
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
      #eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)

  def plotProduction(self, fign, taxis, prodCountry_quantity_var, prodCountry_years_var, figName, figSave):
    plt.figure(fign)
    plt.clf()
    axis_start = np.shape(prodCountry_quantity_var)[1]
    plt.plot(taxis[-axis_start:], np.sum(prodCountry_quantity_var, axis=0)*self.qty_multiplier, '-k')
    plt.ylabel('quantity (mmt)', size=self.axlsize)
    plt.text(self.xlims[0]+3, 2.1e8*self.qty_multiplier, 'Production', size=self.axlsize)
    plt.ylim([0, 800])
    plt.grid(True)
    plt.ion()
    plt.show()
    # plt.draw()
    # raw_input("Press Enter to continue...")
    xaxis_tick = np.arange(self.t_start+self.repeatFirstColumnNTimes*self.t_per_year, self.t_end+1, self.t_per_year*4.)
    plt.xticks(xaxis_tick, np.arange(int(prodCountry_years_var[0]), int(prodCountry_years_var[-1])+1, 4), rotation=60)
    plt.tick_params(axis='both', which='major', labelsize=self.ticklsize)
    if figSave:
      outfname = os.path.join(self.outdir,figName) 
      print(("Save data to %s" % outfname))
      if os.path.exists(outfname): 
        print("File exists, will be overwritten. ")
      #eval(input("Print figure? Press key to continue..."))
      plt.savefig(outfname)
