#!/usr/bin/env python

# -------------------------------------------------------------------------- #
# Core module of the TWIST (Simple version without commented code)
# (Trade WIth STorage) model. 
#
# jacob.schewe@pik-potsdam.de
# Converted to Python 3, 6/9/2018 (mjp38@columbia.edu)
# -------------------------------------------------------------------------- #

import numpy as np
import pickle
import matplotlib.pyplot as plt
import os, sys
import imp
from runningmean import runmean_matchLeftEdge as runmean


def main(module_inputfiles, module_settings, fitting_param=False):
    """TWIST core module.
       Loads input data as specified in TWIST_inputfiles.py;
       imports settings from TWIST_settings.py;
       runs the model;
       outputs results into a pickle file;
       calls plot module TWIST_plot.py.
    """

    # -------------------- Get input data and parameters --------------------

    import TWIST_functions
    import TWIST_plotFunctions
    inputfiles = imp.load_source('inputfiles', module_inputfiles)
    settings = imp.load_source('settings', module_settings)
    TWIST_plot = imp.load_source('TWIST_plot', os.path.abspath(settings.workdir + '/TWIST_plot.py'))

    dataPath = '{0}/{1}'.format(settings.localbasedir, 'input')
    #dataPath = '{0}/{1}/{2}'.format(settings.localbasedir, 'input', commodity)

    if not fitting_param:
        #print('************Not fitting***********')
        commodity = settings.commodity
        start_year = settings.start_year
        input_year = settings.input_year
        q_storage_init_prod = settings.q_storage_init_prod
        production_varying = settings.production_varying
        consumption_varying = settings.consumption_varying
        endogFinalDemand = settings.endogFinalDemand
        filtersize = settings.filtersize
        q_out_offset = settings.q_out_offset
        loss_factor = settings.loss_factor
        alpha_s = settings.alpha_s
        alpha_d = settings.alpha_d
        p_max_prod_const = settings.p_max_prod_const
        p_max_cons_base = settings.p_max_cons_base
        q_storage_max_cons_factor = settings.q_storage_max_cons_factor
        elast_finalDemand =settings.elast_finalDemand
        scenario_mode = settings.scenario_mode
        scenario_start_year = settings.scenario_start_year
        length_of_scenario = settings.length_of_scenario
        include_prodDecline = settings.include_prodDecline
        prodDecline_dic = settings.prodDecline_dic
        include_importStrat = settings.include_importStrat
        importStrat_dic = settings.importStrat_dic
        include_exportRestr = settings.include_exportRestr
        exportRestr_dic = settings.exportRestr_dic
        include_consReduction = settings.include_consReduction
        consReduction_dic = settings.consReduction_dic
        consumption_fixed_reported = settings.consumption_fixed_reported
        q_storage_max_cons_type = settings.q_storage_max_cons_type
        q_out_offset_type = settings.q_out_offset_type
    else:
        #print('fitting free param')
        commodity, start_year, input_year, q_storage_init_prod, production_varying, consumption_varying, \
        endogFinalDemand, filtersize, q_out_offset, loss_factor, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base,\
        q_storage_max_cons_factor, elast_finalDemand, scenario_mode, scenario_start_year, length_of_scenario, \
        include_prodDecline, prodDecline_dic,\
        include_importStrat, importStrat_dic,\
        include_exportRestr, exportRestr_dic, \
        include_consReduction, consReduction_dic, \
        consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type = fitting_param

       #print(commodity, start_year, input_year, q_storage_init_prod, production_varying, consumption_varying, endogFinalDemand, filtersize,
        #      q_out_offset)

        # todo: find spin up solution
        # print('--------')
        # print(alpha_s, alpha_d, p_max_prod_const, p_max_cons_base, q_storage_max_cons_factor)


    producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grain_endStock_file\
        = inputfiles.inputFiles(commodity, dataPath)

    funs = TWIST_functions.Functions(settings.n_price_units)

    prodCountry_names, n_producers, n_producers_per_country, month_of_harvest = funs.producer_country_data(producfileVar,
                                                                                    iso3file,
                                                                                    harvDatefile,
                                                                                    settings.q_clip,
                                                                                    settings.n_producers_smallest)

    consCountry_names, n_consumers = funs.consumer_country_data(consumfileVar, settings.q_clip)

    prodCountry_quantity_var, prodCountry_years_var = funs.read_harvest_timeseries(producfileVar, prodCountry_names,
                                                                                   start_year,
                                                                                   settings.q_trade)

    consCountry_quantity_var, consCountry_years_var = funs.read_demand_timeseries(consumfileVar, consCountry_names,
                                                                                  settings.q_trade)

    # Lists of Producers and consumers
    producers = np.arange(0, n_producers)
    consumers = np.arange(0, n_consumers)

    # Get n_producers x t_per_year array of fraction of annual harvest per month/timestep
    harvest_distrYear = funs.harvestingPeriod(settings.t_per_year, month_of_harvest, settings.harvest_distribution,
                                              producers)


    if (production_varying == 0):
        # For constant annual harvests:
        t_start = settings.constant_t_start
        t_end = settings.constant_t_end
        q_harvest = funs.harvest_timeseries_constant(t_start, t_end, settings.shock_year, settings.shock_month,
                                                     settings.shock_factor,
                                                     prodCountry_quantity_var,
                                                     prodCountry_years_var,
                                                     input_year,
                                                     settings.t_per_year)
    elif (production_varying == 1):
        # For time-varying annual harvests:
        q_harvest, t_start, t_end = funs.harvest_timeseries_varying(prodCountry_quantity_var, settings.t_per_year,
                                                                    settings.repeatFirstColumnNTimes)


    if (consumption_varying == 0):
        # For constant monthly demand:
        t_start = settings.constant_t_start
        t_end = settings.constant_t_end
        finalCons, p_max_cons, q_storage_max_cons = funs.demand_timeseries_constant(t_start, t_end,
                                                                                    consCountry_quantity_var,
                                                                                    consCountry_years_var,
                                                                                    input_year,
                                                                                    settings.min_frac,
                                                                                    p_max_cons_base,
                                                                                    q_storage_max_cons_factor,
                                                                                  settings.t_per_year)

    elif (consumption_varying == 1):
        # For time-varying monthly demand:
        q_harvest_total_init = np.sum(q_harvest[0, :])
        finalCons, p_max_cons, q_storage_max_cons, q_out_min_annual = funs.demand_timeseries_varying(
                                                                        consCountry_quantity_var,
                                                                        filtersize,
                                                                        settings.t_per_year,
                                                                        settings.repeatFirstColumnNTimes,
                                                                        settings.min_frac,
                                                                        p_max_cons_base,
                                                                        settings.match_q_spinup,
                                                                        q_harvest_total_init,
                                                                        q_storage_max_cons_factor,
                                                                        q_storage_max_cons_type,
                                                                        q_out_offset,
                                                                        q_out_offset_type,
                                                                        consumption_fixed_reported, loss_factor)

    t_scenario = 0
    if scenario_mode == 'one_year_shock_with_proj':
        # print('print scenario mode:', scenario_mode)
        # change the production and consumption at the given scenario year
        q_harvest, t_end, t_scenario = funs.harvest_timeseries_scenario_one_year(q_harvest, prodCountry_years_var, scenario_start_year,
                                                      settings.repeatFirstColumnNTimes, length_of_scenario,
                                                      production_decline)

        # # if consumption is reduced to simulate direct consumption by the export country with export restrictions
        # # then the producer storage need to be reduced by that amount
        # if consumption_fixed_reported == True and include_exportRestr == True:
        #     scenario_direct_consumption = finalCons[t_scenario][0] * (1-consumption_reduced)
        #


        # consumption, default flexible, if consumption_fixed_reported == True then no smoothing and fixed
        finalCons, q_storage_max_cons, cons_nonSmoothed = funs.demand_timeseries_scenario(finalCons, q_storage_max_cons,
                                                consCountry_quantity_var, consCountry_years_var, scenario_start_year,
                                                settings.repeatFirstColumnNTimes, length_of_scenario,
                                                settings.q_out_offset_type, q_out_offset, consumption_fixed_reported)





    elif scenario_mode == 'constant_shock_size_all_years':
        # print('print scenario mode:', scenario_mode)
        # consumption, default flexible, if consumption_fixed_reported == True then no smoothing and fixed
        finalCons, q_storage_max_cons, cons_nonSmoothed = funs.demand_timeseries_scenario(finalCons, q_storage_max_cons,
                                                consCountry_quantity_var, consCountry_years_var, scenario_start_year,
                                                settings.repeatFirstColumnNTimes, length_of_scenario,
                                                settings.q_out_offset_type, q_out_offset, consumption_fixed_reported, consumption_reduced)

        # change the production for consecutive years after the initial shock, keep constant.
        q_harvest, t_end = funs.harvest_timeseries_scenario_all_years(q_harvest, prodCountry_years_var, scenario_start_year,
                                                      settings.repeatFirstColumnNTimes,  length_of_scenario,
                                                      production_decline)




    # Further parameters defining agents' behaviour:
    p_storage, q_storage_prod = funs.producerInput(t_start, t_end, settings.q_trade, q_storage_init_prod)
    q_storage_cons = funs.consumerInput(finalCons)
    countries_producers = funs.countries(prodCountry_names)

    # Create an *independent* array
    q_out_min = np.array([item for item in finalCons])



    # Load (prescribed) export restrictions and import strategies
    # exportRestr = funs.exportRestrictions(include_exportRestr)
    # importStrat = funs.importStrategies()
    ###  importStrat = funs.importStrategies(settings.repeatFirstColumnNTimes, start_year, consCountry_names, consCountry_years_var)

    exportRestr = {}
    if include_exportRestr == True:
        exportRestr['World'] = exportRestr_dic
        # print('Export restrictions', exportRestr)

    consReduction = {}
    if include_consReduction == True:
        consReduction = consReduction_dic

    prodDecline = {}
    if include_prodDecline == True:
        prodDecline = prodDecline_dic

    # import strategies
    importStrat = {}
    if include_importStrat == True:
        importStrat = importStrat_dic

    # Global plot settings
    qty_multiplier = settings.qty_multiplier
    ticklsize = settings.ticklsize
    axlsize = settings.axlsize
    # Load plot functions
    plotFunctions = TWIST_plotFunctions.PlotFunctions(settings.outdir, settings.t_per_year, start_year,
                                                      t_start, t_end, settings.repeatFirstColumnNTimes,
                                                      consumption_varying, qty_multiplier, axlsize, ticklsize,
                                                      endogFinalDemand, subplots=False)

    # Initialize arrays
    # Supply- and demand-curves
    supply_prod = np.zeros((n_producers, settings.n_price_units))
    demand_cons = np.zeros((len(consumers), settings.n_price_units))


    # Balance of residual quantities that arise from discrete nature of the curves:
    q_resid = 0
    # Sum of unsold produce discarded by producers:
    q_discard = 0
    # Counter of consumers running out of stocks:
    n_cons_fail = 0
    # Lists for recording time step results:
    Px = []
    # Q_out = []
    Q_resid = []
    Q_storage_prod = np.empty(((t_end - t_start + 1), n_producers))
    Q_storage_cons = np.empty(((t_end - t_start + 1), n_consumers))
    Q_out = np.empty(((t_end - t_start + 1), n_consumers))
    Q_harvest = np.empty(((t_end - t_start + 1), n_consumers))
    P_min_prod = np.empty(((t_end - t_start + 1), n_producers))
    P_max_prod = np.empty(((t_end - t_start + 1), n_producers))
    q_harvest_last = np.zeros((n_producers))

    # Log file:
    fo = open('TWIST_out.txt', 'w')
    #fo_opt = open('TWIST_out_WM_price.cvs', 'w')
    #print('#year', ';', 'world_makret_price', ';', 'quantity', file=fo_opt)

    # --------------------------- Run ----------------------------- #
    if not fitting_param:
        # Tell user what they chose
        print('---------- Hello! ----------')
        print('We are running at ', settings.t_per_year, ' time steps ("months") per year.')


    # Time-forward loop:
    t = t_start
    year_sim = []
    q_out_running_list = []
    while t <= t_end:
        month_total = t
        year = int(np.floor(np.divide((t - 1), settings.t_per_year)))
        month_of_year = t - (year) * settings.t_per_year
        print('This is month ', int(month_of_year), ' of year ', int(year), file=fo)

        # # Added so that the the initial storage level is shared between the producer and consumer for the first time step
        # if t == 1:
        #     q_storage_cons = [q_storage_prod[0]*0.5]
        #     q_storage_prod = [q_storage_prod[0]*0.5]



        # Producer side:
        # Harvest and computation of individual and global supply-curves
        for producer in producers:
            # Harvest
            if harvest_distrYear[producer, month_of_year - 1] != 0:
                # Calculate this month's new harvest
                new_harvest = q_harvest[year, producer] * harvest_distrYear[producer, month_of_year - 1]

                # # if you are running a scenario and the baseline option, then keep the production constant
                # if scenario_mode == True and production_decline == 1 and t > (t_end - length_of_scenario):
                #     new_harvest = float(np.array(q_harvest_last))
                #     q_harvest_match_cons = new_harvest
                #
                # # if you are running scenario and not base line, consumption should still be matched to the baseline fixed production
                # if scenario_mode == True and production_decline != 1 and t == (t_end - length_of_scenario+1):
                #     q_harvest_match_cons = float(q_harvest_last)

                try:
                    production_multiplier = prodDecline[year - settings.repeatFirstColumnNTimes + start_year]
                    print('Production decline', year - settings.repeatFirstColumnNTimes + start_year, ', factor:',  production_multiplier)
                except KeyError:
                     production_multiplier = 1.

                # absolute amount of production due to a shock, production_shock=0 in case prodDecline=False
                production_shock = new_harvest * (1 - production_multiplier)

                # in case of a production shock lower the current production
                new_harvest -= production_shock



                # Add new harvest to storage
                q_storage_prod[producer] += new_harvest
                print('Producer ', producer, ' has harvested ', new_harvest, ' units.', file=fo)
                # Record last harvest quantity for use in producerVariables
                q_harvest_last[producer] = new_harvest




            # save the harvest values
            Q_harvest[t -1, producer] = new_harvest

            # print 'Producer ', producer, ' storage level is ', q_storage_prod[producer], ' units.'
            # Determine p_min_prod, p_max_prod
            p_min_prod, p_max_prod = funs.producerVariables(settings.t_per_year, t, year, month_of_year, Px,
                                                            p_storage[producer], month_of_harvest[producer],
                                                            q_harvest[year, producer],
                                                            Q_storage_prod[:, producer],
                                                            p_max_prod_const)


            # print >>fo, "Supply curve exponent alpha is ", alpha_s
            P_min_prod[t - 1, producer] = p_min_prod
            P_max_prod[t - 1, producer] = p_max_prod
            # (Prescribed) export restrictions make some of the storage volume unavailable for trade

            try:
                export_multiplier = exportRestr[countries_producers[producer]][
                    year - settings.repeatFirstColumnNTimes + start_year]
                print('export', year - settings.repeatFirstColumnNTimes + start_year, export_multiplier)
            except KeyError:
                export_multiplier = 1.



            # Numerical supply-curve (1-D array):
            supply_prod[producer] = funs.producerSupplyCurve(q_storage_prod[producer] * export_multiplier, p_min_prod,
                                                             p_max_prod, alpha_s)

        # print >>fo, "Producer storage levels are ", q_storage_prod
        # print >>fo, "Last harvests were ", q_harvest_last

        # Construct global supply-curve
        glob_supply = np.sum(supply_prod, axis=0)

        # Consumer side:
        # Determine q_out
        # ...of first time step (when there is no history); to use for construction of demand curve, not for actual consumption
        if t == 1:
            q_out = q_out_min[t - 1, :]

        try:
            consumption_multiplier = consReduction[year - settings.repeatFirstColumnNTimes + start_year]
            print('reduce', year - settings.repeatFirstColumnNTimes + start_year, consumption_multiplier)
        except KeyError:
            consumption_multiplier = 1.

        # absolute amount of consumption that is reduced in case consumption_multiplier is != 1 for the current timestep
        abs_consumption_reduction = q_out * (1 - consumption_multiplier)

        try:
            importStrat_multiplier = importStrat[year - settings.repeatFirstColumnNTimes + start_year]
            # print('Import strategy', year - settings.repeatFirstColumnNTimes + start_year, importStrat_multiplier)
        except KeyError:
            importStrat_multiplier = 1.

        # Export restrictions (if applicable) and computation of individual demand-curves and import strategy
        for consumer in consumers:
            ### Numerical demand-curve (1-D array):
            demand_cons[consumer] = funs.consumerDemandCurve(consumer, q_storage_cons, q_out, p_max_cons[:],
                                                             q_storage_max_cons[t - 1, :]*importStrat_multiplier, alpha_d, abs_consumption_reduction)

        # Construct global demand-curve
        glob_demand = np.sum(demand_cons, axis=0)

        # demand-curve only for calculating the stock to use change when there is a change in q_storage_max_cons
        # only for 2020 shock
        if year - settings.repeatFirstColumnNTimes + start_year == scenario_start_year:
            demand_cons_basline = np.zeros((len(consumers), settings.n_price_units))
            for consumer in consumers:
                if commodity == 'Wheat_proj':
                    baseline_importStrat_multiplier = 1.05
                elif commodity == 'Corn_proj':
                    baseline_importStrat_multiplier = 1.2
                elif commodity == 'Rice_proj':
                    baseline_importStrat_multiplier = 1.05
                else:
                    baseline_importStrat_multiplier = 1

                ### Numerical demand-curve (1-D array):
                demand_cons_basline[consumer] = funs.consumerDemandCurve(consumer, q_storage_cons, q_out, p_max_cons[:],
                                                                 q_storage_max_cons[t - 1, :]*baseline_importStrat_multiplier, alpha_d, abs_consumption_reduction)

            # print the value if it is different from the baseline value.
            if not importStrat_multiplier == 1.0:
                print('Import strategy', year - settings.repeatFirstColumnNTimes + start_year,', factor:', importStrat_multiplier)

        # Find world market price = intersection of global supply- and demand-curve.
        px, p1 = funs.worldMarketPrice(glob_demand, glob_supply)
        if np.isnan(px):
            print('Error: world market price is NaN.')
            sys.exit("Aborting...")
        print('World market price is ', px, ', last integer below is ', p1, file=fo)
        Px.append(px)

        # Calculate global traded quantity
        # Trade only as much as producers want to sell:
        # slope
        m = glob_supply[p1 + 1] - glob_supply[p1]
        # q-axis intercept
        b = glob_supply[p1] - m * p1
        # qx
        qx_glob = m * px + b

        # Transactions
        sold_units = 0
        for producer in producers:
            # Compute qx = q(px) analytically to avoid underestimation due to discrete p-axis
            # slope
            m = supply_prod[producer, p1 + 1] - supply_prod[producer, p1]
            # q-axis intercept
            b = supply_prod[producer, p1] - m * p1
            # qx
            qx = m * px + b
            # Sell
            # print(qx)
            q_storage_prod[producer] -= qx

            # q_storage_prod[producer] -= new_harvest
            if q_storage_prod[producer] < 0:
                print('Error: Producer ', producer, ' storage level has become negative!')
                sys.exit("Aborting...")
            # Count sold units
            sold_units += qx
            # if producer == 1:
            # print 'Producer ', producer, ' has sold ', (qx), ' units. Storage level is ', q_storage_prod[producer], ' units.'

        # reduce by the amount which is consumed directly, abs_consumption_reduction=0 in case of include_consReduction=False

        q_storage_prod -= abs_consumption_reduction

        Q_storage_prod[t - 1] = q_storage_prod

        # add spillage rate is activated
        # reduce the excess stock by the loss rate so that the producer storage level is lowered before next time step
        if q_out_offset_type == 2 or q_out_offset_type == 3:
            q_storage_prod[producer] = q_storage_prod[producer] * loss_factor
        # demand-curve only for calculating the stock to use change when there is a change in q_storage_max_cons
        # only for 2020 shock
        if year - settings.repeatFirstColumnNTimes + start_year == scenario_start_year:
            q_storage_cons_baseline = q_storage_cons.copy()

            # Compute qx = q(px) analytically to avoid underestimation due to discrete p-axis slope
            m = demand_cons_basline[consumer, p1 + 1] - demand_cons_basline[consumer, p1]
            # q-axis intercept
            b = demand_cons_basline[consumer, p1] - m * p1
            # qx
            qx = m * px + b
            # Buy
            q_storage_cons_baseline[consumer] += qx


        purchased_units = 0
        for consumer in consumers:
            # Compute qx = q(px) analytically to avoid underestimation due to discrete p-axis
            # slope
            m = demand_cons[consumer, p1 + 1] - demand_cons[consumer, p1]
            # q-axis intercept
            b = demand_cons[consumer, p1] - m * p1
            # qx
            qx = m * px + b
            # Buy
            q_storage_cons[consumer] += qx

            # Count purchased units
            purchased_units += qx
            # print 'Consumer ', consumer, ' has bought ', (qx), ' units. Storage level is ', q_storage_cons[consumer], ' units.'




        # Determine new q_out; this is what will be consumed this month
        # Completely isoelastic final demand (assuming relative consumer storage level as proxy for domestic price):
        if endogFinalDemand == 1 and consumption_fixed_reported != True:
            if t <= settings.repeatFirstColumnNTimes:
                q_out = q_out_min[t - 1, :]

                #q_out_running_list.append(float(q_out))
                # if scenario_mode == True and t <= (t_end-length_of_scenario):
                #     q_out_price_corrected.append(float(q_out))

            elif t > settings.repeatFirstColumnNTimes:
                q_out_mid = q_out_min[t - 1,
                            :] / settings.min_frac  # Retrieving 100% of observed (long-term average) demand


                ### Reference price: Average of previous years
                pref = np.mean(Px[-settings.tau:])

                ### Domestic price index: World price modulated by relative storage level
                pdom = px / pref

                ### Consumption
                q_out = q_out_mid * (pdom ** elast_finalDemand)

                q_out[q_out < (settings.q_out_cutoff * q_out_mid)] = settings.q_out_cutoff * q_out_mid[
                   q_out < (settings.q_out_cutoff * q_out_mid)]  # Cut-off

                # # get the reference consumption for the previous years
                # q_out_ref = np.array([runmean(q_out_running_list, filtersize)[-1]])
                #
                # q_out = q_out_ref * (pdom ** elast_finalDemand)
                #
                # q_out[q_out < (settings.q_out_cutoff * q_out_ref)] = settings.q_out_cutoff * q_out_ref[
                #   q_out < (settings.q_out_cutoff * q_out_ref)]  # Cut-off
                #q_out_running_list.append(float(q_out))

        elif consumption_fixed_reported == True:
            q_out = q_out_min[t - 1, :]

            q_out -= abs_consumption_reduction

        elif endogFinalDemand == 0:
            q_out = q_out_min[t - 1, :]


        # demand-curve only for calculating the stock to use change when there is a change in q_storage_max_cons
        # only for 2020 shock
        if year - settings.repeatFirstColumnNTimes + start_year == scenario_start_year:
            q_storage_cons_scenario = q_storage_cons.copy()
            for consumer in consumers:
                # Consume
                q_storage_cons_scenario[consumer] -= q_out[consumer]
                q_storage_cons_baseline[consumer] -= q_out[consumer]

                stock_to_use_scenario = q_storage_cons_scenario[0]/q_out[0]
                stock_to_use_baseline = q_storage_cons_baseline[0]/q_out[0]

                # comment for fitting
                # in order to determine the right value of the importStrat_multiplier when applied to
                # "q_storage_max_cons[t - 1, :]*importStrat_multiplier" in order to calculate the S/U compared to
                # the baseline S/U, since the increased demand scenario is based on increase in S/U and not increase
                # in q_storage_max_cons
                # print(year - settings.repeatFirstColumnNTimes + start_year, 'S/U=', stock_to_use_scenario, ' No change in importStat S/U=', stock_to_use_baseline, ' Change in S/U:', stock_to_use_scenario/stock_to_use_baseline)


        for consumer in consumers:
            # Consume
            if (q_storage_cons[consumer] - q_out[consumer]) < 0:
                shortage = abs(q_storage_cons[consumer] - q_out[consumer])
                # print('consumer out of stock, year', start_year + (year - settings.repeatFirstColumnNTimes)) # comment for fitting
                print('Consumer ', consumer,
                      ' has run out of stocks! Storage level falls short of required consumption by ', shortage,
                      ' units.', file=fo)
                n_cons_fail += 1
                q_storage_cons[consumer] = 0.
                Q_out[t - 1, consumer] = q_out[consumer] - shortage

                # # If you run out of stocks the consumption should  still match production for scenario run
                # if scenario_mode == True and t > (t_end - length_of_scenario):
                #     Q_out[t - 1, consumer] = q_harvest_match_cons
            else:
                q_storage_cons[consumer] -= q_out[consumer]
                Q_out[t - 1, consumer] = q_out[consumer]

            Q_storage_cons[t - 1] = q_storage_cons


        # add the mean consumption and value of the consumer max storage level for the scenario years to the historic part, which will be used in the next setp
        if scenario_mode == 'constant_shock_size_all_years' and t >= t_end-length_of_scenario:

            # add the current consumption to the original un-smoothed historic time series
            cons_nonSmoothed = np.concatenate((cons_nonSmoothed, np.array([float(q_out)])))

            # smooth the data
            #cons_smoothed = runmean(cons_nonSmoothed, filtersize)
            # get the current smoothed value of the consumption
            #q_out_mean = cons_smoothed[-1]

            # determine the running mean of the current consumption by extending the consumption time series with half
            # the running mean window (total size is filtersize*2+1) with the expected increase in consumption. This is
            # done because for the right edge value is otherwise extended by repeating the edge values, which will in
            # our case will mean we will just flatten out consumption over the scenario period.
            temp_mean = cons_nonSmoothed.copy()
            yearly_increase_in_production = 1.02  # 2% mean increase in wheat production from previous year
            for i in range(filtersize):
                temp_mean = np.append(temp_mean, float(q_out)*yearly_increase_in_production**(i+1))

            q_out_mean = np.mean(temp_mean[-(filtersize*2+1):])


            # add the current running mean to the historic time period of running mean
            q_out_min_extended = np.append(q_out_min, [q_out_mean])
            # get the right size back again
            q_out_min = np.array([[x] for x in q_out_min_extended])

            # determine the surplus storage capacity determined as a certain percentage of the initial year +
            # the current mean consumption
            q_max_cons_plus = np.multiply(q_out_min[0], q_storage_max_cons_factor - 1.)
            q_max_cons_current = q_out_mean+q_max_cons_plus
            q_storage_max_cons_extended = np.append(q_storage_max_cons, q_max_cons_current)
            # transform in the right shape again
            q_storage_max_cons = np.array([[x] for x in q_storage_max_cons_extended])



        # Check for residual quantity
        # print 'Total units sold, purchased: ', sold_units, purchased_units
        # print 'difference between units sold and units purchased is ', (sold_units - purchased_units)
        q_resid += (sold_units - purchased_units)
        Q_resid.append(q_resid)

        if not fitting_param:
            if settings.plotExampleCurves:
                # Example plot of supply- and demand curves at a given time:
                plotm = 10
                if (production_varying == 1):
                    plotm += settings.repeatFirstColumnNTimes * settings.t_per_year
                if t in [plotm, (plotm + 1)]:
                    figNum = (t - plotm + 1)
                    figName = 'supply_demand_curves.pdf'
                    figSave = 1
                    plotFunctions.plotSupplyDemandCurves(figNum, t, glob_supply, glob_demand, figName, figSave)



        if t >= settings.repeatFirstColumnNTimes and t < t_end:
            year_real = start_year + (1 + year - settings.repeatFirstColumnNTimes)
            year_sim.append(year_real)
           # print(year_real, ';', px, ';', float(q_storage_prod + q_storage_cons), file=fo_opt)

        # Step up time counter
        t += 1

    if not fitting_param:
        print('---------- Summary ----------')
        print('We have run for ', month_total, ' time steps.')
        print('Residual quantity of goods is ', q_resid, '.')
        print('Consumers have run out of stocks ', n_cons_fail, ' times.')
        print('Producers have discarded ', q_discard / 1.e9, ' billion units of unsold produce.')

    #print('')
    fo.close()
    #fo_opt.close()

    ##### Pickle ####
    # Results file:
    fp = open('TWIST_results.pkl', 'wb')
    pickle.dump((Px, Q_out, Q_storage_prod, Q_storage_cons, P_min_prod, P_max_prod), fp)
    fp.close()


    ### Call plot module ###

    if not fitting_param:
        # Switch on interactive mode so that the plot module may plot on the screen
        plt.ion()

        print("\nCalling plot module...\n")
        TWIST_plot.main(inputfiles, settings)

    return Px[(settings.repeatFirstColumnNTimes):], \
           Q_storage_prod[(settings.repeatFirstColumnNTimes):]+Q_storage_cons[(settings.repeatFirstColumnNTimes):], \
           Q_storage_prod[(settings.repeatFirstColumnNTimes):], \
           Q_storage_cons[(settings.repeatFirstColumnNTimes):], \
           Q_storage_prod[:], \
           Q_storage_cons[:], \
           Q_out[(settings.repeatFirstColumnNTimes):], \
           Q_out[:], \
           Q_harvest[(settings.repeatFirstColumnNTimes):], \
           year_sim



if __name__ == '__main__':
    # TWIST_core.py executed as script
    #
    # As we're running in an interactive session, reload modules in case they have been changed:
    print("Caution: Executing TWIST main module directly. \
      Make sure the inputfiles and settings used are as desired.")

    # Present directory is working directory
    workdir = os.path.dirname(os.path.realpath(__file__))
    inputfiles = str(workdir + '/TWIST_inputfiles.py')
    settings = str(workdir + '/TWIST_settings.py')

    # Run the model:
    main(inputfiles, settings)
