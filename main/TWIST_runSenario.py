import os
import sys
import imp
import numpy as np
import matplotlib.pyplot as plt
import TWIST_coreSimple
import TWIST_functions
from datetime import datetime
from matplotlib.ticker import MaxNLocator


def main(commodity, scenario_mode, historic_start_year, scenario_start_year, length_of_scenario, include_prodDecline, prodDecline_dic, include_importStrat, importStrat_dic, include_exportRestr, exportRestr_dic, include_consReduction, consReduction_dic, consumption_fixed_reported):
    date = datetime.now().strftime('%Y-%m-%d %H:%M')

    print("\nTWIST scenario module\n")
    print('Commodity:', commodity)
    print('Historic period:', historic_start_year, 'to', scenario_start_year-1)
    print('Scenario period:', scenario_start_year, 'to', scenario_start_year+length_of_scenario,'\n')

    # Present directory is working directory
    workdir = os.path.dirname(os.path.realpath(__file__))
    settings = str(workdir + '/TWIST_settings.py')
    inputfiles = str(workdir + '/TWIST_inputfiles.py')


    _settings_ = imp.load_source('settings', settings)
    _inputfiles_ = imp.load_source('inputfiles', inputfiles)

    modelbasedir = os.path.abspath(_settings_.localbasedir)
    sys.path.append(modelbasedir)
    dataPath = '{0}/{1}'.format(_settings_.localbasedir, 'input')
    savePath = '{0}/{1}'.format(_settings_.localbasedir, 'scenario_result')

    # create dictionary where the fitting results will be saved
    #os.mkdir(savePath)

    # get the name and path of the input file for the selected commodity
    producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStock_file = _inputfiles_.inputFiles(
        commodity,
        dataPath)

    funs = TWIST_functions.Functions(_settings_.n_price_units)

    if commodity == 'Wheat' or commodity == 'wheat':
        q_out_offset = 2611363.7937285574
        alpha_s = 0.09347491449809266
        alpha_d = 1.3765466341755122
        p_max_prod_const = 599.9999999999195
        p_max_cons_base = 650.0000000000001
        q_storage_max_cons_factor = 1.5185408892910361
        cropYear_month1 = 6

    elif commodity == 'wheat_proj' or commodity == 'Wheat_proj':
        historic_start_year = 1975
        cropYear_month1 = 6
        q_storage_max_cons_type = 0
        q_out_offset_type = 0

        loss_factor = 1.0
        q_out_offset = 2800570.445512435

        alpha_s = 0.12160632882103238
        alpha_d = 0.6865763166524175
        p_max_prod_const = 219.90003763140106
        p_max_cons_base = 1489.7230171361807
        q_storage_max_cons_factor = 1.678958539479257

        # #Jacobs 2017 paper
        # q_out_offset = 2800000.0
        # alpha_s = 0.1
        # alpha_d = 1.0
        # p_max_prod_const = 350.0
        # p_max_cons_base = 850.0
        # q_storage_max_cons_factor = 1.55


    elif commodity == 'Rice' or commodity == 'rice':
        q_out_offset = 1118139.8249075676
        alpha_s = 0.6826275736634217
        alpha_d = 1.4999915988623311
        p_max_prod_const = 207.37990317461018
        p_max_cons_base = 1097.5614550363118
        q_storage_max_cons_factor = 1.6622956878152386
        cropYear_month1 = 8

    elif commodity == 'Rice_proj' or commodity == 'rice_proj':
        historic_start_year = 1975
        # cropYear_month1 = 1
        # q_storage_max_cons_type = 2
        # q_out_offset_type = 0
        # q_out_offset = 1534307.1289919969
        # alpha_s = 0.4222340014186418
        # alpha_d = 1.169932052142505
        # p_max_prod_const = 219.50416085911684
        # p_max_cons_base = 2199.9999948036525
        # q_storage_max_cons_factor = 1.3182503164578954

        # q_storage_max_cons_type = 1
        # q_out_offset_type = 0
        # q_out_offset = 1528521.3501655988
        # alpha_s = 0.1 #fixed
        # alpha_d = 0.9832068431188863
        # p_max_prod_const = 249.99999999999997
        # p_max_cons_base = 2032.9581187240076
        # q_storage_max_cons_factor = 1.3615307698598877

        cropYear_month1 = 1
        q_storage_max_cons_type = 1
        q_out_offset_type = 0

        loss_factor = 1.0
        q_out_offset = 1528521.3501655988

        alpha_s = 0.14999999999822272
        alpha_d = 1.2611893937096115
        p_max_prod_const = 249.99999999999997
        p_max_cons_base = 1767.8939996361903
        q_storage_max_cons_factor = 1.2994046797590273

    elif commodity == 'Corn' or commodity == 'corn':
        cropYear_month1 = 9
        q_out_offset = 1432796.626606623
        alpha_s = 0.6672767411153131
        alpha_d = 0.8538841549305937
        p_max_prod_const = 106.35015505572002
        p_max_cons_base = 650.0000000001278
        q_storage_max_cons_factor = 1.8145664740709542

    elif commodity == 'Corn_proj' or commodity == 'corn_proj':
        historic_start_year = 1975
        # cropYear_month1 = 8
        # q_storage_max_cons_type = 0
        # q_out_offset_type = 0
        # q_out_offset = 1328391.5118264556
        # alpha_s = 0.49999930891671635
        # alpha_d = 0.6010348921054514
        # p_max_prod_const = 94.4704754806224
        # p_max_cons_base = 706.4650438614258
        # q_storage_max_cons_factor = 1.9498987551310856]

        # cropYear_month1 = 9
        # q_storage_max_cons_type = 0
        # q_out_offset_type = 0
        # q_out_offset = 1348071.5595195028
        # alpha_s = 0.1 # fixed
        # alpha_d = 0.7281114607708978
        # p_max_prod_const = 109.5883097955085
        # p_max_cons_base = 502.8850498850064
        # q_storage_max_cons_factor = 1.9934567173973283

        cropYear_month1 = 8
        q_storage_max_cons_type = 0
        q_out_offset_type = 0

        loss_factor = 1.0
        q_out_offset = 1348071.5595195028

        alpha_s = 0.14976020867121806
        alpha_d = 0.7144599886411545
        p_max_prod_const = 109.61030026631633
        p_max_cons_base = 577.066376473279
        q_storage_max_cons_factor = 1.9083889232550795

    elif commodity == 'Soybean' or commodity == 'soybean' or commodity == 'Soybeans' or commodity == 'soybeans':
        historic_start_year = 1975
        cropYear_month1 = 8
        q_storage_max_cons_type = 1
        q_out_offset_type = 4

        q_out_offset = 0.9947524968087756
        loss_factor = 1.0033248512909552
        q_storage_init_prod = 6086000.0

        alpha_s = 0.1498308796730929
        alpha_d = 0.5477861232804951
        p_max_prod_const = 227.50418703149296
        p_max_cons_base = 1499.9010418296555
        q_storage_max_cons_factor = 1.6675170367588628
    else:
        sys.exit('Model is not calibrated for commodity {} yet. Available crops are Wheat, Rice and Corn'.format(commodity))


    # use fix value, for the endogenuos determination of the final demand estimates (FlexCons)
    elast_finalDemand = -0.1

    historic_time_period = np.arange(historic_start_year, scenario_start_year)

    # get the beginning stock for the time period to be fitted
    beginning_stocks = funs.read_in_stock_data(grainStorage_file, historic_time_period)

    # set the initial storage level
    q_storage_init_prod = beginning_stocks[0]

    production_varying = 1
    consumption_varying = 1
    endogFinalDemand = 1
    filtersize = 5  # this is the window size which the reference consumption is calculated
    # number of years => 2*filtersize +1

    # print("\nRun Baseline.")
    # # run baseline case
    # baseline = 1 # no decline, run as baseline scenario
    # scenario_mode = True
    # parameters = [commodity, historic_start_year, historic_start_year, q_storage_init_prod, production_varying,
    #               consumption_varying, \
    #               endogFinalDemand, filtersize, q_out_offset, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base, \
    #               q_storage_max_cons_factor, elast_finalDemand, scenario_mode, scenario_start_year, baseline,
    #               length_of_scenario, include_exportRestr, consumption_reduced_at_scenario_start,
    #               consumption_fixed_reported]

    # price_base, total_stocks_base, prod_stocks_base, cons_stocks_base, prod_stocks_base_spinup, cons_stocks_base_spinup, consumption_base, consumption_base_spinup, harvest_base, year_sim_base = TWIST_coreSimple.main(inputfiles, settings, parameters)


    #print('\nConstumer stock historic average=', np.average(cons_stocks_base[:-length_of_scenario]))




    print("\nRun Scenario.")
    parameters = [commodity, historic_start_year, historic_start_year, q_storage_init_prod, production_varying, consumption_varying, \
                  endogFinalDemand, filtersize, q_out_offset, loss_factor, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base, \
                  q_storage_max_cons_factor, elast_finalDemand, scenario_mode, scenario_start_year, length_of_scenario,
                  include_prodDecline, prodDecline_dic,
                  include_importStrat, importStrat_dic,
                  include_exportRestr, exportRestr_dic,
                  include_consReduction, consReduction_dic,
                  consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

    # run scenario
    price, total_stocks, prod_stocks, cons_stocks, prod_stocks_spinup, cons_stocks_spinup, consumption, consumption_spinup, harvest, year_sim = TWIST_coreSimple.main(inputfiles, settings, parameters)

    print("\nRun Projection.")
    # run baseline case
    baseline = {}  # no prod decline, no export restrictions, no cons reduction. run as baseline scenario
    scenario_mode = False
    include_exportRestr = False
    if commodity == 'Wheat_proj':
        baseline_importStat = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2011:1.05, 2012:1.05, 2013:1.05, 2014:1.05, 2015:1.05, 2016:1.05, 2017:1.05, 2018:1.05, 2019:1.05, 2020:1.05, 2021:1.05}
    elif commodity == 'Corn_proj':
        baseline_importStat = {1998: 0.98, 1999:0.94, 2000:0.92, 2001:0.88, 2002:0.85, 2003:0.85, 2004:0.85, 2005:0.85, 2006:0.85, 2011:1.2, 2012:1.2, 2013:1.2, 2014:1.2, 2015:1.2, 2016:1.2, 2017:1.2, 2018:1.2, 2019:1.2, 2020:1.2, 2021:1.2, 2022:1.2}
    elif commodity == 'Rice_proj':
        baseline_importStat = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2015:1.02, 2016:1.02, 2017:1.03, 2018:1.04, 2019:1.05, 2020:1.05, 2021:1.05, 2022:1.05}
    elif commodity == 'Soybean':
        baseline_importStat = {1986:0.98, 1987:0.96, 1988:0.94, 1989:0.94, 1990:0.94, 1991:0.94, 1992:0.94, 1993:0.94, 1994:0.94, 1995:0.94, 1996:0.94, 1997:0.92, 1998: 0.9, 1999: 0.88, 2000: 0.88, 2001: 0.88, 2002: 0.88, 2003: 0.88, 2004: 0.88, 2003: 0.88, 2004: 0.88, 2005: 0.9, 2006: 1.02, 2007: 1.02, 2008: 1.05, 2009: 1.05, 2010: 1.05, 2011: 1.05, 2012: 1.05, 2013: 1.05, 2014: 1.0, 2015: 1.0, 2016: 1.0, 2017: 1.0, 2018: 1.0, 2019: 1.0, 2020: 1.0}
        # baseline_importStat = {}
    else:
        baseline_importStat = {}
    parameters = [commodity, historic_start_year, historic_start_year, q_storage_init_prod, production_varying,
                  consumption_varying, \
                    endogFinalDemand, filtersize, q_out_offset, loss_factor, alpha_s, alpha_d, p_max_prod_const, p_max_cons_base, \
                  q_storage_max_cons_factor, elast_finalDemand, scenario_mode, scenario_start_year, length_of_scenario,
                  include_prodDecline, baseline,
                  include_importStrat, baseline_importStat,
                  include_exportRestr, baseline,
                  include_consReduction, baseline,
                  consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

    price_proj, total_stocks_proj, prod_stocks_proj, cons_stocks_proj, prod_stocks_proj_spinup, cons_stocks_proj_spinup, consumption_proj, consumption_proj_spinup, harvest_proj, year_sim_proj = TWIST_coreSimple.main(inputfiles, settings, parameters)




    #***********************************************************
    #                       PLOT
    #***********************************************************

    # read in the ending stock
    end_stock_rep = funs.read_in_stock_data(grainEndStock_file, historic_time_period)

    # read in the annual world market price
    WM_price_rep = funs.read_in_price_data(grainPrices_file, CPIfile, historic_time_period, cropYear_month1)

    # read in reported consumption
    Q_out_rep = funs.read_in_consumption_data(consumfileVar, historic_time_period)

    # read in reported production
    Q_harvest_rep = funs.read_in_production_data(producfileVar, historic_time_period)

    stock_to_use = (cons_stocks_spinup+prod_stocks_spinup)/consumption_spinup
    temp = stock_to_use[10:len(stock_to_use)-length_of_scenario]
    stock_to_use_mean = np.mean(stock_to_use[10:len(stock_to_use)-length_of_scenario])
    print('\nHistoric mean S/U=', round(float(stock_to_use_mean), 3))
    print('At scenario start S/U=', round(float(temp[-1]), 3))



    stock_to_use_rep = end_stock_rep/Q_out_rep
    stock_to_use_mean_rep = np.mean(stock_to_use_rep)
    print('\nReported mean S/U=', round(float(stock_to_use_mean_rep), 3))
    print('Reported at scenario start S/U=', round(float(stock_to_use_rep[-1]), 3))



    # fig, axs = plt.subplots(1, 4, figsize=(16, 6))
    #
    # axs[0].set_title(commodity + ' World Market Price ')
    # axs[0].plot(historic_time_period, WM_price_rep, 'k-', label='reported  WM price')
    # axs[0].plot(year_sim, price, 'g-', label='Scenario')
    # axs[0].plot(year_sim_base, price_base, 'b-', label='Baseline')
    # axs[0].set_xlabel('Year')
    # axs[0].set_ylabel('Price (US$/mt)')
    # axs[0].legend()
    #
    # axs[1].set_title(commodity + ' ending stocks ')
    # axs[1].plot(historic_time_period, end_stock_rep, 'k-', label='reported ending stock')
    # axs[1].plot(year_sim, total_stocks, 'b-', label='Scenario')
    # axs[1].plot(year_sim_base, total_stocks_base, 'g-', label='Baseline')
    # axs[1].legend()
    # axs[1].set_ylabel('Quantity (mmt)')
    # axs[1].set_xlabel('Year')
    #
    # axs[2].set_title(commodity + ' consumption ')
    # axs[2].plot(historic_time_period, Q_out_rep, 'k-', label='reported consumption')
    # axs[2].plot(year_sim, consumption, 'b-', label='Scenario')
    # axs[2].plot(year_sim_base, consumption_base, 'g-', label='Baseline')
    # axs[2].legend()
    # axs[2].set_xlabel('Year')
    # axs[2].set_ylabel('Quantity (mmt)')
    #
    # axs[3].set_title(commodity + ' production ')
    # axs[3].plot(historic_time_period, Q_harvest_rep, 'k-', label='reported production')
    # axs[3].plot(year_sim, harvest, 'b-', label='Scenario', alpha=0.6)
    # axs[3].plot(year_sim_base, harvest_base, 'g-', label='Baseline', alpha=0.6)
    # axs[3].legend()
    # axs[3].set_xlabel('Year')
    # axs[3].set_ylabel('Quantity (mmt)')
    #
    # fig.tight_layout(rect=[0, 0.03, 1, 0.9])
    # fig.savefig(savePath+'/Scenario'+commodity+date+'.png')

    fig, axs = plt.subplots(4, 1, figsize=(6, 12), sharex=True)
    start = historic_start_year
    scenario = scenario_start_year-1
    finish = scenario_start_year+length_of_scenario-1
    blue = '#538cc6'
    orange = '#ff8c1a'
    yellow = '#ffd11a'
    red = '#ff5c33'

    axs[0].set_title('Scenario for '+commodity + ' \nwith production failure '+ str(scenario_start_year) + ' lasting '+str(length_of_scenario)+' years', size=14)

    # # theresa, quick fix
    # print(scenario_start_year, type(scenario_start_year))
    # if scenario_start_year == 2019:
    #     print('2019 fix length')
    #     axs[0].plot(historic_time_period[0:-1], WM_price_rep, 'k-', label='Reported')
    # else:
    axs[0].plot(historic_time_period, WM_price_rep, 'k-', label='Reported')
    axs[0].plot(year_sim_proj, price_proj, '-', color='g', label='Sim. with policies')
    axs[0].plot(year_sim, price, '-', color=orange, label='Scenario')
    # axs[0].plot(year_sim_base, price_base, '-', color=blue, label='Baseline')
    axs[0].legend()
    axs[0].set_ylabel('Price (US$/mt)', size=12)
    axs[0].set_xlim([start, finish])
    if commodity == 'Rice_proj':
        axs[0].set_ylim([0, 750])
    axs[0].axvspan(scenario, finish, color='k', alpha=0.15)

    print()
    print('Shock:', scenario_start_year)
    print('Production decline:', prodDecline_dic)
    print('Export restriction', exportRestr_dic)
    print('Consumtion reduction:', consReduction_dic)
    print('Shock length:', year_sim[-5:])
    print('Price scenario', price[-5:])
    # print('Shock length:', year_sim_base[-5:])
    # print('Price baseline:', price_base[-5:])
    print('Shock length:', year_sim_proj[-5:])
    print('Price projection:', price_proj[-5:])

    print('Price scenario/Price projection=', np.array(price[-5:])/np.array(price_proj[-5:]))
    print('2020 Price change scenario/projection=',  np.round(np.array(price[-3])/np.array(price_proj[-3])-1,3)*100)
    print('2020 Price change scenario/2019 price=',  np.round(np.array(price[-3])/np.array(price_proj[-4])-1,3)*100)
    print('Price 2020 =', np.round(np.array(price[-3]),2))
    # print('Price scenario/Price baseline=', np.array(price[-length_of_scenario:])/np.array(price_base[-length_of_scenario:]))




    #axs[1].set_title(commodity + ' ending stocks ')
    axs[1].plot(historic_time_period, end_stock_rep*10**(-6), 'k-')#, label='Reported ending stock')
    #axs[1].plot(year_sim, cons_stocks*10**(-6), '-', color=orange, label='Consumer stocks')
    #axs[1].plot(year_sim_base, prod_stocks*10**(-6), '-', color=red, label='Producer stocks')
    axs[1].plot(year_sim_proj, total_stocks_proj*10**(-6), '-', color='g', label='Projection total')
    axs[1].plot(year_sim, total_stocks*10**(-6), '-', color=orange, label='Scenario total')
    # axs[1].plot(year_sim_base, total_stocks_base*10**(-6), '-', color=blue, label='Baseline total')
    #axs[1].legend()
    axs[1].set_ylabel('Ending stocks (mmt)', size=12)
    axs[1].set_xlim([start, finish])
    axs[1].axvspan(scenario, finish, color='k', alpha=0.15)

    #axs[2].set_title(commodity + ' production ')
    axs[2].plot(historic_time_period, Q_harvest_rep*10**(-6), 'k-', label='Reported production')
    axs[2].plot(year_sim_proj, harvest_proj*10**(-6), '-', color='g', label='Projection', alpha=0.6)
    axs[2].plot(year_sim, harvest*10**(-6), '-', color=orange, label='Scenario', alpha=0.6)
    # axs[2].plot(year_sim_base, harvest_base*10**(-6), '-', color=blue, label='Baseline', alpha=0.6)
    #axs[2].legend()
    axs[2].set_ylabel('Production (mmt)', size=12)
    axs[2].set_xlim([start, finish])
    axs[2].axvspan(scenario, finish, color='k', alpha=0.15)

    #axs[3].set_title(commodity + ' consumption ')
    axs[3].plot(historic_time_period, Q_out_rep*10**(-6), 'k-')#, label='Reported consumption')
    axs[3].plot(year_sim_proj, consumption_proj*10**(-6), '-', color='g')#, label='Scenario')
    axs[3].plot(year_sim, consumption*10**(-6), '-', color=orange)#, label='Scenario')
    # axs[3].plot(year_sim_base, consumption_base*10**(-6), '-', color=blue)#, label='Baseline')
    #axs[2].legend()
    axs[3].set_xlabel('Year', size=13)
    axs[3].set_ylabel('Consumption (mmt)', size=12)
    axs[3].set_xlim([start, finish])
    axs[3].axvspan(scenario, finish, color='k', alpha=0.15)

    fig.tight_layout(rect=[0, 0.02, 1, 0.98])
    fig.savefig(savePath+'/Scenario_Historic'+commodity+'_'+date+'.png')




    fig2 = plt.figure()

    years_with_spinup = np.concatenate((np.arange(year_sim[0]-11, year_sim[0]-1), year_sim))

    plt.title('End Storage')
    plt.plot(years_with_spinup, (cons_stocks_spinup+prod_stocks_spinup)*10**(-6), '-', color=blue, label='Total')
    plt.plot(years_with_spinup, cons_stocks_spinup*10**(-6), '-', color=orange, label='Consumer')
    plt.plot(years_with_spinup, prod_stocks_spinup*10**(-6), '-', color=red, label='Producer')
    plt.plot(historic_time_period, end_stock_rep*10**(-6), 'k-', label='Reported')
    plt.legend()
    plt.xlabel('Year', size=13)
    plt.ylabel('Ending storage (mmt)', size=12)

    fig3 = plt.figure()
    plt.title('Stock to Use ratio')
    plt.plot(years_with_spinup, (cons_stocks_spinup+prod_stocks_spinup)/consumption_spinup, '-', color=blue, label='Total')
    plt.plot(years_with_spinup, cons_stocks_spinup/consumption_spinup, '-', color=orange, label='Consumer')
    plt.plot(years_with_spinup, prod_stocks_spinup/consumption_spinup, '-', color=red, label='Producer')
    plt.plot(historic_time_period, end_stock_rep/Q_out_rep, 'k-', label='reported')


    plt.legend()
    plt.xlabel('Year', size=13)
    plt.ylabel('Stock-to-use (%)', size=12)


    fig_price = plt.figure(figsize=(6, 4))
    if commodity == 'Wheat_proj':
        plt.plot(historic_time_period, WM_price_rep, 'k-', label='US HRW, deflated')
        plt.plot(year_sim_proj, price_proj, '-', color='g', label='Sim with policies')
        plt.plot(year_sim, price, '-', color=orange, label='Scenario')
        print('Wheat')
        print(historic_time_period)
        print(WM_price_rep)
        print(price_proj)
    elif commodity == 'Corn_proj':
        plt.plot(historic_time_period, WM_price_rep, 'k-', label='Maize, deflated')
        plt.plot(year_sim_proj, price_proj, '-', color='g', label='Sim with policies')
        plt.plot(year_sim, price, '-', color=orange, label='Scenario')
        print('Maize')
        print(historic_time_period)
        print(WM_price_rep)
        print(price_proj)
    elif commodity == 'Rice_proj':
        plt.plot(historic_time_period, WM_price_rep, 'k-', label='Rice Thai 5%, deflated')
        plt.plot(year_sim_proj, price_proj, '-', color='g', label='Sim with policies')
        plt.plot(year_sim, price, '-', color=orange, label='Scenario')
        print('Rice')
        print(historic_time_period)
        print(WM_price_rep)
        print(price_proj)
    elif commodity == 'Soybean':
        plt.plot(historic_time_period, WM_price_rep, 'k-', label='Soybean deflated')
        plt.plot(year_sim_proj, price_proj, '-', color='g', label='Sim with policies')
        plt.plot(year_sim, price, '-', color=orange, label='Scenario')
        print('Soybean')
        print(historic_time_period)
        print(WM_price_rep)
        print(price_proj)

    # axs[0].plot(year_sim, price, '-', color=orange, label='Scenario')
    # axs[0].plot(year_sim_base, price_base, '-', color=blue, label='Baseline')
    plt.legend()
    plt.ylabel('Price (US$/mt)', size=12)
    plt.xlim([start, scenario_start_year-1])
    if commodity == 'Rice_proj':
        plt.ylim([0, 750])

    def difference(dataset, lag=1):
        diff = np.empty(len(dataset)-1)
        for i in range(lag, len(dataset)):
            value = dataset[i] - dataset[i - lag]
            diff[i-1] = value
        return diff

    fig5 = plt.figure()
    plt.title('First difference price time series')
    plt.plot(historic_time_period[1::], difference(WM_price_rep), 'k-', label='Reported')
    plt.plot(year_sim_proj[1::], difference(price_proj), '-', color='g', label='Sim. with policies')
    plt.plot(year_sim[1::], difference(price), '-', color=orange, label='Sim.')
    plt.legend()
    plt.ylabel('Difference (US$/mt)', size=12)
    plt.xlim([start+1, scenario_start_year-1])
    plt.show()


    # length = len(year_sim_mean)-length_of_scenario-2
    #
    # fig2, axs2 = plt.subplots(3, 1, figsize=(6, 9), sharex=True)
    #
    # axs2[0].set_title('Scenario for '+commodity + ' \nwith production failure '+ str(scenario_start_year) + ' lasting '+str(length_of_scenario)+' years', size=14)
    # axs2[0].plot(year_sim_mean[length::], price_low[length::], '-', color=yellow)
    # axs2[0].plot(year_sim_mean[length::], price_high[length::], '-', color=red)
    # axs2[0].plot(year_sim_mean[length::], price_mean[length::], '-', color=orange, label='Scenario')
    # axs2[0].plot(year_sim_base[length::], price_base[length::], '-', color=blue, label='Baseline')
    # axs2[0].legend()
    # #axs2[0].xaxis.set_ticklabels([])
    # axs2[0].set_ylabel('Price (US$/mt)', size=12)
    # axs2[0].axvspan(scenario, finish, color='k', alpha=0.15)
    #
    # #axs2[0].set_xlim([scenario-2, finish])
    #
    #
    # axs2[1].plot(year_sim_mean[length::], cons_stocks_low[length::]*10**(-6), '-', color=yellow, label='Low storage')
    # axs2[1].plot(year_sim_mean[length::], cons_stocks_high[length::]*10**(-6), '-', color=red, label='High storage')
    # axs2[1].plot(year_sim_mean[length::], cons_stocks_mean[length::]*10**(-6), '-', color=orange, label='Mean storage')
    # axs2[1].legend()
    # axs2[1].set_ylabel('Consumer stocks (mmt)', size=12)
    # axs2[1].axvspan(scenario, finish, color='k', alpha=0.15)
    #
    #
    # axs2[2].plot(historic_time_period[scenario-3::], Q_harvest_rep[scenario-3::]*10**(-6), 'k-', label='Reported production')
    # axs2[2].plot(year_sim_mean[length::], harvest_mean[length::]*10**(-6), '-', color=orange, label='Scenario', alpha=0.6)
    # axs2[2].plot(year_sim_base[length::], harvest_base[length::]*10**(-6), '-', color=blue, label='Baseline', alpha=0.6)
    # axs2[2].set_ylabel('Production (mmt)', size=12)
    # axs2[2].set_xlabel('Year', size=13)
    # axs2[2].axvspan(scenario, finish, color='k', alpha=0.15)
    # axs2[2].set_xlim([scenario-2, finish])
    # axs2[2].xaxis.set_major_locator(MaxNLocator(integer=True))
    #
    #
    # fig2.tight_layout(rect=[0, 0.02, 1, 0.98])
    # fig2.savefig(savePath+'/Scenario'+commodity+date+'_zoom.png')

if __name__ == '__main__':

    commodity = 'Rice_proj'  # possible folders, Wheat_proj, Rice_proj or Corn_proj
    scenario_mode = False  # 'constant_shock_size_all_years' or 'one_year_shock_with_proj'
    historic_start_year = 1975
    scenario_start_year = 2019
    length_of_scenario = 2  # years
    include_prodDecline = False
    production_decline = {2020:1-0.025}  # 1 => base line scenario, otherwise the decrease 0.9 is a shock of 10%

    include_importStrat = True  # should be ture in order to include policies
    if commodity == 'Wheat_proj':
        importStrat_dic = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2011:1.05, 2012:1.05, 2013:1.05, 2014:1.05, 2015:1.05, 2016:1.05, 2017:1.05, 2018:1.05, 2019:1.05, 2020:1.05, 2021:1.05}   # In covid paper, 80th percentile increase in s/u corresponds to 2020:1.0712
        importStrat_dic = {}
    elif commodity == 'Corn_proj':
        importStrat_dic ={1998: 0.98, 1999:0.94, 2000:0.92, 2001:0.88, 2002:0.85, 2003:0.85, 2004:0.85, 2005:0.85, 2006:0.85, 2011:1.2, 2012:1.2, 2013:1.2, 2014:1.2, 2015:1.2, 2016:1.2, 2017:1.2, 2018:1.2, 2019:1.2, 2020:1.2, 2021:1.2, 2022:1.2} # In covid paper, 80th percentile increase in s/u corresponds to 2020:1.2232
        importStrat_dic = {}
    elif commodity == 'Rice_proj':
        importStrat_dic = {2000:0.98, 2001:0.96, 2002:0.94, 2003:0.92, 2004:0.92, 2005:0.92, 2006:0.92, 2015:1.02, 2016:1.02, 2017:1.03, 2018:1.04, 2019:1.05, 2020:1.05, 2021:1.05, 2022:1.05}  # In covid paper, 80th percentile increase in s/u corresponds to 2020:1.0677
    elif commodity == 'Soybean':
        # importStrat_dic ={1998: 0.98, 1999:0.94, 2000:0.92, 2001:0.88, 2002:0.85, 2003:0.85, 2004:0.85, 2005:0.85, 2006:0.85, 2011:1.2, 2012:1.2, 2013:1.2, 2014:1.2, 2015:1.2, 2016:1.2, 2017:1.2, 2018:1.2, 2019:1.2, 2020:1.2, 2021:1.2, 2022:1.2} # In covid paper, 80th percentile increase in s/u corresponds to 2020:1.2232
        importStrat_dic = {}
    else:
        importStrat_dic = {}

    include_exportRestr = False
    exportRestr_dic = {2020:1-0.019} #{2020:1-0.167} #{2020: 1-0.132}#{2007:0.95, 2010:0.92, 2020: 1-0.2} #{2019: 1-0.13}
    include_consReduction = False
    consumption_reduced ={2020:1-0.009} #{2020:1-0.077}# {2020: 1-0.076}#{2020:1-0.117}#{2019: 1-0.077, 2020: 1-0.077}  # 1 => base line scenario, no decrease
    consumption_fixed_reported = True  # no smoothing


    main(commodity, scenario_mode, historic_start_year, scenario_start_year, length_of_scenario, include_prodDecline, production_decline, include_importStrat, importStrat_dic, include_exportRestr, exportRestr_dic, include_consReduction, consumption_reduced, consumption_fixed_reported)
