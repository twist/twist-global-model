import numpy as np
import os


def inputFiles(commodity, dataPath):
    """Define input files for TWIST model.
    """

    # Input directory
    #  dataPath='/Users/puma/Dropbox/Schewe/TWIST_Input'

    # For Wheat:
    if commodity == 'Wheat' or commodity == 'wheat':

        # ### USDA psd world totals EXTENDED to 2016:
        # producfileVar = dataPath + '/psd_wheat_production_world_1960to2016.csv'
        # consumfileVar = dataPath + '/psd_wheat_domesticConsumption_world_1960to2016.csv'
        # iso3file = dataPath + '/World_country_codes.csv'
        # harvDatefile = dataPath + '/World_Harvest_Dates_Wheat_nohead.csv'
        #
        # # grainStorage_file = dataPath + '\psd_update_jan2017\psd_wheat_beginningStocks_world_1960to2016.csv'
        # grainStorage_file = dataPath + '/psd_wheat_beginningStocks_world_1960to2017.csv'
        # ### U.S. consumer price index:
        # # CPIfile = dataPath + '\US_BLS_ConsumerPriceIndex_Annual_1965to2015.csv'
        # CPIfile = dataPath + '/US_BLS_ConsumerPriceIndex_Annual_1965to2016.csv'
        # grainPrices_file = dataPath + '/monthlyNominalGrainPrivesWB_WheatUSHRW_1960to2016.csv'

        # USDA psd world totals:
        producfileVar = dataPath + '/Wheat/psd_wheat_production_world_1960to2019.csv'
        consumfileVar = dataPath + '/Wheat/psd_wheat_domesticConsumption_world_1960to2019.csv'
        grainStorage_file = dataPath + '/Wheat/psd_wheat_beginningStocks_world_1960to2019.csv'
        grainEndStorage_file = dataPath + '/Wheat/psd_wheat_endingStocks_world_1960to2018.csv'


        iso3file = dataPath + '/Wheat/World_country_codes.csv'
        harvDatefile = dataPath + '/Wheat/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Wheat/US_BLS_ConsumerPriceIndex_Annual_1960to2018.csv'
        grainPrices_file = dataPath + '/Wheat/monthlyNominalGrainPrivesWB_WheatUSHRW_1960to2016.csv'

    # For Corn:
    elif commodity == 'Corn' or commodity == 'corn':
        # USDA psd world totals:
        producfileVar = dataPath + '/Corn/psd_corn_production_world_1960to2019.csv' # MT (metric ton)
        consumfileVar = dataPath + '/Corn/psd_corn_domesticConsumption_world_1960to2019.csv' # MT
        grainStorage_file = dataPath + '/Corn/psd_corn_beginningStocks_world_1960to2019.csv' # 1000 MT
        grainEndStorage_file = dataPath + '/Corn/psd_corn_endingStocks_world_1960to2019.csv' # 1000 MT

        iso3file = dataPath + '/Corn/World_country_codes.csv'
        harvDatefile = dataPath + '/Corn/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Corn/US_BLS_ConsumerPriceIndex_Annual_1960to2018.csv'
        grainPrices_file = dataPath + '/Corn/monthlyNominalGrainPricesWB_Corn_1960to2018.csv' # US$/MT

    # For Rice:
    elif commodity == 'Rice' or commodity == 'rice':
        # USDA psd world totals:
        producfileVar = dataPath + '/Rice/psd_rice_production_world_1960to2019.csv' # MT (metric ton)
        consumfileVar = dataPath + '/Rice/psd_rice_domesticConsumption_world_1960to2019.csv' # MT
        grainStorage_file = dataPath + '/Rice/psd_rice_beginningStocks_world_1960to2019.csv' # 1000 MT
        grainEndStorage_file = dataPath + '/Rice/psd_rice_endingStocks_world_1960to2019.csv' # 1000 MT

        iso3file = dataPath + '/Rice/World_country_codes.csv'
        harvDatefile = dataPath + '/Rice/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Rice/US_BLS_ConsumerPriceIndex_Annual_1960to2018.csv'
        grainPrices_file = dataPath + '/Rice/monthlyNominalGrainPrivesWB_Rice5procent_1960to2018.csv' # US$/MT

    # For Sorghum:
    elif commodity == 'Sorghum' or commodity == 'sorghum':
        # USDA psd world totals:
        producfileVar = dataPath + '/Sorghum/psd_sorghum_production_world_1960to2019.csv' # MT (metric ton)
        consumfileVar = dataPath + '/Sorghum/psd_sorghum_domesticConsumption_world_1960to2019.csv' # MT
        grainStorage_file = dataPath + '/Sorghum/psd_sorghum_beginningStocks_world_1960to2019.csv' # 1000 MT
        grainEndStorage_file = dataPath + '/Sorghum/psd_sorghum_endingStocks_world_1960to2019.csv' # 1000 MT

        iso3file = dataPath + '/Sorghum/World_country_codes.csv'
        harvDatefile = dataPath + '/Sorghum/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Sorghum/US_BLS_ConsumerPriceIndex_Annual_1960to2018.csv'
        grainPrices_file = dataPath + '/Sorghum/monthlyNominalGrainPrivesWB_Sorghum_1960to2018.csv' # US$/MT

    elif commodity == 'Sugar' or commodity == 'sugar':
        # USDA psd world totals:
        producfileVar = dataPath + '/Sugar/psd_sugar_production_world_1960to2019.csv' # MT (metric ton)
        consumfileVar = dataPath + '/Sugar/psd_sugar_domsticConsumption_world_1960to2019.csv' # MT
        grainStorage_file = dataPath + '/Sugar/psd_sugar_beginningStocks_world_1960to2019.csv' # 1000 MT
        grainEndStorage_file = dataPath + '/Sugar/psd_sugar_endingStocks_world_1960to2019.csv' # 1000 MT

        iso3file = dataPath + '/Sugar/World_country_codes.csv'
        harvDatefile = dataPath + '/Sugar/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Sugar/US_BLS_ConsumerPriceIndex_Annual_1960to2018.csv'
        grainPrices_file = dataPath + '/Sugar/monthlyNominalGrainPrivesWB_Sugar_1960to2018.csv' # US$/MT

    elif commodity == 'Soybean_meal' or commodity == 'soybean_meal':
        # USDA psd world totals:
        producfileVar = dataPath + '/Soybean_meal/psd_soybeans_production_world_1964to2019.csv' # MT (metric ton)
        consumfileVar = dataPath + '/Soybean_meal/psd_soybeans_domsticConsumption_world_1964to2019.csv' # MT
        grainStorage_file = dataPath + '/Soybean_meal/psd_soybeans_beginningStocks_world_1964to2019.csv' # 1000 MT
        grainEndStorage_file = dataPath + '/Soybean_meal/psd_soybeans_endingStocks_world_1964to2019.csv' # 1000 MT

        iso3file = dataPath + '/Soybean_meal/World_country_codes.csv'
        harvDatefile = dataPath + '/Soybean_meal/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Soybean_meal/US_BLS_ConsumerPriceIndex_Annual_1960to2018.csv'
        grainPrices_file = dataPath + '/Soybean_meal/monthlyNominalGrainPrivesWB_Soybeans_1960to2018.csv' # US$/MT

    elif commodity == 'Soybean' or commodity == 'soybean' or commodity == 'Soybeans' or commodity == 'soybeans':
        # USDA psd world totals:
        producfileVar = dataPath + '/Soybean/psd_soybeans_production_world_1964to2020.csv' # MT (metric ton)
        consumfileVar = dataPath + '/Soybean/psd_soybeans_domsticConsumption_world_1964to2020.csv' # MT
        grainStorage_file = dataPath + '/Soybean/psd_soybeans_beginningStocks_world_1964to2020.csv' # 1000 MT
        grainEndStorage_file = dataPath + '/Soybean/psd_soybeans_endingStocks_world_1964to2020.csv' # 1000 MT

        iso3file = dataPath + '/Soybean/World_country_codes.csv'
        harvDatefile = dataPath + '/Soybean/World_Harvest_Dates_Wheat_nohead.csv'

        # U.S. consumer price index:
        CPIfile = dataPath + '/Soybean/US_BLS_ConsumerPriceIndex_Annual_1960to2020.csv'
        grainPrices_file = dataPath + '/Soybean/monthlyNominalGrainPrivesWB_Soybeans_1960to2020.csv' # US$/MT
    # For Wheat:
    elif commodity == 'Wheat_proj' or commodity == 'wheat_proj':

        # USDA psd world totals with FAO projection extended 2019-2022:
        producfileVar = dataPath + '/USDA_PSD/Wheat/Wheat_2019_proj/psd_wheat_production_world_1960to2022.csv'
        consumfileVar = dataPath + '/USDA_PSD/Wheat//Wheat_2019_proj/psd_wheat_domesticConsumption_world_1960to2022.csv'
        grainStorage_file = dataPath + '/USDA_PSD/Wheat//Wheat_2019_proj/psd_wheat_beginningStocks_world_1960to2022.csv'
        grainEndStorage_file = dataPath + '/USDA_PSD/Wheat//Wheat_2019_proj/psd_wheat_endingStocks_world_1960to2022.csv'
        iso3file = dataPath + '/World_country_codes.csv'
        harvDatefile = dataPath + '/World_Harvest_Dates_Wheat_nohead.csv'
        # U.S. consumer price index:
        CPIfile = dataPath + '/US_CPI/US_BLS_ConsumerPriceIndex_Annual_1960to2019.csv'
        grainPrices_file = dataPath + '/WB_grain_price/monthlyNominalGrainPrivesWB_WheatUSHRW_1960to2019.csv'

    elif commodity == 'Rice_proj' or commodity == 'rice_proj':
        # USDA psd world totals with FAO projection extended 2019-2022:
        producfileVar = dataPath + '/USDA_PSD/Rice/Rice_2019_proj/psd_rice_production_world_1960to2022.csv'
        consumfileVar = dataPath + '/USDA_PSD/Rice/Rice_2019_proj/psd_rice_domesticConsumption_world_1960to2022.csv'
        grainStorage_file = dataPath + '/USDA_PSD/Rice/Rice_2019_proj/psd_rice_beginningStocks_world_1960to2022.csv'
        grainEndStorage_file = dataPath + '/USDA_PSD/Rice/Rice_2019_proj/psd_rice_endingStocks_world_1960to2022.csv'
        iso3file = dataPath + '/World_country_codes.csv'
        harvDatefile = dataPath + '/World_Harvest_Dates_Wheat_nohead.csv'
        # U.S. consumer price index:
        CPIfile = dataPath + '/US_CPI/US_BLS_ConsumerPriceIndex_Annual_1960to2019.csv'
        grainPrices_file = dataPath + '/WB_grain_price/monthlyNominalGrainPrivesWB_Rice5procent_1960to2019.csv'

    elif commodity == 'Corn_proj' or commodity == 'corn_proj':
        # USDA psd world totals with FAO projection extended 2019-2022:
        producfileVar = dataPath + '/USDA_PSD/Corn/Corn_2019_proj/psd_corn_production_world_1960to2022.csv'
        consumfileVar = dataPath + '/USDA_PSD/Corn/Corn_2019_proj/psd_corn_domesticConsumption_world_1960to2022.csv'
        grainStorage_file = dataPath + '/USDA_PSD/Corn/Corn_2019_proj/psd_corn_beginningStocks_world_1960to2022.csv'
        grainEndStorage_file = dataPath + '/USDA_PSD/Corn/Corn_2019_proj/psd_corn_endingStocks_world_1960to2022.csv'
        iso3file = dataPath + '/World_country_codes.csv'
        harvDatefile = dataPath + '/World_Harvest_Dates_Wheat_nohead.csv'
        # U.S. consumer price index:
        CPIfile = dataPath + '/US_CPI/US_BLS_ConsumerPriceIndex_Annual_1960to2019.csv'
        grainPrices_file = dataPath + '/WB_grain_price/monthlyNominalGrainPricesWB_Maize_1960to2019.csv'
    else:
        'WARNING: Chosen commodity is not implemented'
    return producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStorage_file