#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import warnings
import argparse


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(
    description='program to run TWIST with production decline, export restrictions and/or increased consumer demand')
parser.add_argument(
    '--scenario_type', type=str, default='covid19_scenario',
    help="Choose which simulation to run 'covid_scenario' ")
parser.add_argument(
    '--crop', type=str, default='Wheat',
    help="Choose commodity type current options 'wheat', 'rice' or 'maize'")
parser.add_argument(
    '--start_year', type=int, default=1975,
    help='Start of historical time point 1975-2019 possible')
parser.add_argument(
    '--end_year', type=int, default=2022,
    help='end year of simulation, 2019 for historical, 2022 for forecast or production_shock')
parser.add_argument(
    '--scenario_start_year', type=int, default=2020,
    help='only applicable for the production_shock_scenario. This is the year which the production decline is applied to')
parser.add_argument(
    '--shock_severity', type=str, default='moderate',
    help="Strength of production shock option 'moderate', 'severe', 'extreme' or 'baseline', for the baseline, no shock is applied")
parser.add_argument(
    '--exportRestr_countries', type=str, default='none',
    help="For wheat: 'RUS', 'UKR', 'KAZ', 'RUS,UKR' or ''RUS,UKR,KAZ'. For rice: 'VNM', 'THA', 'IND', 'VNM,THA' or 'VNM,THA,IND'. For corn: 'UKR', 'ARG', 'BRA', 'UKR,ARG' or 'UKR,ARG,BRA', or 'none'")
parser.add_argument(
    '--include_prodDecline',  type=str2bool, default=False, help="Run TWIST with a 20th production decline in export countries and 20th production decline in locust threatened countries")
parser.add_argument(
    '--include_importStrat', type=str2bool, default=False, help="Run TWIST with a 80th percentil increase in world stock-to-use, to estimate the effect of increased consumer import")

# Get agents input data and parameters
args = parser.parse_args()

if args.crop == 'Wheat' or args.crop == 'wheat':
    crop = 'Wheat'
elif args.crop == 'Rice' or args.crop == 'rice':
    crop = 'Rice'
elif args.crop == 'Maize' or args.crop == 'maize' or args.crop == 'Corn' or args.crop == 'corn':
    crop = 'Corn'
else:
    warnings.warn("TWIST is not calibrated for {} (try 'Wheat', 'Rice' or 'Corn)".format(args.crop))
    sys.exit(1)

if args.scenario_type == 'covid19_scenario':
    TWIST_path = os.path.join(os.getcwd())
    if TWIST_path == '/twist':
        TWIST_path = '/twist/main'


    os.system("cd {}".format(TWIST_path))
    os.system("python {}/run_TWIST_covid19.py --crop {} "
              "--exportRestr_countries {} --include_prodDecline {} --include_importStrat {}".format(TWIST_path,crop,
                                                                  args.exportRestr_countries,
                                                                  args.include_prodDecline,
                                                                  args.include_importStrat))
else:
    warnings.warn("covid19_scenario is currently the only available scenario_type option)".format(args.crop))
    sys.exit(1)
