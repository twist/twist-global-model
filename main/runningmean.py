import numpy as np

#####################
# Simple running mean
# in window of size
# 2*halfwin + 1
# jacob.schewe
#####################

def runmean(data, halfwin):
  """
  Simple running mean. 
  CAUTION: Data is *extended* at the edges by repeating the 
  edge values; thereby any trend present in the data will 
  become attenuated at the edges!
  """
  window = 2*halfwin + 1
  if window > len(data): 
    print('Error: window too large!')
    import sys
    sys.exit(0)
  weights = np.repeat(1.0, window) / window
  ### Treat edges: Extend data
  extended_data = np.hstack([[data[0]] * (halfwin), data, [data[len(data)-1]] * (halfwin)])
  rm = np.convolve(extended_data, weights, 'valid')
  return rm

def runmean_matchEdges(data, halfwin):
  """
  Simple running mean. 
  CAUTION: Running window gets *narrower* toward the edges; 
  edge values are retained exactly, therefore the resulting 
  data series may be particularly curved at the edges! 
  """
  window = 2*halfwin + 1
  if window > len(data): 
    print('Error: window too large!')
    import sys
    sys.exit(0)
  weights = np.repeat(1.0, window) / window
  rm_mid = np.convolve(data, weights, 'valid')
  ### Treat edges: Window gets narrower
  edge_count = np.arange(0, halfwin)
  rm_start = np.zeros_like(edge_count, dtype=np.float)
  rm_end = np.zeros_like(edge_count, dtype=np.float)
  for i in edge_count: 
    ewindow = 2*i + 1
    rm_start[i] = np.mean(data[0:ewindow])
    rm_end[halfwin-(i+1)] = np.mean(data[-ewindow:])
  rm = np.append(np.append(rm_start, rm_mid), rm_end)
  return rm

def runmean_matchLeftEdge(data, halfwin):
  """
  Combination of runmean and runmean_matchEdges. 
  CAUTION: Running window gets *narrower* toward the left edge; 
  edge values are retained exactly, therefore the resulting 
  data series may be particularly curved at the left edge! 
  Right edge: Data is *extended* at the edges by repeating the 
  edge values; thereby any trend present in the data will 
  become attenuated at the edges! 
  """

  window = 2*halfwin + 1
  if window > len(data): 
    print('Error: window too large!')
    import sys
    sys.exit(0)
  weights = np.repeat(1.0, window) / window
  extended_data = np.hstack([data, [data[len(data)-1]] * (halfwin)])
  rm_mid = np.convolve(extended_data, weights, 'valid')
  ### Treat left edge: Window narrows symmetrically. 
  edge_count = np.arange(0, halfwin)
  rm_start = np.zeros_like(edge_count, dtype=np.float)
  for i in edge_count: 
    ewindow = 2*i + 1
    rm_start[i] = np.mean(data[0:ewindow])
  rm = np.append(rm_start, rm_mid)
  return rm
