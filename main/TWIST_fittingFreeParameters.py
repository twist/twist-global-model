import numpy as np
from scipy.optimize import least_squares
import os
import imp
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import TWIST_coreSimple
import TWIST_functions
from scipy.stats import pearsonr
from datetime import datetime

def main(commodity, time_period, cropYear_month1, initial_guess, lo_bound, up_bound, grid_steps, q_storage_max_cons_type, q_out_offset_type):
    date = datetime.now().strftime('%Y-%m-%d %H:%M')

    print('Fit free parameter')
    print('Commodity:', commodity)
    print('Time period:', time_period[0], 'to', time_period[-1])

    workdir = os.path.dirname(os.path.realpath(__file__))
    module_inputfiles = str(workdir + '/TWIST_inputfiles.py')
    module_settings = str(workdir + '/TWIST_settings.py')

    inputfiles = imp.load_source('inputfiles', module_inputfiles)
    settings = imp.load_source('settings', module_settings)

    dataPath = '{0}/{1}'.format(settings.localbasedir, 'input')
    savePath = '{0}/{1}/{2}'.format(settings.localbasedir, 'fitting_result', commodity+' '+date)

    # create dictionary where the fitting results will be saved
    os.mkdir(savePath)

    # Log file:
    fo = open(savePath+'/Fitting.txt', 'w')
    print(date, file=fo)
    print('Commodity:', commodity, file=fo)
    print('Time period:', time_period[0], 'to', time_period[-1], file=fo)
    print('cropYear_month1 =', cropYear_month1, file=fo)
    print('q_storage_max_cons_type =', q_storage_max_cons_type, file=fo)
    print('q_out_offset_type =', q_out_offset_type, file=fo)


    # get the name and path of the input file for the selected commodity
    producfileVar, consumfileVar, iso3file, harvDatefile, CPIfile, grainPrices_file, grainStorage_file, grainEndStock_file = inputfiles.inputFiles(
        commodity,
        dataPath)

    funs = TWIST_functions.Functions(settings.n_price_units)


    # The initial guess of the free parameters used for determining the storage offset level, need to run funs.fit_end_storage
    # [alpha_s, alpha_d, p_max_prod_const, p_max_cons_base, q_storage_max_cons_factor, e_d]
    param_fixed_value = [initial_guess[0], initial_guess[1], initial_guess[2]*10**8, initial_guess[3]*10**8, initial_guess[4], -0.1]

    # Get the initial producer storage level form the beginning stock and fit the consumption offset parameter for the fix model
    q_out_offset, loss_factor, q_storage_init_prod = funs.fit_end_storage(commodity, time_period, grainStorage_file, grainEndStock_file, producfileVar, consumfileVar, param_fixed_value, q_storage_max_cons_type, q_out_offset_type, savePath, make_plot=True)

    print(file=fo)
    print('Fitting the storage offset value and getting the initial storage level with fit_end_storage()', file=fo)
    print('q_out_offset =', q_out_offset, file=fo)
    print('loss_factor =', loss_factor, file=fo)
    print('q_storage_init_prod =', q_storage_init_prod, file=fo)

    # read in the ending stock
    end_stock_rep = funs.read_in_stock_data(grainEndStock_file, time_period)

    # read in the annual world market price
    WM_price_rep = funs.read_in_price_data(grainPrices_file, CPIfile, time_period, cropYear_month1)

    # read in reported consumption
    Q_out_rep = funs.read_in_consumption_data(consumfileVar, time_period)

    # read in reported production
    Q_harvest_rep = funs.read_in_production_data(producfileVar, time_period)



    # get the looping values
    steps_alpha_s, steps_alpha_d, steps_P_max_prod, steps_P_max_cons, steps_I_max_cons = funs.leastsq_grid_steps(lo_bound, up_bound, initial_guess, grid_steps)

    print()
    print('Grid of starting values:')
    print(steps_alpha_s)
    print(steps_alpha_d)
    print([x*10**8 for x in steps_P_max_prod])
    print([x*10**8 for x in steps_P_max_cons])
    print(steps_I_max_cons)

    print(file=fo)
    print('Free param: [alpha_s, alpha_d, P_max_prod, P_max_cons, I_max_cons]', file=fo)
    print('initial guess:', initial_guess, file=fo)
    print('lower boundary:', lo_bound, file=fo)
    print('upper boundary:', up_bound, file=fo)


    print(file=fo)
    print('Grid of starting values:', file=fo)
    print('alpha_s:', steps_alpha_s, file=fo)
    print('alpha_d: ', steps_alpha_d, file=fo)
    print('P_max_prod:', [x*10**8 for x in steps_P_max_prod], file=fo)
    print('P_max_cons:', [x*10**8 for x in steps_P_max_cons], file=fo)
    print('I_max_cons:', steps_I_max_cons, file=fo)


    def model_fixCon(years, free_param):

        alpha, beta, price_producer, price_consumer, q_storage_max_cons_factor,  = free_param

        # specifics to run fix model
        production_varying = 1
        consumption_varying = 1
        endogFinalDemand = 0
        filtersize = 0

        # fix parameter value
        elast_finalDemand = -0.1
        # alpha = 0.1

        # fit without scenario
        scenario_mode = False #'one_year_shock_with_proj' #False
        scenario_start_year = 2017
        production_decline = 1
        length_of_scenario = 2
        include_prodDecline = False
        prodDecline_dic = {}
        include_importStrat = False
        importStrat_dic = {}
        include_exportRestr = False
        exportRestr_dic = {} #{2019: 1 - 0.1323}
        include_consReduction = False
        consReduction_dic = {}
        consumption_fixed_reported = True  # no smoothing

        # the parameter needed to run the fitting routine
        fitting_param = [commodity, years[0], years[0], q_storage_init_prod, production_varying, consumption_varying, endogFinalDemand,
                         filtersize, q_out_offset, loss_factor, alpha, beta, price_producer*10**8, price_consumer*10**8, q_storage_max_cons_factor, elast_finalDemand,
                         scenario_mode, scenario_start_year, length_of_scenario, include_prodDecline, prodDecline_dic,  include_importStrat, importStrat_dic,include_exportRestr, exportRestr_dic, include_consReduction, consReduction_dic,
                        consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

        # Run the TWIST model
        price_sim, storage_sim, prod_stocks, cons_stocks, prod_stocks_spinup, cons_stocks_spinup, consumption_sim, \
        consumption_spinup, harvest_rep, year_sim = TWIST_coreSimple.main(module_inputfiles,
                                                                                               module_settings,
                                                                                               fitting_param)

        # Select only the years which are overlapping with the reported data (time_period) of world market prices using
        index_of_years = np.where(np.isin(year_sim, years))

        WM_price_sim = np.array([price_sim[i] for i in index_of_years[0]]).flatten()
        end_stock_sim = np.array([storage_sim[i] for i in index_of_years[0]]).flatten()
        end_stock_prod_sim = np.array([prod_stocks[i] for i in index_of_years[0]]).flatten()
        end_stock_cons_sim = np.array([cons_stocks[i] for i in index_of_years[0]]).flatten()
        Q_out_sim = np.array([consumption_sim[i] for i in index_of_years[0]]).flatten()
        Q_harvest_sim = np.array([harvest_rep[i] for i in index_of_years[0]]).flatten()


        # return WM_price_sim, end_stock_sim, Q_out_sim, Q_harvest_sim, prod_stocks_spinup, cons_stocks_spinup, year_sim

        return WM_price_sim, end_stock_sim, end_stock_prod_sim, end_stock_cons_sim, Q_out_sim, Q_harvest_sim

    def model_flexCon(years, free_param):

        alpha, beta, price_producer, price_consumer, q_storage_max_cons_factor = free_param

        # specifics to run flex model
        production_varying = 1
        consumption_varying = 1
        endogFinalDemand = 1
        filtersize = 5


        # fix parameter value
        elast_finalDemand = -0.1
        # alpha = 0.1

        # fit without scenario
        scenario_mode = False  #'one_year_shock_with_proj' #False
        scenario_start_year = 2017
        length_of_scenario = 1
        include_prodDecline = False
        prodDecline_dic = {}
        include_importStrat = False
        importStrat_dic = {}
        include_exportRestr = False
        exportRestr_dic = {}
        include_consReduction = False
        consReduction_dic = {}
        consumption_fixed_reported = False  # no smoothing

        # the parameter needed to run the fitting routine
        fitting_param = [commodity, years[0], years[0], q_storage_init_prod, production_varying, consumption_varying, endogFinalDemand,
                         filtersize, q_out_offset, loss_factor, alpha, beta, price_producer*10**(8), price_consumer*10**(8), q_storage_max_cons_factor, elast_finalDemand,
                         scenario_mode, scenario_start_year, length_of_scenario, include_prodDecline, prodDecline_dic,  include_importStrat, importStrat_dic, include_exportRestr, exportRestr_dic,  include_consReduction, consReduction_dic,
                        consumption_fixed_reported, q_storage_max_cons_type, q_out_offset_type]

        # Run the TWIST model
        price_sim, storage_sim, prod_stocks, cons_stocks, prod_stocks_spinup, cons_stocks_spinup, consumption_sim, \
        consumption_spinup, harvest_rep, year_sim = TWIST_coreSimple.main(module_inputfiles,
                                                                                               module_settings,
                                                                                               fitting_param)

        # Select only the years which are overlapping with the reported data (time_period) of world market prices using
        index_of_years = np.where(np.isin(year_sim, years))

        WM_price_sim = np.array([price_sim[i] for i in index_of_years[0]]).flatten()
        end_stock_sim = np.array([storage_sim[i] for i in index_of_years[0]]).flatten()
        end_stock_prod_sim = np.array([prod_stocks[i] for i in index_of_years[0]]).flatten()
        end_stock_cons_sim = np.array([cons_stocks[i] for i in index_of_years[0]]).flatten()
        Q_out_sim = np.array([consumption_sim[i] for i in index_of_years[0]]).flatten()
        Q_harvest_sim = np.array([harvest_rep[i] for i in index_of_years[0]]).flatten()


        # return WM_price_sim, end_stock_sim, Q_out_sim, Q_harvest_sim, prod_stocks_spinup, cons_stocks_spinup, year_sim
        return WM_price_sim, end_stock_sim, end_stock_prod_sim, end_stock_cons_sim, Q_out_sim, Q_harvest_sim

    def residuals(free_param, WM_price, ending_stock, years):

        # WM_price_sim_fix, end_stock_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix, Q_prod_sim_fix, Q_cons_sim_fix, year_sim_fix = model_fixCon(years, free_param)
        # WM_price_sim_flex, end_stock_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex, Q_prod_sim_flex, Q_cons_sim_flex, year_sim_flex = model_flexCon(years, free_param)

        WM_price_sim_fix, end_stock_sim_fix, end_stock_prod_sim_fix, end_stock_cons_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix= model_fixCon(years, free_param)
        WM_price_sim_flex, end_stock_sim_flex, end_stock_prod_sim_flex, end_stock_cons_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex = model_flexCon(years, free_param)

        # Returns the difference between your model and the observed data
        return np.r_[WM_price - WM_price_sim_fix, (ending_stock - end_stock_sim_flex)/(0.2 * end_stock_rep),
                     WM_price - WM_price_sim_flex]
        #return np.r_[WM_price - WM_price_sim_fix, ending_stock - end_stock_sim_fix]

    initial_values = []
    number_eval = []
    end_status = []
    best_fit = []
    cost_value = []
    chi2 = []

    print()
    print('Start leastsq fitting: initialised with a spread in initial guesses')
    number_runs = len(steps_alpha_s)*len(steps_alpha_d)*len(steps_P_max_prod)*len(steps_P_max_cons)*len(steps_I_max_cons)
    print('Number of runs for leastsq:', number_runs)
    print(file=fo)
    print('Number of runs for least square loop:', number_runs, file=fo)

    def progress(amtDone):
        #print("\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(amtDone * 50), amtDone * 100), end="", flush=True)
        print("\rProgress: [{0:50s}] {1:.1f}%".format('█' * int(amtDone * 50), amtDone * 100), end="", flush=True)



    # initiate the progress bar
    progress(0/(number_runs))
    j = 1  # counter for the progress bar

    # start the fitting, loop over all whole grid of starting values
    for alpha_s in steps_alpha_s:
        for alpha_d in steps_alpha_d:
            for p_max_prod in steps_P_max_prod:
                for p_max_cons in steps_P_max_cons:
                    for i_max_cons in steps_I_max_cons:

                        # Run fitting
                        solution = least_squares(residuals, [alpha_s, alpha_d, p_max_prod, p_max_cons, i_max_cons],
                                                 args=(WM_price_rep, end_stock_rep, time_period),
                                                 bounds=(lo_bound, up_bound))
                        # print(alpha_s, alpha_d, p_max_prod, p_max_cons, i_max_cons)
                        # print(solution.x)
                        # print(solution.cost)

                        # WM_price_sim_fix, end_stock_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix, Q_prod_sim_fix, Q_cons_sim_fix, year_sim_fix = model_fixCon(
                        #     time_period, solution.x)
                        # WM_price_sim_flex, end_stock_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex, Q_prod_sim_flex, Q_cons_sim_flex, year_sim_flex = model_flexCon(
                        #     time_period, solution.x)

                        WM_price_sim_fix,end_stock_sim_fix, end_stock_prod_sim_fix, end_stock_cons_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix = model_fixCon(
                            time_period, solution.x)
                        WM_price_sim_flex, end_stock_sim_flex, end_stock_prod_sim_flex, end_stock_cons_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex= model_flexCon(
                            time_period, solution.x)

                        chi2_FlexP = sum(((WM_price_sim_flex - WM_price_rep) / (0.1 * WM_price_rep)) ** 2)
                        chi2_FixP = sum(((WM_price_sim_fix - WM_price_rep) / (0.1 * WM_price_rep)) ** 2)
                        chi2_FlexS = sum(((end_stock_sim_flex - end_stock_rep) / (0.2 * end_stock_rep)) ** 2)

                        chi2.append([chi2_FlexP, chi2_FixP, chi2_FlexS])

                        # save info about each run so it is possible to determine the best fit after the fitting is done
                        initial_values.append([alpha_s, alpha_d, p_max_prod, p_max_cons, i_max_cons])
                        cost_value.append(solution.cost)
                        best_fit.append(solution.x)
                        number_eval.append(solution.nfev)
                        end_status.append(solution.status)

                        # update the printout to the progress bar
                        progress(j/number_runs)

                        j += 1


    print()
    #print('Fitting done')

    # select the best fit by the model with has minimum cost function value
    lowest_cost = np.argmin(cost_value)
    lowest_chi2 = np.argmin(np.sum(chi2, axis=1))
    print('lowest cost run number:', lowest_cost)
    print('lowest chi2 run number:', lowest_chi2)

    print(file=fo)
    print('lowest cost run number:', lowest_cost, file=fo)
    print('lowest chi2 run number:', lowest_chi2, file=fo)

    # ****************************************************
    #                Plot Lowest Chi2
    # ****************************************************

    # WM_price_sim_fix, end_stock_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix, Q_prod_sim_fix, Q_cons_sim_fix, year_sim_fix = model_fixCon(time_period, best_fit[lowest_chi2])
    # WM_price_sim_flex, end_stock_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex, Q_prod_sim_flex, Q_cons_sim_flex, year_sim_flex = model_flexCon(time_period, best_fit[lowest_chi2])


    WM_price_sim_fix, end_stock_sim_fix, end_stock_prod_sim_fix, end_stock_cons_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix = model_fixCon(time_period, best_fit[lowest_chi2])
    WM_price_sim_flex, end_stock_sim_flex, end_stock_prod_sim_flex, end_stock_cons_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex = model_flexCon(time_period, best_fit[lowest_chi2])

    # calculate the pearson correlation coefficient
    R_price_fix = pearsonr(WM_price_sim_fix, WM_price_rep)
    R_stock_fix = pearsonr(end_stock_sim_fix, end_stock_rep)
    R_consu_fix = pearsonr(Q_out_sim_fix, Q_out_rep)
    R_harve_fix = pearsonr(Q_harvest_sim_fix, Q_harvest_rep)


    R_price_flex = pearsonr(WM_price_sim_flex, WM_price_rep)
    R_stock_flex = pearsonr(end_stock_sim_flex, end_stock_rep)
    R_consu_flex = pearsonr(Q_out_sim_flex, Q_out_rep)
    R_harve_flex = pearsonr(Q_harvest_sim_flex, Q_harvest_rep)

    fig, axs = plt.subplots(1, 4, figsize=(16, 6))

    axs[0].set_title(commodity + ' World Market Price ')
    axs[0].plot(time_period, WM_price_rep, 'k-', label='reported  WM price')
    axs[0].plot(time_period, WM_price_sim_fix, 'b-', label='Fixcons: Fitted, r=' + str(round(R_price_fix[0], 3)))
    axs[0].plot(time_period, WM_price_sim_flex, 'g-', label='Flexcons: Fitted, r=' + str(round(R_price_flex[0], 3)))
    axs[0].set_xlabel('Year')
    axs[0].set_ylabel('Price (US$/mt)')
    axs[0].legend()

    # axs[1].set_title(commodity + ' ending stocks ')
    # axs[1].plot(time_period, end_stock_rep, 'k-', label='reported ending stock')
    # axs[1].plot(time_period, end_stock_sim_fix, 'b-', label='FixCons: Fitted r=' + str(round(R_stock_fix[0], 3)))
    # axs[1].plot(time_period, end_stock_sim_flex, 'g-', label='FlexCons: Not fitted r=' + str(round(R_stock_flex[0], 3)))
    # axs[1].legend()
    # axs[1].set_ylabel('Quantity (mmt)')

    axs[1].set_title(commodity + ' ending stocks ')
    axs[1].plot(time_period, end_stock_rep*10**(-6), 'k-', label='reported ending stock')
    axs[1].plot(time_period, end_stock_sim_fix*10**(-6), 'b-', label='FixCons: Fitted r=' + str(round(R_stock_fix[0], 3)))
    axs[1].plot(time_period, end_stock_sim_flex*10**(-6), 'g-', label='FlexCons: Not fitted r=' + str(round(R_stock_flex[0], 3)))
    axs[1].plot(time_period, end_stock_prod_sim_flex*10**(-6), 'g--', label='FlexCons: Producer')
    axs[1].plot(time_period, end_stock_cons_sim_flex*10**(-6), 'g-.', label='FlexCons: Consumer')
    axs[1].plot(time_period, end_stock_prod_sim_fix*10**(-6), 'b--', label='FixCons: Producer')
    axs[1].plot(time_period, end_stock_cons_sim_fix*10**(-6), 'b-.', label='FixCons: Consumer')
    axs[1].legend()
    axs[1].set_ylabel('Quantity (mmt)')

    divider = make_axes_locatable(axs[1])
    ax2 = divider.append_axes("bottom", size="20%", pad=0)
    axs[1].figure.add_axes(ax2)

    ax2.plot(time_period, np.zeros(len(time_period)), 'k-', alpha=0.6)
    ax2.plot(time_period, end_stock_sim_fix - end_stock_rep, 'b--', label='Residual FixCon')
    ax2.plot(time_period, end_stock_sim_flex - end_stock_rep, 'g--', label='Residual FlexCon')
    #ax2.legend()
    ax2.set_xlabel('Year')


    axs[2].set_title(commodity + ' consumption ')
    axs[2].plot(time_period, Q_out_rep, 'k-', label='reported consumption')
    axs[2].plot(time_period, Q_out_sim_fix, 'b-', label='FixCon, r=' + str(round(R_consu_fix[0], 3)))
    axs[2].plot(time_period, Q_out_sim_flex, 'g-', label='FlexCons, r=' + str(round(R_consu_flex[0], 3)))
    axs[2].legend()
    axs[2].set_xlabel('Year')
    axs[2].set_ylabel('Quantity (mmt)')

    axs[3].set_title(commodity + ' production ')
    axs[3].plot(time_period, Q_harvest_rep, 'k-', label='reported production')
    axs[3].plot(time_period, Q_harvest_sim_fix, 'b-', label='FixCons, r=' + str(round(R_harve_fix[0], 3)), alpha=0.6)
    axs[3].plot(time_period, Q_harvest_sim_flex, 'g-', label='FlexCons,r=' + str(round(R_harve_flex[0], 3)), alpha=0.6)
    axs[3].legend()
    axs[3].set_xlabel('Year')
    axs[3].set_ylabel('Quantity (mmt)')

    fig.tight_layout(rect=[0, 0.03, 1, 0.9])

    # plt.title('\nBest fit: alpha_s=' + str(round(best_fit[lowest_chi2][0], 3))
    #              + ' alpha_d=' + str(round(best_fit[lowest_chi2][1], 3))
    #              + ' P_max_prod=' + str(round(best_fit[lowest_chi2][2] * 10 ** 8, 1))
    #              + ' P_max_cons=' + str(round(best_fit[lowest_chi2][3] * 10 ** 8, 1))
    #              + ' I_max=' + str(round(best_fit[lowest_chi2][4], 3))
    #              + '\n Initial guess: alpha=' + str(round(initial_guess[lowest_chi2][0], 3))
    #              + ' alpha_d=' + str(round(initial_guess[lowest_chi2][1], 3))
    #              + ' P_max_prod=' + str(round(initial_guess[lowest_chi2][2] * 10 ** 8, 0))
    #              + ' P_max_cons=' + str(round(initial_guess[lowest_chi2][3] * 10 ** 8, 0))
    #              + ' I_max=' + str(round(initial_guess[lowest_chi2][4], 3)))


    fig.savefig(savePath+'/Fitting_FreeParameters_lowest_chi2.png')



    # ****************************************************
    #           Plot Lowest Cost function
    # ****************************************************



    # WM_price_sim_fix, end_stock_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix, Q_prod_sim_fix, Q_cons_sim_fix, year_sim_fix = model_fixCon(time_period,
    #                                                                                      best_fit[lowest_cost])
    # WM_price_sim_flex, end_stock_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex, Q_prod_sim_flex, Q_cons_sim_flex, year_sim_flex = model_flexCon(time_period,
    #                                                                                           best_fit[lowest_cost])
    WM_price_sim_fix, end_stock_sim_fix, end_stock_prod_sim_fix, end_stock_cons_sim_fix, Q_out_sim_fix, Q_harvest_sim_fix = model_fixCon(
        time_period, best_fit[lowest_cost])
    WM_price_sim_flex, end_stock_sim_flex, end_stock_prod_sim_flex, end_stock_cons_sim_flex, Q_out_sim_flex, Q_harvest_sim_flex = model_flexCon(
        time_period, best_fit[lowest_cost])

    # calculate the pearson correlation coefficient
    R_price_fix = pearsonr(WM_price_sim_fix, WM_price_rep)
    R_stock_fix = pearsonr(end_stock_sim_fix, end_stock_rep)
    R_consu_fix = pearsonr(Q_out_sim_fix, Q_out_rep)
    R_harve_fix = pearsonr(Q_harvest_sim_fix, Q_harvest_rep)

    R_price_flex = pearsonr(WM_price_sim_flex, WM_price_rep)
    R_stock_flex = pearsonr(end_stock_sim_flex, end_stock_rep)
    R_consu_flex = pearsonr(Q_out_sim_flex, Q_out_rep)
    R_harve_flex = pearsonr(Q_harvest_sim_flex, Q_harvest_rep)

    fig, axs = plt.subplots(1, 4, figsize=(16, 6))

    axs[0].set_title(commodity + ' World Market Price ')
    axs[0].plot(time_period, WM_price_rep, 'k-', label='reported  WM price')
    axs[0].plot(time_period, WM_price_sim_fix, 'b-', label='Fixcons: Fitted, r=' + str(round(R_price_fix[0], 3)))
    axs[0].plot(time_period, WM_price_sim_flex, 'g-', label='Flexcons: Fitted, r=' + str(round(R_price_flex[0], 3)))
    axs[0].set_xlabel('Year')
    axs[0].set_ylabel('Price (US$/mt)')
    axs[0].legend()

    # axs[1].set_title(commodity + ' ending stocks ')
    # axs[1].plot(time_period, end_stock_rep, 'k-', label='reported ending stock')
    # axs[1].plot(time_period, end_stock_sim_fix, 'b-', label='FixCons: Fitted r=' + str(round(R_stock_fix[0], 3)))
    # axs[1].plot(time_period, end_stock_sim_flex, 'g-', label='FlexCons: Not fitted r=' + str(round(R_stock_flex[0], 3)))
    # axs[1].legend()
    # axs[1].set_ylabel('Quantity (mmt)')

    axs[1].set_title(commodity + ' ending stocks ')
    axs[1].plot(time_period, end_stock_rep*10**(-6), 'k-', label='reported ending stock')
    axs[1].plot(time_period, end_stock_sim_fix*10**(-6), 'b-', label='FixCons: Fitted r=' + str(round(R_stock_fix[0], 3)))
    axs[1].plot(time_period, end_stock_sim_flex*10**(-6), 'g-', label='FlexCons: Not fitted r=' + str(round(R_stock_flex[0], 3)))
    axs[1].plot(time_period, end_stock_prod_sim_flex*10**(-6), 'g--', label='FlexCons: Producer')
    axs[1].plot(time_period, end_stock_cons_sim_flex*10**(-6), 'g-.', label='FlexCons: Consumer')
    axs[1].plot(time_period, end_stock_prod_sim_fix*10**(-6), 'b--', label='FixCons: Producer')
    axs[1].plot(time_period, end_stock_cons_sim_fix*10**(-6), 'b-.', label='FixCons: Consumer')
    axs[1].legend()
    axs[1].set_ylabel('Quantity (mmt)')

    divider = make_axes_locatable(axs[1])
    ax2 = divider.append_axes("bottom", size="20%", pad=0)
    axs[1].figure.add_axes(ax2)

    ax2.plot(time_period, np.zeros(len(time_period)), 'k-', alpha=0.6)
    ax2.plot(time_period, end_stock_sim_fix - end_stock_rep, 'b--', label='Residual FixCon')
    ax2.plot(time_period, end_stock_sim_flex - end_stock_rep, 'g--', label='Residual FlexCon')
    ax2.set_xlabel('Year')

    axs[2].set_title(commodity + ' consumption ')
    axs[2].plot(time_period, Q_out_rep, 'k-', label='reported consumption')
    axs[2].plot(time_period, Q_out_sim_fix, 'b-', label='FixCon, r=' + str(round(R_consu_fix[0], 3)))
    axs[2].plot(time_period, Q_out_sim_flex, 'g-', label='FlexCons, r=' + str(round(R_consu_flex[0], 3)))
    axs[2].legend()
    axs[2].set_xlabel('Year')
    axs[2].set_ylabel('Quantity (mmt)')

    axs[3].set_title(commodity + ' production ')
    axs[3].plot(time_period, Q_harvest_rep, 'k-', label='reported production')
    axs[3].plot(time_period, Q_harvest_sim_fix, 'b-', label='FixCons, r=' + str(round(R_harve_fix[0], 3)), alpha=0.6)
    axs[3].plot(time_period, Q_harvest_sim_flex, 'g-', label='FlexCons,r=' + str(round(R_harve_flex[0], 3)), alpha=0.6)
    axs[3].legend()
    axs[3].set_xlabel('Year')
    axs[3].set_ylabel('Quantity (mmt)')

    fig.tight_layout(rect=[0, 0.03, 1, 0.9])

    fig.savefig(savePath + '/Fitting_FreeParameters_lowest_cost.png')



    # ****************************************************
    #       Plot Histogram
    # ****************************************************

    # Plot the histogram of the solution of all fits, with the best fit (lowest chi2) marked with a dashed line
    # print('Plot the histogram of best fit values of all the runs')

    fig_hist, axs = plt.subplots(1, 5, figsize=(16, 6))

    best_fit_split_list = []

    best_fit_split_list.append([best_fit[x][0] for x in range(len(best_fit))])
    best_fit_split_list.append([best_fit[x][1] for x in range(len(best_fit))])
    best_fit_split_list.append([best_fit[x][2] * 10 ** 8 for x in range(len(best_fit))])
    best_fit_split_list.append([best_fit[x][3] * 10 ** 8 for x in range(len(best_fit))])
    best_fit_split_list.append([best_fit[x][4] for x in range(len(best_fit))])

    labels = [r"$\alpha$", r"$\beta$", "$P_{max,p}$", "$p_{max,c}$", "$I_{max,c+}$"]

    for i in range(len(best_fit[lowest_chi2])):
        axs[i].hist(best_fit_split_list[i])
        axs[i].set_xlabel(labels[i])

    fig_hist.savefig(savePath+'/Fitting_multi_initialGuess_histogram_best_fit.png')



    print()
    print('Best fit values for lowest chi2:')
    print('alpha_s =', best_fit[lowest_chi2][0])
    print('alpha_d =', best_fit[lowest_chi2][1])
    print('price_prod =', best_fit[lowest_chi2][2]*10**8)
    print('price_cons =', best_fit[lowest_chi2][3]*10**8)
    print('I_max_cons =', best_fit[lowest_chi2][4])

    print(file=fo)
    print('Best fit values for lowest chi2:', file=fo)
    print('alpha_s =', best_fit[lowest_chi2][0], file=fo)
    print('alpha_d =', best_fit[lowest_chi2][1], file=fo)
    print('p_max_prod_const =', best_fit[lowest_chi2][2]*10**8, file=fo)
    print('p_max_cons_base =', best_fit[lowest_chi2][3]*10**8, file=fo)
    print('q_storage_max_cons_factor =', best_fit[lowest_chi2][4], file=fo)

    print(file=fo)
    print('Best fit values for lowest cost function:', file=fo)
    print('alpha_s =', best_fit[lowest_cost][0], file=fo)
    print('alpha_d =', best_fit[lowest_cost][1], file=fo)
    print('p_max_prod_const =', best_fit[lowest_cost][2]*10**8, file=fo)
    print('p_max_cons_base =', best_fit[lowest_cost][3]*10**8, file=fo)
    print('q_storage_max_cons_factor =', best_fit[lowest_cost][4], file=fo)

    fo.close()

    # ****************************************************
    #       Plot Storage levels
    # ****************************************************

    # blue = '#538cc6'
    # orange = '#ff8c1a'
    # yellow = '#ffd11a'
    # red = '#ff5c33'

    # fig2 = plt.figure()
    #
    #
    # years_with_spinup_fix = np.concatenate((np.arange(year_sim_fix[0]-11, year_sim_fix[0]-1), year_sim_fix))
    # years_with_spinup_flex = np.concatenate((np.arange(year_sim_flex[0]-11, year_sim_flex[0]-1), year_sim_flex))
    #
    # plt.title('End Storage flex')
    # plt.plot(years_with_spinup_flex, (Q_cons_sim_flex+Q_prod_sim_flex)*10**(-6), '-', color=blue, label='Total Flex')
    # plt.plot(years_with_spinup_flex, Q_cons_sim_flex*10**(-6), '-', color=orange, label='Consumer Flex')
    # plt.plot(years_with_spinup_flex, Q_prod_sim_flex*10**(-6), '-', color=red, label='Producer Flex')
    # plt.plot(time_period, end_stock_rep*10**(-6), 'k-', label='Reported')
    # plt.legend()
    # plt.xlabel('Year', size=13)
    # plt.ylabel('Ending storage (mmt)', size=12)
    #
    # fig2.savefig(savePath + '/Storage_Flex_fit.png')
    # fig3 = plt.figure()
    #
    # plt.title('End Storage fix')
    # plt.plot(years_with_spinup_fix, (Q_cons_sim_fix+Q_prod_sim_fix)*10**(-6), '-', color=blue, label='Total Fix')
    # plt.plot(years_with_spinup_fix, Q_cons_sim_fix*10**(-6), '-', color=orange, label='Consumer Fix')
    # plt.plot(years_with_spinup_fix, Q_prod_sim_fix*10**(-6), '-', color=red, label='Producer Fix')
    # plt.plot(time_period, end_stock_rep*10**(-6), 'k-', label='Reported')
    # plt.legend()
    # plt.xlabel('Year', size=13)
    # plt.ylabel('Ending storage (mmt)', size=12)
    #
    # fig3.savefig(savePath + '/Storage_Fix_fit.png')

if __name__ == '__main__':

    commodity = 'Soybean'
    start_year = 1975
    end_year = 2018  #End year for fitting the free parameters
    q_storage_max_cons_type = 1  # 0 wheat and corn,  1 rice
    q_out_offset_type = 4  # 0 wheat, corn, rice, 2 or 0 soybean. 0 constant, (1 Multiplicative), 2 spillage rate, 3 constant offset and spillage rate, 4 mutiplative in two steps
    cropYear_month1 = 8  # the month of which the crop year starts, when the yearly average price is determined after crop calander instead of the year
    # Wheat=6 July to June next year
    # Rice=0 Jan to Dec
    # Corn=8 Oct ot Sep
    # soybean=8 Oct ot Sep




    # set the initial guess and the boundaries
    # order alpha_s, alpha_d, P_max_prod, P_max_cons, I_max_cons
    # wheat
    # initial_guess = [0.1, 1, 350*10**(-8), 850*10**(-8), 1.55]
    # lo_bound = [0.05, 0.6, 200*10**(-8), 650*10**(-8), 1]
    # up_bound = [0.5, 1.4, 600*10**(-8), 1500*10**(-8), 2]

    if commodity == 'Wheat_proj':
        cropYear_month1 = 6
        initial_guess = [0.1, 1, 350*10**(-8), 850*10**(-8), 1.55]
        lo_bound = [0.05, 0.6, 200*10**(-8), 650*10**(-8), 1]
        up_bound = [0.15, 1.4, 600*10**(-8), 1500*10**(-8), 2]

    elif commodity == 'Corn_proj':
        cropYear_month1 = 8
        initial_guess = [0.1, 1, 200*10**(-8), 600*10**(-8), 1.55]
        lo_bound = [0.05, 0.6, 50*10**(-8), 300*10**(-8), 1]
        up_bound = [0.15, 1.4, 290*10**(-8), 1000*10**(-8), 2]

    elif commodity == 'Rice_proj':
        cropYear_month1 = 1
        initial_guess = [0.1, 1, 200*10**(-8), 1300*10**(-8), 1.55]
        lo_bound = [0.05, 0.5, 50*10**(-8), 800*10**(-8), 1]
        up_bound = [0.15, 3, 250*10**(-8), 3000*10**(-8), 2]

    elif commodity == 'Soybean' or commodity == 'soybean' or commodity == 'Soybeans' or commodity == 'soybeans':
        cropYear_month1 = 8
        initial_guess = [0.1, 1, 500*10**(-8), 950*10**(-8), 1.55]
        lo_bound = [0.05, 0.5, 200*10**(-8), 650*10**(-8), 1]
        up_bound = [0.15, 3, 650*10**(-8), 1500*10**(-8), 2]

    else:
        cropYear_month1 = 6
        initial_guess = [0.1, 1, 350*10**(-8), 850*10**(-8), 1.55]
        lo_bound = [0.05, 0.6, 200*10**(-8), 650*10**(-8), 1]
        up_bound = [0.15, 1.4, 600*10**(-8), 1500*10**(-8), 2]

    # set grid parameters,, which is the combination of all initial guesses
    # determine the number of initial guesses for each free parameter
    # grid_steps = [4, 3, 2, 2, 3]  # if 1, then only the initial guess value is taken
    grid_steps = [3, 3, 2, 2, 2]  # if 1, then only the initial guess value is taken
    # grid_steps = [3, 3, 1, 1, 1]  # if 1, then only the initial guess value is taken
    # grid_steps = [1, 1, 1, 1, 1]  # if 1, then only the initial guess value is taken

    time_period = np.arange(start_year, end_year + 1)

    # Run the model:
    main(commodity, time_period, cropYear_month1, initial_guess, lo_bound, up_bound, grid_steps, q_storage_max_cons_type, q_out_offset_type)