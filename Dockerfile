FROM continuumio/miniconda3

COPY . /twist

# Install packages
WORKDIR /twist
RUN sed -i '1s/TWIST_env/base/' dependencies/TWIST.yml
RUN conda env update --file dependencies/TWIST.yml

ENV TWIST_path=/twist

# Run script
ENTRYPOINT ["python", "main/run_TWIST.py"]