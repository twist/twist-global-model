# TWIST-Model
- Trade WIth STorage (TWIST) model for computation of global commodity price and storage levels
- Primary Authors: Jacob Schewe, Christian Otto and Katja Frieler
- Original Description: 2017 paper in ERL entitled "The role of storage dynamics in annual wheat prices" https://iopscience.iop.org/article/10.1088/1748-9326/aa678e 
- Contact E-mail for original developer: jacob.schewe@pik-potsdam.de
- 2019 version: Otto, Puma, and other C2-P2 Team Members
- 2020 version: Falkendal

## Model Vignette
1. INPUT: required = national crop production, domestic demand, and stock data; ancillary = harvesting seasons 
2. OUTPUT: world market price and storage dynamics
3. CODE: written in python
4. RUNTIME: few minutes on desktop computer
5. RESOLUTION: global

## Running TWIST
To run TWIST, first clone this repository with something like:

```
git clone https://gitlab.pik-potsdam.de/twist/twist-global-model.git
```

This will clone the repository into the directory where you ran the `git clone` comand. Next, you should navigate to the TWIST directory with:

```
cd twist-global-model
```

Obtain the present working directory with:

```
pwd
```

then replace line 12 of `main/TWIST_settings.py` with the output of the `pwd` command. In other words, set the variable `TWIST_path` to the complete path where the cloned TWIST repository can be found. For example:

```
TWIST_path = "/Users/brandon/repos/twist-global-model"
```

Now, you are ready to run TWIST. To do this, run:

```
python main/run_TWIST.py
```

A directory called `TWIST_Results` will be created in the main repository. A combined output plot of the simulated World Matket price, ending stocks, comsumption and production `TWIST_Plot.png` will be created within `TWIST_Results/` together with a output csv file `TWIST_result.csv` containing both the baseline and scenario time series. Additionally a summary output file called `TWIST_out.txt` will be created at the top-level of the directory along with a pickle of the model (`TWIST_results.pkl`).

## Running Scenarios 
`run_TWIST.py` can be run for differnt crops and for different scenarios. These options are activated by passing additional arguments when running the code, e.g.

```
./run_TWIST.py --crop Wheat --include_prodDecline True --include_importStrat False --exportRestr_countries 'none'
```

This terminal command will run the TWIST model for trade and production shocks happening in 2020 related to world market uncertainties due to the COVID-19 pandemic and the locust outbreak. In this specific case the model will be run for wheat, with a production decline in three main producers ( Russia, Ukraine and Kazakhstan) and in locust threatened counties. The output will show who much the world market price will change in 2020-2021 due to the induced shock. The plot and output file contain both the scenario and baseline time series for 1975-2021.

The model is currently calibrated for 3 differnt crops
- --crop: Wheat, Rice or Corn

The availble scenario options are
- --exportRestr_countries: 
    - Wheat: 'RUS', 'UKR', 'KAZ', 'RUS,UKR' or 'RUS,UKR,KAZ'
    - Rice: 'VNM', 'THA', 'IND', 'VNM,THA' or 'VNM,THA,IND'
    - Corn: 'UKR', 'ARG', 'BRA', 'UKR,ARG' or 'UKR,ARG,BRA'
    - 'none'


- --include_prodDecline: True/False 
- --include_importStrat: True/False


**Baseline**
The baseline price for 2020/2021 is calculated by prescribing both production and consumption for the period 1975-2021. For the period 1975-2019, we use available USDA data and, for the agricultural year 2020/2021, we combine it with the projections published in the OECD-FAO Agricultural Outlook 2019- 2028 report. 

**Export restrictions**
Export restrictions are simulated by temporarily withholding parts of the producer-side stocks from the world supply function. The size of the export ban is based on the production share of major exporters. We lower the available global producer side stocks by a fraction of the production from selected countries. We assume that the country imposing an export ban is able to satisfy domestic demand. That is, the country does not import any grains while the ban is effective, and the consumer demand of the country is fulfilled by direct domestic consumption. We simulate this by lowering the world demand by the share of total consumption for the country imposing an export ban and then we reduce the producer-side storage by the absolute consumption of the country imposing export restrictions, to account for direct domestic consumption. Available countries imposing export bans are: for wheat, Russia, Ukraine and Kazakhstan; for rice, Vietnam, Thailand, and India; and for maize, Ukraine, Argentina, and Brazil.

**Production decline**
The World production decline scenario is implemented by reducing the projected world production in the agricultural year 2020/2021. The production declines are defined as the 20th or 5th percentile of the year-to-year change in production during 2000-2019 for countries restricting exports and countries affected by the locust infestation, respectively. The change in production is calculated by dividing the production of a given year with the production of the previous year. The 20th percentile is chosen as a representative value of a bad weather event, which happens, on average, every 5 years. The 5th percentile is assumed to represent an extreme production failure, with a frequency of one in every 20 years.

**Import policies**
The import strategy scenario represent major changes in consumer-side buying/selling behavior. We model this by intorducing changes in the consumer-side “target” inventory level. We assume an 80th percentile (a 1-in-5-years) increase in the stock-to-use (S/U) ratio with regard to the period 2000-2019. The S/U ratio is calculated as ending stocks divided by the domestic consumption (see Supplementary Table 6 for the adopted values for wheat, rice and maize).



## Docker

To build the Docker container, run:

```
docker build -f Dockerfile . -t wm-twist
```

Now, you can run the container with:

```
docker run -v $PWD:/twist wm-twist
```


